<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="#">
        <img src="{{asset('imagen/icons/constructorGo.svg')}}" width="30" height="30" class="d-inline-block align-top" alt="">
        Constructor Go
    </a>
</nav>
<div class="container">
    <div class="row justify-content-center">
        @if(session()->has('flash'))
            <div class="alert alert-danger" style="margin-top: 2%">
                {{ session('flash') }}
            </div>
        @endif
    </div>
    <div class="row justify-content-center">
        <div class="card mb-3" style="width: 540px; margin-top: 10%">
            <div class="row no-gutters">
                <div class="col-sm-12">
                    <div class="card-body">
                        <h5 class="card-title">Formulario de Pago</h5>
                        <form method="POST" action="https://www.livees.net/Checkout/api?_=52s4b2g5669nuwv4d5ce50lrx4dh7jp1mat9id3o91qzk38fy&__=x09689oc9epdvtn8eu9m4jzkb5sg2739qd5hfry6i5wb681la"
                            enctype="multipart/form-data">
                            @csrf

                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <input id="postURL" class="form-control"
                                           name="postURL" type="hidden" value="http://constructorgo.a2hosted.com/confirmed">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <input id="amt2" class="form-control"
                                           name="amt2" type="hidden" value="{{$amt2}}">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <input id="invno" class="form-control"
                                           name="invno" type="hidden" value="{{$invno}}">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="name">Email: </label>
                                    <input id="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
                                           name="name" type="text"  placeholder="Ingresa tu nombre" value="{{$name}}" required>
                                    <div class="invalid-feedback">{!! $errors->first('name', ':message') !!}</div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="lastname">Email: </label>
                                    <input id="lastname" class="form-control {{$errors->has('lastname') ? 'is-invalid' : ''}}"
                                           name="lastname" type="text"  placeholder="Ingresa tu apellido" value="{{$lastname}}" required>
                                    <div class="invalid-feedback">{!! $errors->first('lastname', ':message') !!}</div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="email">Email: </label>
                                    <input id="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}"
                                           name="email" type="email"  placeholder="Ingresa tu email" value="" required>
                                    <div class="invalid-feedback">{!! $errors->first('email', ':message') !!}</div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12 margen-abajo">
                                    <label for="pais">Pais</label>
                                    <select id="pais" class="form-control {{$errors->has('pais') ? 'is-invalid' : ''}}" name="pais" required>
                                        <option value="BO">Bolivia</option>
                                    </select>
                                    <div class="invalid-feedback">{!! $errors->first('pais', ':message') !!}</div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12 margen-abajo">
                                    <label for="ciudad">Ciudad</label>
                                    <select id="ciudad" class="form-control {{$errors->has('ciudad') ? 'is-invalid' : ''}}" name="ciudad" required>
                                        <option value="Santa Cruz">Santa Cruz</option>
                                    </select>
                                    <div class="invalid-feedback">{!! $errors->first('ciudad', ':message') !!}</div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="phone">Email: </label>
                                    <input id="phone" class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}"
                                           name="phone" type="tel"  placeholder="Ingresa tu telefono" value="{{$phone}}" required>
                                    <div class="invalid-feedback">{!! $errors->first('phone', ':message') !!}</div>
                                </div>
                            </div>


                            <div class="form-row justify-content-end">
                                <button type="submit" class="btn btn-dark btn-sm">Pagar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
