<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="#">
        <img src="{{asset('imagen/icons/constructorGo.svg')}}" width="30" height="30" class="d-inline-block align-top" alt="">
        Constructor Go
    </a>
</nav>
<div class="container">
    <div class="row justify-content-center">
        @if(session()->has('flash'))
            <div class="alert alert-danger" style="margin-top: 2%">
                {{ session('flash') }}
            </div>
        @endif
    </div>
    <div class="row justify-content-center">
        <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
            <div class="card-header">Transaccion Exitosa</div>
            <div class="card-body">
                <h5 class="card-title">Su pago fue realizado con exito</h5>
                <p class="card-text">ID de compra Go Constructor : {{$result}}
                    <br>
                    ID de pago con Livees : {{$order_id}}
                </p>
            </div>
        </div>
    </div>
</div>

</body>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        window.JSInterface.success(true);
    });

</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
