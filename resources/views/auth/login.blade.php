<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Web Cargo</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="#">
        <img src="{{asset('imagen/icons/constructorGo.svg')}}" width="30" height="30" class="d-inline-block align-top" alt="">
        Web Cargo
    </a>
</nav>
<div class="container">
    <div class="row justify-content-center">
        @if(session()->has('flash'))
            <div class="alert alert-danger" style="margin-top: 2%">
                {{ session('flash') }}
            </div>
        @endif
    </div>
    <div class="row justify-content-center">
        <div class="card mb-3" style="width: 540px; margin-top: 20%">
            <div class="row no-gutters">
                <div class="col-sm-12 col-md-4 bg-dark">

                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Iniciar Sesión</h5>
                        <form method="POST" action="{{route('login')}}">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="entradaEmail">Email: </label>
                                    <input id="entradaEmail" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}"
                                           name="email" type="email"  placeholder="Ingresa tu email" value="{{old('email')}}">
                                    <div class="invalid-feedback">{!! $errors->first('email', ':message') !!}</div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="entradaContrasena">Contraseña: </label>
                                    <input id="entradaContrasena" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}"
                                           name="password" type="password" placeholder="Ingresa tu contraseña">
                                    <div class="invalid-feedback">{!! $errors->first('password', ':message') !!}</div>
                                </div>
                            </div>
                            <div class="form-row justify-content-end">
                                <button type="submit" class="btn btn-dark btn-sm">Ingresar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
