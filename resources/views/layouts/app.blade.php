<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Web Cargo</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/simple-sidebar.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
</head>
<style>
    .margen-abajo{
        margin-bottom: 15px;
    }
    .separador-nav{
        margin-top: 10%;
    }
    .margen-boton{
        margin: 5px;
    }
     #leaflet-map {
			width: 800px;
			height:500px;
		}
		
		/* css to customize Leaflet default styles  */
		.custom .leaflet-popup-tip,
		.custom .leaflet-popup-content-wrapper {
			background: #e93434;
			color: #ffffff;
		}
</style>
<body>

    <div id="wrapper" class="d-flex toggled">

        <!-- Sidebar -->
        <div id="sidebar-wrapper" class="bg-dark border-right text-white">
            <div class="sidebar-heading">
                <a class="navbar-brand text-white" href="#">
                    <img src="{{asset('imagen/icons/constructorGo.svg')}}" width="30" height="30" class="d-inline-block align-top" alt="">
                    Web Cargo
                </a>
            </div>
            <div class="list-group list-group-flush">
                @can('user.index')
                    <a href="{{route('admin.user.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Usuarios</a>
                @endcan
                @can('permission_user.index')
                    <a href="{{route('admin.permission_user.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Asignar Permisos y Roles</a>
                @endcan
                @can('client.index')
                    <a href="{{route('admin.client.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Clientes</a>
                @endcan
                @can('driver.index')
                    <a href="{{route('admin.driver.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Transportista</a>
                @endcan
                @can('provider.index')
                    <a href="{{route('admin.provider.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Proveedor</a>
                @endcan
                @can('family.index')
                    <a href="{{route('admin.family.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Tipo Vehiculo</a>
                @endcan
                @can('category.index')
                    <a href="{{route('admin.category.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Categoria</a>
                @endcan
                @can('product.index')
                    <a href="{{route('admin.product.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Producto</a>
                @endcan
                @can('car.index')
                    <a href="{{route('admin.car.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Camioneta</a>
                @endcan
                @can('service.index')
                    <a href="{{route('admin.service.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Servicios</a>
                @endcan
				@can('mark.index')
                    <a href="{{route('admin.mark.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Marca</a>
                @endcan
				@can('color.index')
                    <a href="{{route('admin.color.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Colores</a>
                @endcan
				@can('ocupa.index')
                    <a href="{{route('admin.ocupa.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Gestionar Ocupaciones</a>
                @endcan
                @can('map.index')
                <a href="{{route('admin.map.index')}}" class="list-group-item list-group-item-action bg-dark text-white">Ver Mapa</a>
                @endcan
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <nav class="navbar navbar-dark bg-dark">
                <a id="menu-toggle" class="navbar-brand" href="#"><i class="material-icons">menu</i></a>
                <form class="form-inline" method="POST" action="{{route('logout')}}">
                    @csrf
                    <button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Cerrar Sesión</button>
                </form>
            </nav>

            <div class="container">
                @if(session()->has('error'))
                    <div class="alert alert-danger margen-boton" role="alert">{{session('error')}}</div>
                @endif
                @yield('content')
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="script" type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script rel="script" type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script rel="script" type="text/javascript" src="{{asset('js/page-scripts/table-custom-elements.js')}}"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>


</body>
</html>
