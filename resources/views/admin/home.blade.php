@extends('layouts.app')


@section('content')
    <div class="row" style="margin-top: 5%">
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12">
            <div class="card">
                <h5 class="card-header bg-warning">
                    Bienvenido al panel de Web Cargo
                </h5>
                <div class="card-body">
                    <h5 class="card-title">Nombre y Apellidos :</h5>
                    <p class="card-text">{{auth()->user()->people->first_name . ' ' . auth()->user()->people->last_name}}</p>

                    <h5 class="card-title">Carnet de identidad : </h5>
                    <p class="card-text">{{auth()->user()->people->ci}}</p>

                    <h5 class="card-title">Fecha de nacimiento : </h5>
                    <p class="card-text">{{auth()->user()->people->birthday}}</p>

                    <h5 class="card-title">Telefono : </h5>
                    <p class="card-text">{{auth()->user()->people->telephone}}</p>

                    <h5 class="card-title">Email : </h5>
                    <p class="card-text">{{auth()->user()->email}}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
