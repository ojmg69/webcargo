@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    FORMULARIO DE REGISTRO PARA CLIENTE
                </div>
                <div class="card-body">
                    <form action="{{route('admin.client.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="first_name">Nombre</label>
                                <input id="first_name" class="form-control {{$errors->has('first_name') ? 'is-invalid' : ''}}" type="text" name="first_name" placeholder="Nombre"
                                       value="{{old('first_name')}}">
                                <div class="invalid-feedback">{!! $errors->first('first_name', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="last_name">Apellidos</label>
                                <input id="last_name" class="form-control {{$errors->has('last_name') ? 'is-invalid' : ''}}" type="text" name="last_name" placeholder="Apellidos"
                                       value="{{old('last_name')}}">
                                <div class="invalid-feedback">{!! $errors->first('last_name', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="ci">Carnet Identidad</label>
                                <input id="ci" class="form-control {{$errors->has('ci') ? 'is-invalid' : ''}}" type="text" name="ci"
                                       placeholder="Carnet Identidad" value="{{old('ci')}}">
                                <div class="invalid-feedback">{!! $errors->first('ci', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="datepicker">Fecha de Nacimiento</label>
                                <input id="datepicker" class="form-control {{$errors->has('birthday') ? 'is-invalid' : ''}}" name="birthday"
                                       value="{{old('birthday')}}"/>
                                <div class="invalid-feedback">{!! $errors->first('birthday', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="telephone">Teléfono</label>
                                <input id="telephone" class="form-control {{$errors->has('telephone') ? 'is-invalid' : ''}}" type="text" name="telephone" placeholder="Teléfono"
                                       value="{{old('telephone')}}">
                                <div class="invalid-feedback">{!! $errors->first('telephone', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="gender">Genero</label>
                                <select id="gender" class="form-control {{$errors->has('gender') ? 'is-invalid' : ''}}" name="gender">
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('gender', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 margen-abajo">
                                <label for="occupation">Ocupación</label>
                                <select id="occupation" class="form-control {{$errors->has('occupation') ? 'is-invalid' : ''}}" name="occupation">
                                    <option value="Ingeniero Civil">Ingeniero Civil</option>
                                    <option value="Arquitecto">Arquitecto</option>
                                    <option value="Diseñador Integral">Diseñador Integral</option>
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('occupation', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="image">Subir su foto de perfil (dim 500 x 500)</label>
                                <div class="custom-file">
                                    <input id="image" name="image" type="file" class="custom-file-input {{$errors->has('image') ? 'is-invalid' : ''}}" lang="es" accept="image/*">
                                    <label class="custom-file-label" for="image">Seleccionar una Imagen</label>
                                    <div class="invalid-feedback">{!! $errors->first('image', ':message') !!}</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="email">Email</label>
                                <input id="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" type="email" name="email" placeholder="constructor@gmail.com"
                                       value="{{old('email')}}">
                                <div class="invalid-feedback">{!! $errors->first('email', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6">
                                <label for="password">Password</label>
                                <input id="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" type="password" name="password" placeholder="Password">
                                <div class="invalid-feedback">{!! $errors->first('password', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6">
                                <label for="password_confirmation">Confirmar Password</label>
                                <input id="password_confirmation" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" type="password" name="password_confirmation"
                                       placeholder="Confirmar Password">
                                <div class="invalid-feedback">{!! $errors->first('password', ':message') !!}</div>
                            </div>
                        </div>

                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>

                        <div class="form-row form-action">
                            <button class="btn btn-success margen-boton" type="submit" onclick="loadForm()">Guardar</button>
                            <a class="btn btn-danger margen-boton" href="{{route('admin.client.index')}}" role="button">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }

    </script>

@endsection
