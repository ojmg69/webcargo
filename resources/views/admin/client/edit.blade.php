@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    FORMULARIO DE EDICION PARA CLIENTE
                </div>
                <div class="card-body">
                    <form action="{{route('admin.client.update', [$client->id])}}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="first_name">Nombre</label>
                                <input id="first_name" class="form-control {{$errors->has('first_name') ? 'is-invalid' : ''}}" type="text" name="first_name" placeholder="Nombre"
                                       value="{{$client->people->first_name}}">
                                <div class="invalid-feedback">{!! $errors->first('first_name', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="last_name">Apellidos</label>
                                <input id="last_name" class="form-control {{$errors->has('last_name') ? 'is-invalid' : ''}}" type="text" name="last_name" placeholder="Apellidos"
                                       value="{{$client->people->last_name}}">
                                <div class="invalid-feedback">{!! $errors->first('last_name', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="ci">Carnet Identidad</label>
                                <input id="ci" class="form-control {{$errors->has('ci') ? 'is-invalid' : ''}}" type="text" name="ci"
                                       placeholder="Carnet Identidad" value="{{$client->people->ci}}">
                                <div class="invalid-feedback">{!! $errors->first('ci', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="datepicker">Fecha de Nacimiento</label>
                                <input id="datepicker" class="form-control {{$errors->has('birthday') ? 'is-invalid' : ''}}" name="birthday"
                                       value="{{$client->people->birthday}}"/>
                                <div class="invalid-feedback">{!! $errors->first('birthday', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="telephone">Teléfono</label>
                                <input id="telephone" class="form-control {{$errors->has('telephone') ? 'is-invalid' : ''}}" type="text" name="telephone" placeholder="Teléfono"
                                       value="{{$client->people->telephone}}">
                                <div class="invalid-feedback">{!! $errors->first('telephone', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="gender">Genero</label>
                                <select id="gender" class="form-control {{$errors->has('gender') ? 'is-invalid' : ''}}" name="gender">
                                    <option value="M" {{$client->people->gender == 'M' ? 'selected' : ''}}>Masculino</option>
                                    <option value="F" {{$client->people->gender == 'F' ? 'selected' : ''}}>Femenino</option>
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('gender', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 margen-abajo">
                                <label for="occupation">Ocupación</label>
                                <select id="occupation" class="form-control {{$errors->has('occupation') ? 'is-invalid' : ''}}" name="occupation">
                                    <option value="{{$client->occupation}}" selected>{{$client->occupation}}</option>
                                    <option value="Ingeniero Civil">Ingeniero Civil</option>
                                    <option value="Arquitecto">Arquitecto</option>
                                    <option value="Diseñador Integral">Diseñador Integral</option>
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('occupation', ':message') !!}</div>
                            </div>
                        </div>

                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>

                        <div class="form-row form-action">
                            <button class="btn btn-success margen-boton" type="submit" onclick="loadForm()">Modificar</button>
                            <a class="btn btn-danger margen-boton" href="{{route('admin.client.index')}}" role="button">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }

    </script>

@endsection
