@extends('layouts.app')


@section('content')
    <div class="row" style="margin-top: 5%" >
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12">
            <div class="card">
                <h5 class="card-header bg-warning" style="width: 800px;">
                    Mapa de ubicación de conductores
                </h5>
                <div id="leaflet-map" ></div>
            </div>
        </div>
    </div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
    
	<script src="https://code.jquery.com/jquery-1.11.1.js"></script>
	<script src="/js/main.js"></script>
@endsection
