@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    DETALLE DEL CATEGORIA
                </div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3">Nombre:</dt>
                        <dd class="col-sm-9">{{$category->name}}</dd>

                        <dt class="col-sm-3">Registrado:</dt>
                        <dd class="col-sm-9">{{$category->administrator->people->first_name . ' ' . $category->administrator->people->last_name}}</dd>

                        <dt class="col-sm-3">Fecha de registro:</dt>
                        <dd class="col-sm-9">{{$category->updated_at}}</dd>
                    </dl>
                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-6 margen-abajo">
                            <a href="{{route('admin.category.index')}}" class="btn btn-danger margen-boton" role="button">Atrás</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
