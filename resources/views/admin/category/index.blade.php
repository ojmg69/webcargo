@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
        @can('category.create')
            <div class="col-sm-12">
                <a href="{{route('admin.category.create')}}" role="button" class="btn btn-success">Registrar Categoria</a>
            </div>
        @endcan
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Administrador</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($categories as $category)
                var row = [];
                row[0] = '{{$category->id}}';
                row[1] = '{{$category->name}}';
                row[2] = '{{$category->first_name . ' ' . $category->last_name}}';
                row[3] =   '<div>' +
                    @can('category.show')
                        '<a class="btn btn-info btn-sm" href="{{route('admin.category.show', [$category->id])}}" role="button" style="margin-right:5px">Detalle</a>' +
                    @endcan
                    @can('category.edit')
                        '<a class="btn btn-warning btn-sm" href="{{route('admin.category.edit', [$category->id])}}" role="button" style="margin-right:5px">Editar</a>' +
                    @endcan
                    @can('category.destroy')
                        '<a class="btn btn-danger btn-sm" href="{{route('admin.category.delete', [$category->id])}}" role="button">Eliminar</a>' +
                    @endcan
                    '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
