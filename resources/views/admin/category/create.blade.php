@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    FORMULARIO DE REGISTRO PARA CATEGORIA
                </div>
                <div class="card-body">
                    <form action="{{route('admin.category.store')}}" method="POST">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="name">Nombre</label>
                                <input id="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" type="text" name="name" placeholder="Nombre"
                                       value="{{old('name')}}">
                                <div class="invalid-feedback">{!! $errors->first('name', ':message') !!}</div>
                            </div>
                        </div>

                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>

                        <div class="form-row form-action">
                            <button class="btn btn-success margen-boton" type="submit" onclick="loadForm()">Guardar</button>
                            <a class="btn btn-danger margen-boton" href="{{route('admin.category.index')}}" role="button">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }

    </script>

@endsection
