@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
        @can('product.create')
            <div class="col-sm-12">
                <a href="{{route('admin.product.create')}}" role="button" class="btn btn-success">Registrar Producto</a>
            </div>
        @endcan

    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Código</th>
                                <th>Marca</th>
                                <th>Nombre</th>
                                <th>Familia</th>
                                <th>Categoria</th>
                                <th>Proveedor</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($products as $product)
                var row = [];
                row[0] = '{{$product->id}}';
                row[1] = '{{$product->code}}';
                row[2] = '{{$product->mark}}';
                row[3] = '{{$product->name}}';
                row[4] = '{{$product->family_name}}';
                row[5] = '{{$product->category_name}}';
                row[6] = '{{$product->provider_name}}';
                row[7] =   '<div>' +
                    @can('product.show')
                        '<a class="btn btn-info btn-sm" href="{{route('admin.product.show', [$product->id])}}" role="button" style="margin-right:5px">Detalle</a>' +
                    @endcan
                    @can('product.edit')
                        '<a class="btn btn-warning btn-sm" href="{{route('admin.product.edit', [$product->id])}}" role="button" style="margin-right:5px">Editar</a>' +
                    @endcan
                    @can('product.destroy')
                        '<a class="btn btn-danger btn-sm" href="{{route('admin.product.delete', [$product->id])}}" role="button">Eliminar</a>' +
                    @endcan
                    '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
