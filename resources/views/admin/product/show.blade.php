@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    DETALLE DEL PRODUCTO
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <img src="{{asset('storage/'. $product->url_image)}}" alt="Responsive image" class="img-thumbnail">
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-9">
                            <dl class="row">
                                <dt class="col-sm-3">Código:</dt>
                                <dd class="col-sm-9">{{$product->code}}</dd>

                                <dt class="col-sm-3">Marca:</dt>
                                <dd class="col-sm-9">{{$product->mark}}</dd>

                                <dt class="col-sm-3">Nombre:</dt>
                                <dd class="col-sm-9">{{$product->name}}</dd>

                                <dt class="col-sm-3">Título:</dt>
                                <dd class="col-sm-9">{{$product->title}}</dd>

                                <dt class="col-sm-3">Descripción:</dt>
                                <dd class="col-sm-9">{{$product->description}}</dd>

                                <dt class="col-sm-3">Familia:</dt>
                                <dd class="col-sm-9">{{$product->family->name}}</dd>

                                <dt class="col-sm-3">Categoria:</dt>
                                <dd class="col-sm-9">{{$product->category->name}}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-6 margen-abajo">
                            <a href="{{route('admin.product.index')}}" class="btn btn-danger margen-boton" role="button">Atrás</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
