@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    FORMULARIO DE REGISTRO PARA PRODUCTOS
                </div>
                <div class="card-body">
                    <form action="{{route('admin.product.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="provider_id">Proveedor</label>
                                <select id="provider_id" class="form-control {{$errors->has('provider_id') ? 'is-invalid' : ''}}" name="provider_id">
                                    @foreach($providers as $provider)
                                        <option value="{{$provider->id}}">{{$provider->name . ' ' . $provider->business_name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('provider_id', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="code">Código</label>
                                <input id="code" class="form-control {{$errors->has('code') ? 'is-invalid' : ''}}" type="text" name="code" placeholder="Código"
                                       value="{{old('code')}}">
                                <div class="invalid-feedback">{!! $errors->first('code', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="mark">Marca</label>
                                <input id="mark" class="form-control {{$errors->has('mark') ? 'is-invalid' : ''}}" type="text" name="mark" placeholder="Marca"
                                       value="{{old('mark')}}">
                                <div class="invalid-feedback">{!! $errors->first('mark', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="name">Nombre</label>
                                <input id="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" type="text" name="name"
                                       placeholder="Nombre" value="{{old('name')}}">
                                <div class="invalid-feedback">{!! $errors->first('name', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="title">Título</label>
                                <input id="title" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}" type="text" name="title"
                                       placeholder="Título" value="{{old('title')}}">
                                <div class="invalid-feedback">{!! $errors->first('title', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 margen-abajo">
                                <label for="description">Descripción</label>
                                <textarea id="address" name="description" class="form-control {{$errors->has('description') ? 'is-invalid' : ''}}" placeholder="Descripción">{{old('description')}}</textarea>
                                <div class="invalid-feedback">{!! $errors->first('description', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="image">Seleccione el logo de producto (dim 200 x 200)</label>
                                <div class="custom-file">
                                    <input id="image" name="image" type="file" class="custom-file-input {{$errors->has('image') ? 'is-invalid' : ''}}" lang="es" accept="image/*">
                                    <label class="custom-file-label" for="image">Seleccionar una Imagen</label>
                                    <div class="invalid-feedback">{!! $errors->first('image', ':message') !!}</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="family_id">Familia</label>
                                <select id="family_id" class="form-control {{$errors->has('family_id') ? 'is-invalid' : ''}}" name="family_id">
                                    @foreach($families as $family)
                                        <option value="{{$family->id}}">{{$family->name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('family_id', ':message') !!}</div>
                            </div>

                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="category_id">Categoria</label>
                                <select id="category_id" class="form-control {{$errors->has('category_id') ? 'is-invalid' : ''}}" name="category_id">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('category_id', ':message') !!}</div>
                            </div>
                        </div>

                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>

                        <div class="form-row form-action">
                            <button class="btn btn-success margen-boton" type="submit" onclick="loadForm()">Guardar</button>
                            <a class="btn btn-danger margen-boton" href="{{route('admin.product.index')}}" role="button">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }

    </script>

@endsection
