@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
        @can('car.create')
            <div class="col-sm-12">
                <a href="{{route('admin.car.create')}}" role="button" class="btn btn-success">Registrar Camioneta</a>
            </div>
        @endcan
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Nro Placa</th>
                                <th>Conductor</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($cars as $car)
                var row = [];
                row[0] = '{{$car->id}}';
                row[1] = '{{$car->mark}}';
                row[2] = '{{$car->model}}';
                row[3] = '{{$car->license_plate}}';
                row[4] = '{{$car->first_name . ' ' . $car->last_name}}';
                row[5] =   '<div>' +
                    @can('car.show')
                        '<a class="btn btn-info btn-sm" href="{{route('admin.car.show', [$car->id])}}" role="button" style="margin-right:5px">Detalle</a>' +
                    @endcan
                    @can('car.edit')
                        '<a class="btn btn-warning btn-sm" href="{{route('admin.car.edit', [$car->id])}}" role="button" style="margin-right:5px">Editar</a>' +
                    @endcan
                    @can('car.destroy')
                        '<a class="btn btn-danger btn-sm" href="{{route('admin.car.delete', [$car->id])}}" role="button">Eliminar</a>' +
                    @endcan

                    '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
