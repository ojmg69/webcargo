@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    FORMULARIO DE REGISTRO PARA CAMIONETA
                </div>
                <div class="card-body">
                    <form action="{{route('admin.car.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="driver_id">Transportista</label>
                                <select id="driver_id" class="form-control {{$errors->has('driver_id') ? 'is-invalid' : ''}}" name="driver_id">
                                    @foreach($drivers as $driver)
                                        <option value="{{$driver->driver_id}}">{{$driver->first_name . ' ' . $driver->last_name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('driver_id', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="mark">Marca</label>
                                <input id="mark" class="form-control {{$errors->has('mark') ? 'is-invalid' : ''}}" type="text" name="mark" placeholder="Marca"
                                       value="{{old('mark')}}">
                                <div class="invalid-feedback">{!! $errors->first('mark', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="model">Modelo</label>
                                <input id="model" class="form-control {{$errors->has('model') ? 'is-invalid' : ''}}" type="text" name="model" placeholder="Modelo"
                                       value="{{old('model')}}">
                                <div class="invalid-feedback">{!! $errors->first('model', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="license_plate">Número de Placa</label>
                                <input id="license_plate" class="form-control {{$errors->has('license_plate') ? 'is-invalid' : ''}}" type="text" name="license_plate"
                                       placeholder="Número de Placa" value="{{old('license_plate')}}">
                                <div class="invalid-feedback">{!! $errors->first('license_plate', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="capacity">Capacidad</label>
                                <textarea id="capacity" name="capacity" class="form-control {{$errors->has('capacity') ? 'is-invalid' : ''}}" placeholder="Capaciddad">{{old('capacity')}}</textarea>
                                <div class="invalid-feedback">{!! $errors->first('capacity', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="image">Seleccione una foto de la camioneta (dim 500 x 500)</label>
                                <div class="custom-file">
                                    <input id="image" name="image" type="file" class="custom-file-input {{$errors->has('image') ? 'is-invalid' : ''}}" lang="es" accept="image/*">
                                    <label class="custom-file-label" for="image">Seleccionar una Imagen</label>
                                    <div class="invalid-feedback">{!! $errors->first('image', ':message') !!}</div>
                                </div>
                            </div>
                        </div>

                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>

                        <div class="form-row form-action">
                            <button class="btn btn-success margen-boton" type="submit" onclick="loadForm()">Guardar</button>
                            <a class="btn btn-danger margen-boton" href="{{route('admin.car.index')}}" role="button">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }

    </script>

@endsection
