@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    DETALLE DE LA CAMIONETA
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <img src="{{asset('storage/'. $car->url_image)}}" alt="Responsive image" class="img-thumbnail">
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-9">
                            <dl class="row">
                                <dt class="col-sm-3">Marca:</dt>
                                <dd class="col-sm-9">{{$car->mark}}</dd>

                                <dt class="col-sm-3">Modelo:</dt>
                                <dd class="col-sm-9">{{$car->model}}</dd>

                                <dt class="col-sm-3">Número de Placa:</dt>
                                <dd class="col-sm-9">{{$car->license_plate}}</dd>

                                <dt class="col-sm-3">Capacidad:</dt>
                                <dd class="col-sm-9">{{$car->capacity}}</dd>

                                <dt class="col-sm-3">Transportista:</dt>
                                <dd class="col-sm-9">{{$car->driver->people->first_name . ' ' . $car->driver->people->last_name}}</dd>

                            </dl>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-6 margen-abajo">
                            <a href="{{route('admin.car.index')}}" class="btn btn-danger margen-boton" role="button">Atrás</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
