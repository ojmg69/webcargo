@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    DETALLE DEL MARCA
                </div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3">Nombre:</dt>
                        <dd class="col-sm-9">{{$color->name}}</dd>

                        <dt class="col-sm-3">Registrado:</dt>
                        <dd class="col-sm-9">{{$color->administrator->people->first_name . ' ' . $color->administrator->people->last_name}}</dd>

                        <dt class="col-sm-3">Fecha de registro:</dt>
                        <dd class="col-sm-9">{{$color->updated_at}}</dd>
                    </dl>
                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-6 margen-abajo">
                            <a href="{{route('admin.mark.index')}}" class="btn btn-danger margen-boton" role="button">Atrás</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
