@extends('layouts.app')

@section('content')
@include('layouts.modal')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
        @can('color.create')
            <div class="col-sm-12">
                <a href="{{route('admin.mark.create')}}" role="button" class="btn btn-success">Registrar Marca</a>
            </div>
        @endcan
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Administrador</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($colors as $color)
                var row = [];
                row[0] = '{{$color->id}}';
                row[1] = '{{$color->name}}';
                row[2] = '{{$color->first_name . ' ' . $color->last_name}}';
                row[3] =   '<div>' +
                    @can('mark.show')
                        '<a class="btn btn-info btn-sm" href="{{route('admin.mark.show', [$color->id])}}" role="button" style="margin-right:5px">Detalle</a>' +
                    @endcan
                    @can('mark.edit')
                        '<a class="btn btn-warning btn-sm" href="{{route('admin.mark.edit', [$color->id])}}" role="button" style="margin-right:5px">Editar</a>' +
                    @endcan
                    @can('mark.destroy')
                        '<a href="#" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#confirm">Eliminar</a>' +
                    @endcan
                    '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
