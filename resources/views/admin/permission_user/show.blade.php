@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    PERMISOS DEL USUARIO DEL USUARIO
                </div>
                <div class="card-body">
                    <form action="{{route('admin.permission_user.store', [$user->id])}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-3">
                                <img src="{{asset('storage/'. $user->people->url_profile)}}" alt="Responsive image" class="img-thumbnail">
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-9">
                                <dl class="row">
                                    <dt class="col-sm-3">Nombre:</dt>
                                    <dd class="col-sm-9">{{$user->people->first_name}}</dd>

                                    <dt class="col-sm-3">Apellidos:</dt>
                                    <dd class="col-sm-9">{{$user->people->last_name}}</dd>

                                    <dt class="col-sm-3">Genero:</dt>
                                    <dd class="col-sm-9">{{$user->people->gender == 'M' ? 'Masculino' : 'Femenino'}}</dd>

                                    <dt class="col-sm-3">Carnet Identidad:</dt>
                                    <dd class="col-sm-9">{{$user->people->ci}}</dd>

                                    <dt class="col-sm-3">Teléfono:</dt>
                                    <dd class="col-sm-9">{{$user->people->telephone}}</dd>

                                    <dt class="col-sm-3">Fecha de Nacimiento:</dt>
                                    <dd class="col-sm-9">{{$user->people->birthday}}</dd>

                                    <dt class="col-sm-3">Email:</dt>
                                    <dd class="col-sm-9">{{$user->people->user->email}}</dd>
                                </dl>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    @foreach($permissions as $permission)
                                        <div class="col-sm-3">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="permission_ids[]" value="{{$permission->id}}" {{$permission->administrator ? 'checked' : ''}}>
                                                <label class="form-check-label" for="inlineCheckbox1">{{$permission->name}}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                        <div class="form-row form-action">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <button type="submit" class="btn btn-success margen-boton" onclick="loadForm()">Guardar</button>
                                <a href="{{route('admin.permission_user.index')}}" class="btn btn-danger margen-boton" role="button">Atrás</a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }
    </script>
@endsection
