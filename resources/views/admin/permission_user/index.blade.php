@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Carnet Identidad</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($users as $user)
                var row = [];
                row[0] = '{{$user->id}}';
                row[1] = '{{$user->first_name}}';
                row[2] = '{{$user->last_name}}';
                row[3] = '{{$user->ci}}';
                row[4] =   '<div>' +
                    @can('permission_user.show')
                    '<a class="btn btn-info btn-sm" href="{{route('admin.permission_user.show', [$user->administrator_id])}}" role="button" style="margin-right:5px">Permisos</a>' +
                    @endcan
                    '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
