@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    DESEA ELIMINAR FAMILIA
                </div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3">Nombre:</dt>
                        <dd class="col-sm-9">{{$family->name}}</dd>

                        <dt class="col-sm-3">Registrado:</dt>
                        <dd class="col-sm-9">{{$family->administrator->people->first_name . ' ' . $family->administrator->people->last_name}}</dd>

                        <dt class="col-sm-3">Fecha de registro:</dt>
                        <dd class="col-sm-9">{{$family->updated_at}}</dd>
                    </dl>

                    <form action="{{route('admin.family.destroy', [$family->id])}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <input id="key_delete" class="form-control {{$errors->has('key_delete') ? 'is-invalid' : ''}}" type="text" name="key_delete" placeholder="ELIMINAR">
                                <div class="invalid-feedback">{!! $errors->first('key_delete', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <button type="submit" class="btn btn-danger margen-boton" onclick="loadForm()">Eliminar</button>
                                <a href="{{route('admin.family.index')}}" class="btn btn-success margen-boton" role="button">Cancelar</a>
                            </div>
                        </div>
                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
        }
    </script>

@endsection
