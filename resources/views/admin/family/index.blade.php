@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
        @can('family.create')
            <div class="col-sm-12">
                <a href="{{route('admin.family.create')}}" role="button" class="btn btn-success">Registrar Tipo de Vehiculo</a>
            </div>
        @endcan
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Administrador</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($families as $family)
                var row = [];
                row[0] = '{{$family->id}}';
                row[1] = '{{$family->name}}';
                row[2] = '{{$family->first_name . ' ' . $family->last_name}}';
                row[3] =   '<div>' +

                    @can('family.show')
                        '<a class="btn btn-info btn-sm" href="{{route('admin.family.show', [$family->id])}}" role="button" style="margin-right:5px">Detalle</a>' +
                    @endcan

                    @can('family.edit')
                        '<a class="btn btn-warning btn-sm" href="{{route('admin.family.edit', [$family->id])}}" role="button" style="margin-right:5px">Editar</a>' +
                    @endcan

                    @can('family.destroy')
                        '<a class="btn btn-danger btn-sm" href="{{route('admin.family.delete', [$family->id])}}" role="button">Eliminar</a>' +
                    @endcan

                    '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
