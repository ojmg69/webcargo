@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    DETALLE DEL SERVICIO
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <img src="{{asset('storage/'. $service->url_image)}}" alt="Responsive image" class="img-thumbnail">
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-9">
                            <dl class="row">
                                <dt class="col-sm-3">Nombre:</dt>
                                <dd class="col-sm-9">{{$service->name}}</dd>

                                <dt class="col-sm-3">Tipo de Empresa:</dt>
                                <dd class="col-sm-9">{{$service->type_business}}</dd>

                                <dt class="col-sm-3">Descripción:</dt>
                                <dd class="col-sm-9">{{$service->description}}</dd>

                                <dt class="col-sm-3">Sucursales:</dt>
                                <dd class="col-sm-9">{{$service->branch_office}}</dd>

                                <dt class="col-sm-3">Contactanos:</dt>
                                <dd class="col-sm-9">{{$service->contacts}}</dd>

                                <dt class="col-sm-3">Whatsaap:</dt>
                                <dd class="col-sm-9">{{$service->whatsaap}}</dd>

                                <dt class="col-sm-3">Facebook:</dt>
                                <dd class="col-sm-9">{{$service->facebook}}</dd>

                                <dt class="col-sm-3">Instagram:</dt>
                                <dd class="col-sm-9">{{$service->instagram}}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-6 margen-abajo">
                            <a href="{{route('admin.service.index')}}" class="btn btn-danger margen-boton" role="button">Atrás</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
