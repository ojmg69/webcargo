@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
        @can('service.create')
            <div class="col-sm-12">
                <a href="{{route('admin.service.create')}}" role="button" class="btn btn-success">Registrar Servicio</a>
            </div>
        @endcan
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Tipo Empresa</th>
                                <th>Visitas</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($services as $service)
                var row = [];
                row[0] = '{{$service->id}}';
                row[1] = '{{$service->name}}';
                row[2] = '{{$service->type_business}}';
                row[3] = '{{$service->count_visit}}';
                row[4] =   '<div>' +
                    @can('service.show')
                        '<a class="btn btn-info btn-sm" href="{{route('admin.service.show', [$service->id])}}" role="button" style="margin-right:5px">Detalle</a>' +
                    @endcan
                    @can('service.edit')
                        '<a class="btn btn-warning btn-sm" href="{{route('admin.service.edit', [$service->id])}}" role="button" style="margin-right:5px">Editar</a>' +
                    @endcan
                    @can('service.destroy')
                        '<a class="btn btn-danger btn-sm" href="{{route('admin.service.delete', [$service->id])}}" role="button">Eliminar</a>' +
                    @endcan
                    '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
