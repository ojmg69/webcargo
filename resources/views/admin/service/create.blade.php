@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    FORMULARIO DE REGISTRO DE SERVICIOS
                </div>
                <div class="card-body">
                    <form action="{{route('admin.service.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="type_business">Tipo de Servicio</label>
                                <select id="type_business" class="form-control {{$errors->has('type_business') ? 'is-invalid' : ''}}" name="type_business">
                                    <option value="PROVEEDOR">PROVEEDOR</option>
                                    <option value="DISTRIBUIDOR">DISTRIBUIDOR</option>
                                    <option value="SERVICIO">SERVICIO</option>
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('type_business', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-12 margen-abajo">
                                <label for="name">Nombre</label>
                                <input id="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" type="text" name="name"
                                       placeholder="Nombre" value="{{old('name')}}">
                                <div class="invalid-feedback">{!! $errors->first('name', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 margen-abajo">
                                <label for="description">Descripción</label>
                                <textarea id="description" name="description" class="form-control {{$errors->has('description') ? 'is-invalid' : ''}}" placeholder="Descripción">{{old('description')}}</textarea>
                                <div class="invalid-feedback">{!! $errors->first('description', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 margen-abajo">
                                <label for="branch_office">Sucursales</label>
                                <textarea id="branch_office" name="branch_office" class="form-control {{$errors->has('branch_office') ? 'is-invalid' : ''}}" placeholder="Sucursales">{{old('branch_office')}}</textarea>
                                <div class="invalid-feedback">{!! $errors->first('branch_office', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 margen-abajo">
                                <label for="contacts">Contactanos</label>
                                <textarea id="contacts" name="contacts" class="form-control {{$errors->has('contacts') ? 'is-invalid' : ''}}" placeholder="Contactanos">{{old('contacts')}}</textarea>
                                <div class="invalid-feedback">{!! $errors->first('contacts', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-12 margen-abajo">
                                <label for="whatsaap">Whatsaap (opcional)</label>
                                <input id="whatsaap" class="form-control {{$errors->has('whatsaap') ? 'is-invalid' : ''}}" type="text" name="whatsaap"
                                       placeholder="Whatsaap" value="{{old('whatsaap')}}">
                                <div class="invalid-feedback">{!! $errors->first('whatsaap', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-12 margen-abajo">
                                <label for="facebook">Facebook (opcional)</label>
                                <input id="facebook" class="form-control {{$errors->has('facebook') ? 'is-invalid' : ''}}" type="text" name="facebook"
                                       placeholder="Facebook" value="{{old('facebook')}}">
                                <div class="invalid-feedback">{!! $errors->first('facebook', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-12 margen-abajo">
                                <label for="instagram">Instagram (opcional)</label>
                                <input id="instagram" class="form-control {{$errors->has('instagram') ? 'is-invalid' : ''}}" type="text" name="instagram"
                                       placeholder="Instagram" value="{{old('instagram')}}">
                                <div class="invalid-feedback">{!! $errors->first('instagram', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="image">Seleccione un banner para el servicio</label>
                                <div class="custom-file">
                                    <input id="image" name="image" type="file" class="custom-file-input {{$errors->has('image') ? 'is-invalid' : ''}}" lang="es" accept="image/*">
                                    <label class="custom-file-label" for="image">Seleccionar una Imagen</label>
                                    <div class="invalid-feedback">{!! $errors->first('image', ':message') !!}</div>
                                </div>
                            </div>
                        </div>

                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>

                        <div class="form-row form-action">
                            <button class="btn btn-success margen-boton" type="submit" onclick="loadForm()">Guardar</button>
                            <a class="btn btn-danger margen-boton" href="{{route('admin.service.index')}}" role="button">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }

    </script>

@endsection
