@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    FORMULARIO DE EDICION PARA TRANSPORTISTA
                </div>
                <div class="card-body">
                    <form action="{{route('admin.driver.update', [$driver->id])}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="first_name">Nombre</label>
                                <input id="first_name" class="form-control {{$errors->has('first_name') ? 'is-invalid' : ''}}" type="text" name="first_name" placeholder="Nombre"
                                       value="{{$driver->people->first_name}}">
                                <div class="invalid-feedback">{!! $errors->first('first_name', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="last_name">Apellidos</label>
                                <input id="last_name" class="form-control {{$errors->has('last_name') ? 'is-invalid' : ''}}" type="text" name="last_name" placeholder="Apellidos"
                                       value="{{$driver->people->last_name}}">
                                <div class="invalid-feedback">{!! $errors->first('last_name', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="ci">Carnet Identidad</label>
                                <input id="ci" class="form-control {{$errors->has('ci') ? 'is-invalid' : ''}}" type="text" name="ci"
                                       placeholder="Carnet Identidad" value="{{$driver->people->ci}}">
                                <div class="invalid-feedback">{!! $errors->first('ci', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="datepicker">Fecha de Nacimiento</label>
                                <input id="datepicker" class="form-control {{$errors->has('birthday') ? 'is-invalid' : ''}}" name="birthday"
                                       value="{{$driver->people->birthday}}"/>
                                <div class="invalid-feedback">{!! $errors->first('birthday', ':message') !!}</div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="telephone">Teléfono</label>
                                <input id="telephone" class="form-control {{$errors->has('telephone') ? 'is-invalid' : ''}}" type="text" name="telephone" placeholder="Teléfono"
                                       value="{{$driver->people->telephone}}">
                                <div class="invalid-feedback">{!! $errors->first('telephone', ':message') !!}</div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                <label for="gender">Genero</label>
                                <select id="gender" class="form-control {{$errors->has('gender') ? 'is-invalid' : ''}}" name="gender">
                                    <option value="M" {{$driver->people->gender == 'M' ? 'selected' : ''}}>Masculino</option>
                                    <option value="F" {{$driver->people->gender == 'F' ? 'selected' : ''}}>Femenino</option>
                                </select>
                                <div class="invalid-feedback">{!! $errors->first('gender', ':message') !!}</div>
                            </div>
                        </div>

                        <div id="load" class="progress" hidden="true">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>

                        <div class="form-row form-action">
                            <button type="submit" class="btn btn-success margen-boton" onclick="loadForm()">Modificar</button>
                            <a href="{{route('admin.driver.index')}}" class="btn btn-danger margen-boton" role="button">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }
    </script>

@endsection
