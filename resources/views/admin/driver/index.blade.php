@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
        @can('driver.create')
            <div class="col-sm-12">
                <a href="{{route('admin.driver.create')}}" role="button" class="btn btn-success">Registrar Transportista</a>
            </div>
        @endcan
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Carnet Identidad</th>
                                <th>Teléfono</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($drivers as $driver)
                var row = [];
                row[0] = '{{$driver->id}}';
                row[1] = '{{$driver->first_name}}';
                row[2] = '{{$driver->last_name}}';
                row[3] = '{{$driver->ci}}';
                row[4] = '{{$driver->telephone}}';
                row[5] =   '<div>' +
                    @can('driver.show')
                        '<a class="btn btn-info btn-sm" href="{{route('admin.driver.show', [$driver->driver_id])}}" role="button" style="margin-right:5px">Detalle</a>' +
                    @endcan
                        @can('driver.edit')
                        '<a class="btn btn-warning btn-sm" href="{{route('admin.driver.edit', [$driver->driver_id])}}" role="button" style="margin-right:5px">Editar</a>' +
                    @endcan
                        @can('driver.destroy')
                        '<a class="btn btn-danger btn-sm" href="{{route('admin.driver.delete', [$driver->driver_id])}}" role="button">Eliminar</a>' +
                    @endcan
                        '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
