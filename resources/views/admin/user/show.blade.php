@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    DETALLE DEL USUARIO
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <img src="{{asset('storage/'. $user->people->url_profile)}}" alt="Responsive image" class="img-thumbnail">
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-9">
                            <dl class="row">
                                <dt class="col-sm-3">Nombre:</dt>
                                <dd class="col-sm-9">{{$user->people->first_name}}</dd>

                                <dt class="col-sm-3">Apellidos:</dt>
                                <dd class="col-sm-9">{{$user->people->last_name}}</dd>

                                <dt class="col-sm-3">Genero:</dt>
                                <dd class="col-sm-9">{{$user->people->gender == 'M' ? 'Masculino' : 'Femenino'}}</dd>

                                <dt class="col-sm-3">Carnet Identidad:</dt>
                                <dd class="col-sm-9">{{$user->people->ci}}</dd>

                                <dt class="col-sm-3">Teléfono:</dt>
                                <dd class="col-sm-9">{{$user->people->telephone}}</dd>

                                <dt class="col-sm-3">Fecha de Nacimiento:</dt>
                                <dd class="col-sm-9">{{$user->people->birthday}}</dd>

                                <dt class="col-sm-3">Email:</dt>
                                <dd class="col-sm-9">{{$user->people->user->email}}</dd>
                            </dl>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-6 margen-abajo">
                            <a href="{{route('admin.user.index')}}" class="btn btn-danger margen-boton" role="button">Atrás</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
