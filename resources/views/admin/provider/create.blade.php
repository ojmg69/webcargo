@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                            <a id="basic-form-tab" class="nav-link active" href="#basic" data-toggle="tab" role="tab" aria-controls="basic" aria-selected="true">Datos Básicos</a>
                        </li>
                        <li class="nav-item">
                            <a id="location-form-tab" class="nav-link" href="#location" data-toggle="tab" role="tab" aria-controls="location" aria-selected="false">Datos de Ubicación</a>
                        </li>
                        <li class="nav-item">
                            <a id="auth-form-tab" class="nav-link" href="#auth" data-toggle="tab" role="tab" aria-controls="auth" aria-selected="false">Datos de Autenticación</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.provider.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div id="myTabContent" class="tab-content">

                            <div id="basic" class="tab-pane fade show active" role="tabpanel" aria-labelledby="basic-tab">
                                <div class="form-row">
                                    <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                        <label for="name">Nombre</label>
                                        <input id="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" type="text" name="name" placeholder="Nombre"
                                               value="{{old('name')}}">
                                        <div class="invalid-feedback">{!! $errors->first('name', ':message') !!}</div>
                                    </div>
                                    <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                        <label for="business_name">Razon Social</label>
                                        <input id="business_name" class="form-control {{$errors->has('business_name') ? 'is-invalid' : ''}}" type="text" name="business_name" placeholder="Razon Social"
                                               value="{{old('business_name')}}">
                                        <div class="invalid-feedback">{!! $errors->first('business_name', ':message') !!}</div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                        <label for="nit">Nit</label>
                                        <input id="nit" class="form-control {{$errors->has('nit') ? 'is-invalid' : ''}}" type="number" name="nit"
                                               placeholder="Nit" value="{{old('nit')}}">
                                        <div class="invalid-feedback">{!! $errors->first('nit', ':message') !!}</div>
                                    </div>
                                    <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                        <label for="telephone">Teléfono</label>
                                        <input id="telephone" class="form-control {{$errors->has('telephone') ? 'is-invalid' : ''}}" type="number" name="telephone" placeholder="Teléfono"
                                               value="{{old('telephone')}}">
                                        <div class="invalid-feedback">{!! $errors->first('telephone', ':message') !!}</div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-sm-12 margen-abajo">
                                        <label for="type">Tipo Proveedor</label>
                                        <select id="type" class="form-control {{$errors->has('type') ? 'is-invalid' : ''}}" name="type">
                                            <option value="Fabricante">Fabricante</option>
                                            <option value="Distribuidor">Distribuidor</option>
                                        </select>
                                        <div class="invalid-feedback">{!! $errors->first('type', ':message') !!}</div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-12 margen-abajo">
                                        <label for="image">Seleccione su logo (dim 250 x 250)</label>
                                        <div class="custom-file">
                                            <input id="image" name="image" type="file" class="custom-file-input {{$errors->has('image') ? 'is-invalid' : ''}}" lang="es" accept="image/*">
                                            <label class="custom-file-label" for="image">Seleccionar una Imagen</label>
                                            <div class="invalid-feedback">{!! $errors->first('image', ':message') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="location" class="tab-pane fade" role="tabpanel" aria-labelledby="location-tab">
                                <div class="form-row">
                                    <div class="form-group col-sm-12 margen-abajo">
                                        <div id="map"></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                        <label for="longitude">Longitude</label>
                                        <input id="longitude" class="form-control {{$errors->has('longitude') ? 'is-invalid' : ''}}" type="text" name="longitude"
                                        value="{{old('longitude')}}" readonly>
                                    </div>
                                    <div class="form-group col-sm-12 col-md-6 margen-abajo">
                                        <label for="latitude">Latitude</label>
                                        <input id="latitude" class="form-control {{$errors->has('latitude') ? 'is-invalid' : ''}}" type="text" name="latitude"
                                        value="{{old('latitude')}}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-12 margen-abajo">
                                        <label for="address">Dirección</label>
                                        <textarea id="address" name="address" class="form-control {{$errors->has('address') ? 'is-invalid' : ''}}" placeholder="Dirección">{{old('address')}}</textarea>
                                        <div class="invalid-feedback">{!! $errors->first('address', ':message') !!}</div>
                                    </div>
                                </div>
                            </div>

                            <div id="auth" class="tab-pane fade" role="tabpanel" aria-labelledby="auth-tab">
                                <div class="form-row">
                                    <div class="form-group col-sm-12">
                                        <label for="username">Usuario</label>
                                        <input id="username" class="form-control {{$errors->has('username') ? 'is-invalid' : ''}}" type="text" name="username" placeholder="Usuario"
                                               value="{{old('username')}}">
                                        <div class="invalid-feedback">{!! $errors->first('username', ':message') !!}</div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-sm-12 col-md-6">
                                        <label for="password">Password</label>
                                        <input id="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" type="password" name="password" placeholder="Password">
                                        <div class="invalid-feedback">{!! $errors->first('password', ':message') !!}</div>
                                    </div>
                                    <div class="form-group col-sm-12 col-md-6">
                                        <label for="password_confirmation">Confirmar Password</label>
                                        <input id="password_confirmation" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" type="password" name="password_confirmation"
                                               placeholder="Confirmar Password">
                                        <div class="invalid-feedback">{!! $errors->first('password', ':message') !!}</div>
                                    </div>
                                    <div id="load" class="progress" hidden="true">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                             aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                    </div>

                                    <div class="form-row form-action">
                                        <button class="btn btn-success margen-boton" type="submit" onclick="loadForm()">Guardar</button>
                                        <a class="btn btn-danger margen-boton" href="{{route('admin.provider.index')}}" role="button">Cancelar</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function loadForm(){
            $('#load').attr("hidden",false);
            $('.form-control').attr("readonly", true);
            $('.form-action').attr("hidden", true);
        }
    </script>
    <script>
        var marker = null;
        function initMap() {
            var scz = {lat: -17.783334, lng: -63.182175};
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 14, center: scz});

            map.addListener('click', function (e) {
                placeMarkerAndPanTo(e.latLng, map);
                inputLatLng(e.latLng.toJSON());
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            if(marker != null)
                marker.setMap(null);
            marker = new google.maps.Marker({
                position: latLng,
                map: map
            });
            map.panTo(latLng);
        }
        function inputLatLng(latLng) {
            console.log(latLng.lat);
            console.log(latLng.lng);
            $('#latitude').val(latLng.lat);
            $('#longitude').val(latLng.lng);
        }
    </script>
    <script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyANMbvcUKkfb94GzCA1SnNTF4lV6dd-WV8&callback=initMap"
            type="text/javascript"></script>

@endsection
