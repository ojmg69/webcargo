@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 5%; margin-bottom: 15px">
        @can('provider.create')
            <div class="col-sm-12">
                <a href="{{route('admin.provider.create')}}" role="button" class="btn btn-success">Registrar Proveedor</a>
            </div>
        @endcan
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-striped">

                        <table class="table table-general-elements" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Razon Social</th>
                                <th>Nit</th>
                                <th>teléfono</th>
                                <th>Acciones</th>
                            </tr>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var datos = [];
            @foreach($providers as $provider)
                var row = [];
                row[0] = '{{$provider->id}}';
                row[1] = '{{$provider->name}}';
                row[2] = '{{$provider->business_name}}';
                row[3] = '{{$provider->nit}}';
                row[4] = '{{$provider->telephone}}';
                row[5] =   '<div>' +
                    @can('provider.show')
                        '<a class="btn btn-info btn-sm" href="{{route('admin.provider.show', [$provider->id])}}" role="button" style="margin-right:5px">Detalle</a>' +
                    @endcan
                    @can('provider.edit')
                        '<a class="btn btn-warning btn-sm" href="{{route('admin.provider.edit', [$provider->id])}}" role="button" style="margin-right:5px">Editar</a>' +
                    @endcan
                    @can('provider.destroy')
                        '<a class="btn btn-danger btn-sm" href="{{route('admin.provider.delete', [$provider->id])}}" role="button">Eliminar</a>' +
                    @endcan
                    '</div>';
                datos.push(row);
            @endforeach
            addDatosGeneral(datos);
        });

    </script>
@endsection
