@extends('layouts.app')

@section('content')
    <div class="row separador-nav">
        <div class="col-sm-12">

            <div class="card">
                <div class="card-header">
                    DETALLE DEL PROVEEDOR
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <img src="{{asset('storage/'. $provider->url_image)}}" alt="Responsive image" class="img-thumbnail">
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-9">
                            <dl class="row">
                                <dt class="col-sm-3">Nombre:</dt>
                                <dd class="col-sm-9">{{$provider->name}}</dd>

                                <dt class="col-sm-3">Razón Social:</dt>
                                <dd class="col-sm-9">{{$provider->business_name}}</dd>

                                <dt class="col-sm-3">Nit:</dt>
                                <dd class="col-sm-9">{{$provider->nit}}</dd>

                                <dt class="col-sm-3">Telefono:</dt>
                                <dd class="col-sm-9">{{$provider->telephone}}</dd>

                                <dt class="col-sm-3">Tipo de Proveedor:</dt>
                                <dd class="col-sm-9">{{$provider->type}}</dd>

                                <dt class="col-sm-3">Username:</dt>
                                <dd class="col-sm-9">{{$provider->username}}</dd>

                                <dt class="col-sm-3">Dirección:</dt>
                                <dd class="col-sm-9">{{$provider->address}}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-6 margen-abajo">
                            <a href="{{route('admin.provider.index')}}" class="btn btn-danger margen-boton" role="button">Atrás</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
