<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $fillable = ['occupation_id', 'uuid_facebook', 'uuid_gmail', 'people_id', 'email'];

    public function orders()
    {
        return $this->hasMany('App\Order', 'client_id');
    }

    public function people()
    {
        return $this->belongsTo('App\People', 'people_id');
    }

    public function occupation()
    {
        return $this->belongsTo('App\Occupation', 'occupation_id');
    }
}
