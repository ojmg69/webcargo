<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $table = 'drivers';
    protected $fillable = ['email', 'password', 'mark_id',  'model', 'license_plate' , 'capacity',
        'color_id', 'type_vehicle_id', 'car_owner', 'url_soat', 'url_brevet', 'url_car', 'url_ruat',
        'status_offert', 'status_notify','people_id'];

    public function people()
    {
        return $this->belongsTo('App\People', 'people_id');
    }

    public function offerts()
    {
        return $this->hasMany('App\Offert', 'driver_id');
    }
    public function wallets()
    {
        return $this->hasMany('App\Wallet', 'driver_id');
    }

    public function mark()
    {
        return $this->belongsTo('App\Mark', 'mark_id');
    }

    public function color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }
    public function type_vehicle()
    {
        return $this->belongsTo('App\TypeVehicle', 'type_vehicle_id');
    }

}
