<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderImage extends Model
{
    protected $table = 'order_images';
    protected $fillable = ['url_image', 'order_id'];
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
