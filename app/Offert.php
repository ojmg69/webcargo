<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offert extends Model
{
    protected $table = 'offerts';
    protected $fillable = ['driver_id','order_id','delivery_time','offered_price','status'];

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
    public function driver()
    {
        return $this->belongsTo('App\Driver', 'driver_id');
    }

    public function delivery(){
        return $this->hasOne('App\Delivery', 'offert_id');
    }



}
