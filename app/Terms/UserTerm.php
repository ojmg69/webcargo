<?php
/**
 * Created by PhpStorm.
 * User: Ronaldo Rivero
 * Date: 29/05/2019
 * Time: 20:46
 */

namespace App\Terms;


use Illuminate\Support\Facades\DB;

class UserTerm
{
    public function getUserWithCI($ci)
    {
        $people = DB::table('people')
            ->where('ci', '=', $ci)
            ->first();
        return $people;
    }

    public function getUserWithEmail($email)
    {
        $user = DB::table('users')
            ->where('email', '=', $email)
            ->first();
        return $user;
    }

    public function isValidCiId($id_user, $ci)
    {
        $people = DB::table('people')
            ->join('administrators', 'people.id', '=', 'administrators.people_id')
            ->where('ci', '=', $ci)
            ->whereNotIn('administrators.id', [$id_user])
            ->first();
        return $people == null;
    }

    public function isValidEdit($user_id)
    {
        return true;
    }

    public function isValidDelete($user_id)
    {
        return true;
    }
}
