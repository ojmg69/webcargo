<?php
/**
 * Created by PhpStorm.
 * User: Ronaldo Rivero
 * Date: 30/05/2019
 * Time: 0:35
 */

namespace App\Terms;


use Illuminate\Support\Facades\DB;

class DriverTerm
{
    public function isValidCiId($driver_id, $ci)
    {
        $people = DB::table('people')
            ->join('drivers', 'people.id', '=', 'drivers.people_id')
            ->where('ci', '=', $ci)
            ->whereNotIn('drivers.id', [$driver_id])
            ->first();
        return $people == null;
    }

    public function isValidEdit($driver_id)
    {
        return true;
    }

    public function isValidDelete($driver_id)
    {
        return true;
    }
}
