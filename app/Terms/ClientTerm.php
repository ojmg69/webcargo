<?php
/**
 * Created by PhpStorm.
 * User: Ronaldo Rivero
 * Date: 29/05/2019
 * Time: 20:46
 */

namespace App\Terms;


use Illuminate\Support\Facades\DB;

class ClientTerm
{
    public function getClientWithCI($ci)
    {
        $people = DB::table('people')
            ->where('ci', '=', $ci)
            ->first();
        return $people;
    }

    public function getClientWithEmail($email)
    {
        $user = DB::table('clients')
            ->where('email', '=', $email)
            ->first();
        return $user;
    }

    public function isValidCiId($client_id, $ci)
    {
        $people = DB::table('people')
            ->join('clients', 'people.id', '=', 'clients.people_id')
            ->where('ci', '=', $ci)
            ->whereNotIn('clients.id', [$client_id])
            ->first();
        return $people == null;
    }

    public function isValidEdit($client_id)
    {
        return true;
    }

    public function isValidDelete($client_id)
    {
        return true;
    }
}
