<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $table = 'peoples';
    protected $fillable = [ 'full_name','ci' , 'gender', 'birthday', 'telephone', 'url_profile', 'people_type',
    'uuid_notify', 'number_account', 'address' ,'url_ci', 'country_code','mobile', 'department'];

    public function user(){
        return $this->hasOne('App\User', 'people_id');
    }

    public function driver(){
        return $this->hasOne('App\Driver', 'people_id');
    }

    public function client(){
        return $this->hasOne('App\Client', 'people_id');
    }
    public function location_peoples()
    {
        return $this->hasMany('App\Location', 'people_id');
    }

}
