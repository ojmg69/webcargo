<?php
/**
 * Created by PhpStorm.
 * User: Ronaldo Rivero
 * Date: 29/05/2019
 * Time: 20:51
 */

namespace App\Presenters;


use Illuminate\Support\HtmlString;

class UserPresenter
{
    public function userCanBeEdit()
    {
        //return $this->model->people->status == 200;
        return true;
    }

    public function userCanBeDelete()
    {
        return true;
        /*
        $cant_prov = count($this->model->proveedores);
        $cant_prod = count($this->model->productos);
        $cant_fam = count($this->model->familias);
        $cant_cat = count($this->model->categorias);

        return $this->model->estado == 200 && $cant_prov == 0 && $cant_prod == 0 && $cant_fam == 0 && $cant_cat == 0;
        */
    }

    public function getActionTableIndex($id)
    {
        $html = '<div>';
        if(auth()->user()->hasPermissionTo('user.show'))
        {
            $html = $html . '<a class="btn btn-info btn-sm" href=" ' . route('admin.user.show', [$id]) . ' " role="button" style="margin-right:5px">Detalle</a>';
        }
        if(auth()->user()->hasPermissionTo('user.edit'))
        {
            $html = $html . '<a class="btn btn-warning btn-sm" href=" ' . route('admin.user.edit', [$id]) . ' " role="button" style="margin-right:5px">Editar</a>';
        }
        if(auth()->user()->hasPermissionTo('user.destroy'))
        {
            $html = $html . '<a class="btn btn-danger btn-sm" href=" ' . route('admin.user.delete', [$id]) . ' " role="button" style="margin-right:5px">Eliminar</a>';
        }
        return new HtmlString($html);
    }
}
