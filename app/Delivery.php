<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = 'deliveries';
    protected $fillable = ['observation', 'score', 'offert_id',  'status', 'start_delivery', 'end_delivery'];

    public function offert()
    {
        return $this->belongsTo('App\Offert', 'offert_id');
    }


}
