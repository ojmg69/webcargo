<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'colors';
    protected $fillable = ['name', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function drivers()
    {
        return $this->hasMany('App\Driver', 'mark_id');
    }
}
