<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = ['amount', 'payment_methods_id', 'delivery_id'];

    public function delivery()
    {
        return $this->belongsTo('App\Delivery', 'delivery_id');
    }
    public function payment_method()
    {
        return $this->belongsTo('App\PaymentMethod', 'payment_methods_id');
    }


}
