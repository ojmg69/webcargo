<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';
    protected $fillable = ['longitude', 'latitude', 'people_id'];

    public function location_people()
    {
        return $this->belongsTo('App\People', 'people_id');
    }
}
