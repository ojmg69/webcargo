<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
    'description', 'total_price','locate_origin','locate_destination',
    'origin','destination', 'description_origin', 'description_destination','product_type','helpers','schedule_order',
    'measured_type','weight','width','high','status','type_vehicle_id','client_id',
    'distance_order'];

    public function order_images()
    {
        return $this->hasMany('App\OrderImage', 'order_images_id');
    }

    public function offerts()
    {
        return $this->hasMany('App\Offert', 'order_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }
    public function type_vehicle()
    {
        return $this->belongsTo('App\TypeVehicle', 'type_vehicle_id');
    }


}
