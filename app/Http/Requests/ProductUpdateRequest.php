<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'          =>'required|string|min:3',
            'name'          =>'required|string|min:3',
            'title'         =>'required|string|min:3',
            'description'   =>'nullable|string',
            'image'         =>'nullable|image|dimensions:min_width=200,min_height=200|dimensions:max_width=250,max_height=250',
            'provider_id'   =>'required|integer',
            'family_id'     =>'required|integer',
            'category_id'     =>'required|integer',
        ];
    }
}
