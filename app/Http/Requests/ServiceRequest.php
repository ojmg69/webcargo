<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>'required|string|min:3',
            'type_business' =>'required|string',
            'description'   =>'required|string',
            'branch_office' =>'required|string',
            'contacts'      =>'required|string',
            'whatsaap'      =>'nullable|string',
            'facebook'      =>'nullable|string',
            'instagram'     =>'nullable|string',
            'image'         =>'nullable|image|dimensions:min_width=250,min_height=250',
        ];
    }
}
