<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProviderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string|min:3',
            'business_name' => 'required|string|min:3',
            'nit'           => 'required|alpha_num|min:5',
            'image'         => 'nullable|image|dimensions:min_width=250,min_height=250|dimensions:max_width=300,max_height=300',
            'address'       => 'nullable|string',
            'telephone'     => 'nullable|alpha_num',
            'type'          => 'required|string',
            'longitude'     => 'nullable|numeric',
            'latitude'      => 'nullable|numeric',
            'username'      => 'required|string|regex:/(^([A-Za-z])+$)/u',
            'password'      => 'required|string|alpha_num|confirmed'
        ];
    }
}
