<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        =>'required|string',
            'last_name'         =>'required|string',
            'ci'                =>'required|alpha_num|min:7|max:10|unique:people',
            'gender'            =>'required|max:1',
            'birthday'          =>'required|date',
            'telephone'         =>'required|alpha_num',
            'email'             =>'required|string|max:255|email|unique:users',
            'password'          =>'required|string|min:6|confirmed'
        ];
    }
}
