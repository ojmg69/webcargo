<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mark'              =>'required|string|min:3',
            'model'             =>'required|string|min:3',
            'license_plate'     =>'required|string|min:3|unique:cars',
            'capacity'          =>'nullable|string',
            'image'             =>'required|image|dimensions:min_width=500,min_height=500',
            'driver_id'         =>'required|integer',
        ];
    }
}
