<?php

namespace App\Http\Controllers;

use App\Classes\MessageError;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    private $messageError;

    public function __construct()
    {
        $this->messageError = new MessageError();
    }

    public function login(){
        $credenciales = $this->validate(request(), [
            'usuario' => 'required|string',
            'password' => 'required|string'
        ]);

        $usuario = DB::table('persona')
            ->join('users', 'persona.id', '=', 'users.id_persona')
            ->where('users.usuario', '=', $credenciales['usuario'])->first();

        if($usuario != null){
            if($usuario->estado === 'INACTIVO' || $usuario->tipo == 1 || $usuario->tipo == 2 || $usuario->tipo == 3){
                $error = $this->messageError->getMessageError(MessageError::ERROR_LOGIN);
                return view('layout-error', ['error' => $error]);
            }
        }

        if(Auth::attempt($credenciales)){
            return redirect()->route('admin.perfil.informacion');
        }
        return back()->withErrors([
            'usuario' => 'Estas credenciales no concuerdan con nuestro registros.',
        ])->withInput(request(['usuario']));
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
