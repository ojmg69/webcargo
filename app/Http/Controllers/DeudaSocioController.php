<?php

namespace App\Http\Controllers;

use App\Asignacion_Multa;
use App\B_Asignacion_Multa;
use App\B_Deuda_Cobro_Adicional;
use App\B_Deuda_Contrato_Evento;
use App\B_Deuda_Nuevo_Socio;
use App\B_Deuda_Servicio_Basico;
use App\Bitacora;
use App\Classes\BitacoraKey;
use App\Deuda_Asignacion_Cobro;
use App\Deuda_Cobro_Adicional;
use App\Deuda_Contrato_Alquiler;
use App\Deuda_Contrato_Evento;
use App\Deuda_Nuevo_Socio;
use App\Deuda_Servicio_Basico;
use App\Persona;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeudaSocioController extends Controller
{
    //AFILIACION
    public function deudaAfiliacionIndex($id_socio)
    {
        $socio = Persona::findOrFail($id_socio);
        $deudas = Deuda_Nuevo_Socio::where('id_socio', '=', $id_socio)
            ->orderBy('fecha_deuda', 'asc')
            ->get();
        return view('socio.deuda.afiliacion.index', ['socio' => $socio, 'deudas' => $deudas]);
    }

    public function deudaAfiliacionEditar($id)
    {
        $deuda = Deuda_Nuevo_Socio::findOrFail($id);
        $socio = $deuda->socio;
        return view('socio.deuda.afiliacion.editar', ['socio' => $socio, 'deuda' => $deuda]);
    }

    public function deudaAfiliacionModificar(Request $request, $id)
    {
        $deuda = Deuda_Nuevo_Socio::findOrFail($id);
        $socio = $deuda->socio;

        $deuda->monto = $request['monto'];
        $deuda->save();
        $this->bitacoraAfiliacion('MODIFICADO', $request['detalle'], auth()->user(), $deuda);
        return redirect()->route('admin.socio.deuda.afiliado.index', [$socio->id]);
    }

    //SERVICIOS BASICOS
    public function deudaServicioBasicoIndex($id_socio)
    {
        $socio = Persona::findOrFail($id_socio);
        $fecha = Carbon::now('America/La_Paz')->addMonth(1);

        $ids_recibos = DB::table('recibo_servicio_basico')
            ->join('deuda_servicio_basico', 'recibo_servicio_basico.id_deuda_servicio_basico', '=', 'deuda_servicio_basico.id')
            ->join('contrato_servicio_basico', 'deuda_servicio_basico.id_contrato_servicio_basico', '=', 'contrato_servicio_basico.id')
            ->join('contrato_socio_puesto', 'contrato_servicio_basico.id_contrato_socio_puesto', '=', 'contrato_socio_puesto.id')
            ->where('contrato_socio_puesto.id_socio', '=', $id_socio)
            ->pluck('id_deuda_servicio_basico');

        $deudas = DB::table('deuda_servicio_basico')
            ->whereNotIn('deuda_servicio_basico.id', $ids_recibos)
            ->whereDate('deuda_servicio_basico.fecha_deuda', '<', $fecha)
            ->join('contrato_servicio_basico', 'deuda_servicio_basico.id_contrato_servicio_basico', '=', 'contrato_servicio_basico.id')
            ->join('contrato_socio_puesto', 'contrato_servicio_basico.id_contrato_socio_puesto', '=', 'contrato_socio_puesto.id')
            ->join('servicio_basico', 'contrato_servicio_basico.id_servicio_basico', '=', 'servicio_basico.id')
            ->join('sector', 'contrato_socio_puesto.id_sector', '=', 'sector.id')
            ->join('puesto', 'contrato_socio_puesto.id_puesto', '=', 'puesto.id')
            ->join('persona', 'contrato_socio_puesto.id_socio', '=', 'persona.id')
            ->where('contrato_socio_puesto.id_socio', '=', $id_socio)
            ->select('deuda_servicio_basico.id as id', 'puesto.numero_puesto as puesto', 'sector.nombre as sector', 'persona.nombre as socio_nombre',
                'persona.apellidos as socio_apellidos', 'contrato_socio_puesto.inquilino as inquilino',
                'servicio_basico.nombre as servicio_basico', 'deuda_servicio_basico.mes as mes', 'deuda_servicio_basico.anio as anio',
                'deuda_servicio_basico.monto')
            ->get();

        return view('socio.deuda.servicio_basico.index', ['socio' => $socio, 'deudas' => $deudas]);
    }

    public function deudaServicioBasicoEditar($id)
    {
        $deuda = Deuda_Servicio_Basico::findOrFail($id);
        $socio = $deuda->contrato_servicio_basico->contrato_socio_puesto->socio;
        return view('socio.deuda.servicio_basico.editar', ['socio'=>$socio, 'deuda'=>$deuda]);
    }

    public function deudaServicioBasicoModificar(Request $request, $id)
    {
        $deuda = Deuda_Servicio_Basico::findOrFail($id);
        $deuda->monto = $request['monto'];
        $deuda->save();
        $this->bitacoraServicioBasico('MODIFICADO', $request['detalle'], auth()->user(), $deuda);
        $socio = $deuda->contrato_servicio_basico->contrato_socio_puesto->socio;

        return redirect()->route('admin.socio.deuda.serviciobasico.index', [$socio->id]);
    }

    //ALQUILER
    public function deudaAlquilerIndex($id_socio)
    {
        $socio = Persona::findOrFail($id_socio);


        $ids = DB::table('recibo_deuda_contrato_alquiler')
            ->pluck('id_deuda_contrato_alquiler');

        $deudas = DB::table('deuda_contrato_alquiler')
            ->whereNotIn('deuda_contrato_alquiler.id', $ids)
            ->join('contrato_alquiler', 'deuda_contrato_alquiler.id_contrato_alquiler', '=', 'contrato_alquiler.id')
            ->join('persona', 'contrato_alquiler.id_inquilino', '=', 'persona.id')
            ->where('persona.id', '=', $id_socio)
            ->select('deuda_contrato_alquiler.id as id', 'persona.nombre as nombre', 'persona.apellidos as apellidos', 'contrato_alquiler.motivo as motivo',
                'deuda_contrato_alquiler.detalle as detalle', 'deuda_contrato_alquiler.monto as monto', 'deuda_contrato_alquiler.fecha_deuda as fecha')
            ->get();

        return view('socio.deuda.alquiler.index', ['socio' => $socio, 'deudas' => $deudas]);
    }

    public function deudaAlquilerEditar($id)
    {
        $deuda = Deuda_Contrato_Alquiler::findOrFail($id);
        $socio = $deuda->contrato_alquiler->inquilino;
        return view('socio.deuda.alquiler.editar', ['socio'=>$socio, 'deuda'=>$deuda]);
    }

    public function deudaAlquilerModificar(Request $request, $id)
    {
        $deuda = Deuda_Contrato_Alquiler::findOrFail($id);
        $deuda->monto = $request['monto'];
        $deuda->save();
        $socio = $deuda->contrato_alquiler->inquilino;

        return redirect()->route('admin.socio.deuda.alquiler.index', [$socio->id]);
    }


    //EVENTOS
    public function deudaEventoIndex($id_socio)
    {
        $socio = Persona::findOrFail($id_socio);

        $ids_recibos = DB::table('recibo_contrato_evento')
            ->join('deuda_contrato_evento', 'recibo_contrato_evento.id_deuda_contrato_evento', '=', 'deuda_contrato_evento.id')
            ->join('contrato_evento', 'deuda_contrato_evento.id_contrato_evento', '=', 'contrato_evento.id')
            ->where('contrato_evento.id_socio', '=', $id_socio)
            ->pluck('id_deuda_contrato_evento');

        $deudas = Deuda_Contrato_Evento::whereNotIn('id', $ids_recibos)
            ->join('contrato_evento', 'deuda_contrato_evento.id_contrato_evento', '=', 'contrato_evento.id')
            ->where('contrato_evento.id_socio', '=', $id_socio)
            ->select('deuda_contrato_evento.*')
            ->orderBy('fecha_deuda', 'asc')
            ->get();

        return view('socio.deuda.evento.index', ['socio'=>$socio, 'deudas' => $deudas]);
    }

    public function deudaEventoEditar($id){
        $deuda = Deuda_Contrato_Evento::findOrFail($id);
        $socio = $deuda->contrato_evento->socio;
        return view('socio.deuda.evento.editar', ['socio'=>$socio, 'deuda'=>$deuda]);
    }

    public function deudaEventoModificar(Request $request, $id)
    {
        $deuda = Deuda_Contrato_Evento::findOrFail($id);
        $deuda->descripcion = $request['descripcion'];
        $deuda->monto = $request['monto'];
        $deuda->save();
        $this->bitacoraEvento('MODIFICADO', $request['detalle'], auth()->user(), $deuda);

        $socio = $deuda->contrato_evento->socio;
        return redirect()->route('admin.socio.deuda.evento.index', [$socio->id]);
    }

    //MULTAS
    public function deudaMultaIndex($id_socio)
    {
        $socio = Persona::findOrFail($id_socio);

        $ids_recibos = DB::table('recibo_asignacion_multa')
            ->join('asignacion_multa', 'recibo_asignacion_multa.id_asignacion_multa', '=', 'asignacion_multa.id')
            ->where('asignacion_multa.id_socio', '=', $id_socio)
            ->pluck('id_asignacion_multa');

        $deudas = Asignacion_Multa::whereNotIn('id', $ids_recibos)
            ->where('id_socio', '=', $id_socio)
            ->orderBy('fecha_deuda', 'asc')
            ->get();

        return view('socio.deuda.multa.index', ['socio'=>$socio, 'deudas'=>$deudas]);

    }

    public function deudaMultaEditar($id)
    {
        $deuda = Asignacion_Multa::findOrFail($id);
        $socio = $deuda->socio;

        return view('socio.deuda.multa.editar', ['socio'=>$socio, 'deuda'=>$deuda]);
    }

    public function deudaMultaModificar(Request $request, $id){
        $deuda = Asignacion_Multa::findOrFail($id);
        $deuda->motivo = $request['motivo'];
        $deuda->descripcion = $request['descripcion'];
        $deuda->monto = $request['monto'];
        $deuda->save();
        $this->bitacoraMulta('MODIFICADO', $request['detalle'], auth()->user(), $deuda);

        $socio = $deuda->socio;
        return redirect()->route('admin.socio.deuda.multa.index', [$socio->id]);
    }

    //COBRO ADICIONAL
    public function deudaOtroIndex($id_socio)
    {
        $socio = Persona::findOrFail($id_socio);

        $ids_recibos = DB::table('recibo_deuda_asignacion_cobro')
            ->join('deuda_asignacion_cobro', 'recibo_deuda_asignacion_cobro.id_deuda_asignacion_cobro', 'deuda_asignacion_cobro.id')
            ->join('asignacion_cobro', 'deuda_asignacion_cobro.id_asignacion_cobro', 'asignacion_cobro.id')
            ->where('asignacion_cobro.id_socio', '=', $id_socio)
            ->pluck('id_deuda_asignacion_cobro');

        $deudas = Deuda_Asignacion_Cobro::whereNotIn('deuda_asignacion_cobro.id', $ids_recibos)
            ->join('asignacion_cobro', 'deuda_asignacion_cobro.id_asignacion_cobro', 'asignacion_cobro.id')
            ->where('asignacion_cobro.id_socio', '=', $id_socio)
            ->select('deuda_asignacion_cobro.*')
            ->orderBy('fecha_deuda', 'asc')
            ->get();

        return view('socio.deuda.otro.index', ['socio' => $socio, 'deudas' => $deudas]);
    }

    public function deudaOtroEditar($id)
    {
        $deuda = Deuda_Asignacion_Cobro::findOrFail($id);
        $socio = $deuda->asignacion_cobro->socio;
        return view('socio.deuda.otro.editar', ['socio'=>$socio, 'deuda'=>$deuda]);
    }

    //BITACORA
    public function deudaOtroModificar(Request $request, $id)
    {
        $deuda = Deuda_Asignacion_Cobro::findOrFail($id);
        $socio = $deuda->asignacion_cobro->socio;

        $deuda->monto = $request['monto'];
        $deuda->save();
        return redirect()->route('admin.socio.deuda.otro.index', [$socio->id]);
    }

    private function bitacoraAfiliacion($accion, $detalle, Model $user, Model $afiliacion)
    {
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraKey::DB_DEUDA_AFILIACION;
        $bitacora->id_usuario = $user->persona()->id;

        if($bitacora->save()){
            $b_afiliacion = new B_Deuda_Nuevo_Socio();
            $b_afiliacion->id_deuda_nuevo_socio = $afiliacion->id;
            $b_afiliacion->monto = $afiliacion->monto;
            $b_afiliacion->fecha_deuda = $afiliacion->fecha_deuda;
            $b_afiliacion->id_socio = $afiliacion->id_socio;
            $b_afiliacion->id_bitacora = $bitacora->id;
            $b_afiliacion->save();
        }
    }
    private function bitacoraServicioBasico($accion, $detalle, Model $user, Model $servicio)
    {
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraKey::DB_DEUDA_SERVICIO_BASICO;
        $bitacora->id_usuario = $user->persona()->id;

        if($bitacora->save()){
            $b_servicio = new B_Deuda_Servicio_Basico();
            $b_servicio->id_deuda_servicio_basico = $servicio->id;
            $b_servicio->fecha_deuda = $servicio->fecha_deuda;
            $b_servicio->mes = $servicio->mes;
            $b_servicio->anio = $servicio->anio;
            $b_servicio->monto = $servicio->monto;
            $b_servicio->id_contrato_servicio_basico = $servicio->id_contrato_servicio_basico;
            $b_servicio->id_bitacora = $bitacora->id;
            $b_servicio->save();
        }
    }
    private function bitacoraEvento($accion, $detalle, Model $user, Model $evento)
    {
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraKey::DB_DEUDA_EVENTO;
        $bitacora->id_usuario = $user->persona()->id;

        if($bitacora->save()){
            $b_evento = new B_Deuda_Contrato_Evento();
            $b_evento->id_deuda_contrato_evento = $evento->id;
            $b_evento->fecha_deuda = $evento->fecha_deuda;
            $b_evento->descripcion = $evento->descripcion;
            $b_evento->monto = $evento->monto;
            $b_evento->id_contrato_evento = $evento->id_contrato_evento;
            $b_evento->id_bitacora = $bitacora->id;
            $b_evento->save();
        }
    }
    private function bitacoraMulta($accion, $detalle, Model $user, Model $multa)
    {
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraKey::DB_DEUDA_MULTA;
        $bitacora->id_usuario = $user->persona()->id;

        if($bitacora->save()){
            $b_multa = new B_Asignacion_Multa();
            $b_multa->id_asignacion_multa = $multa->id;
            $b_multa->motivo = $multa->motivo;
            $b_multa->descripcion = $multa->descripcion;
            $b_multa->monto = $multa->monto;
            $b_multa->fecha_deuda = $multa->fecha_deuda;
            $b_multa->id_multa = $multa->id_multa;
            $b_multa->id_socio = $multa->id_socio;
            $b_multa->id_usuario = $user->persona()->id;
            $b_multa->id_bitacora = $bitacora->id;
            $b_multa->save();
        }
    }
    private function bitacoraOtro($accion, $detalle, Model $user, Model $otro)
    {
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraKey::DB_DEUDA_OTRO;
        $bitacora->id_usuario = $user->persona()->id;

        if($bitacora->save()){
            $b_cobro = new B_Deuda_Cobro_Adicional();
            $b_cobro->id_deuda_cobro_adicional = $otro->id;
            $b_cobro->monto = $otro->monto;
            $b_cobro->fecha_deuda = $otro->fecha_deuda;
            $b_cobro->id_gestion_cobro_adicional = $otro->id_gestion_cobro_adicional;
            $b_cobro->id_socio = $otro->id_socio;
            $b_cobro->id_bitacora = $bitacora->id;
            $b_cobro->save();
        }
    }
}
