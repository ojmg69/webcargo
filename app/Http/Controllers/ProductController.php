<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteRequest;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Repositories\CategoryRepository;
use App\Repositories\FamilyRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProviderRepository;
use App\Repositories\StockRepository;

class ProductController extends Controller
{
    protected $product;
    protected $family;
    protected $category;
    protected $provider;
    protected $stock;

    public function __construct(ProductRepository $product, FamilyRepository $family,
                                CategoryRepository $category, ProviderRepository $provider,
                                StockRepository $stock)
    {
        $this->product = $product;
        $this->family = $family;
        $this->category = $category;
        $this->provider = $provider;
        $this->stock = $stock;
    }

    public function index()
    {
        $products = $this->product->index();
        return view('admin.product.index', ['products'=>$products]);
    }

    public function show($id)
    {
        $product = $this->product->find($id);
        return view('admin.product.show', ['product'=>$product]);
    }

    public function create()
    {
        $families = $this->family->index();
        $categories = $this->category->index();
        $providers = $this->provider->index();
        return view('admin.product.create', ['families'=>$families,
            'categories'=>$categories, 'providers'=>$providers]);
    }

    public function store(ProductStoreRequest $request)
    {
        $product = $this->product->store($request);
        $this->stock->store($product);
        return redirect()->route('admin.product.index');
    }

    public function edit($id)
    {
        $product = $this->product->find($id);
        $families = $this->family->index();
        $categories = $this->category->index();
        $providers = $this->provider->index();
        return view('admin.product.edit', ['product'=>$product,
            'families'=>$families, 'categories'=>$categories, 'providers'=>$providers]);
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        $product = $this->product->update($request, $id);
        return redirect()->route('admin.product.index');
    }

    //falta terms
    public function delete($id)
    {
        $product = $this->product->find($id);
        return view('admin.product.delete', ['product'=>$product]);
    }

    //falta terms
    public function destroy(DeleteRequest $request, $id)
    {
        if($this->isValidDelete($request))
        {
            $this->product->destroy($id);
            return redirect()->route('admin.product.index');
        }
        return back()->withErrors(['key_delete'=>trans('validation.key_delete')])
            ->withInput(request(['key_delete']));

    }

    private function isValidDelete($request)
    {
        return $request['key_delete'] == 'ELIMINAR';
    }

}
