<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Presenters\UserPresenter;
use App\Repositories\UserRepository;
use App\Terms\UserTerm;

class UserController extends Controller
{
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->user->index();
        $user_presenter = new UserPresenter();
        return view('admin.user.index', ['users'=>$users, 'user_presenter'=>$user_presenter]);
    }

    public function show($id)
    {
        $user = $this->user->find($id);
        return view('admin.user.show', ['user' => $user]);
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(UserStoreRequest $request)
    {
        $this->user->store($request);
        return redirect()->route('admin.user.index');
    }

    public function edit($id)
    {
        $terms = new UserTerm();
        if($terms->isValidEdit($id))
        {
            $user = $this->user->find($id);
            return view('admin.user.edit', ['user'=>$user]);
        }
        return back()->with('error', trans('validation.user.edit'));
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $terms = new UserTerm();
        if($terms->isValidEdit($id))
        {
            $ci = $request['ci'];
            if($terms->isValidCiId($id, $ci))
            {
                $this->user->update($request, $id);
                return redirect()->route('admin.user.index');
            }
            return back()->withErrors(['ci'=>trans('validation.user.duplicate_ci')])
                ->withInput(request(['ci']));
        }
        return back()->with('error', trans('validation.user.edit'));
    }

    public function delete($id)
    {
        $terms = new UserTerm();
        if($terms->isValidDelete($id))
        {
            $user = $this->user->find($id);
            return view('admin.user.delete', ['user'=>$user]);
        }
        return back()->with('error', trans('validation.user.delete'));
    }

    public function destroy(DeleteRequest $request, $id)
    {
        $terms = new UserTerm();
        if($this->isValidDelete($request))
        {
            if($terms->isValidDelete($id))
            {
                $this->user->destroy($id);
                return redirect()->route('admin.user.index');
            }
            return back()->with('error', trans('validation.user.delete'));
        }
        return back()->withErrors(['key_delete'=>trans('validation.key_delete')])
            ->withInput(request(['key_delete']));
    }


    private function isValidDelete($request)
    {
        return $request['key_delete'] == 'ELIMINAR';
    }
}
