<?php

namespace App\Http\Controllers;

use App\Tipo_Egreso;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class InformeEgresoController extends Controller
{
    public function generar()
    {
        return view('informes.egreso.generar');
    }

    public function imprimir(Request $request)
    {
        $fecha_inicial = Carbon::parse($request['fecha_inicial']);
        $fecha_finalizacion = Carbon::parse($request['fecha_finalizacion']);

        $informes = [];
        $index = 0;

        while(($fecha_inicial->month <= $fecha_finalizacion->month && $fecha_inicial->year <= $fecha_finalizacion->year))
        {
            $mes = $this->getMes($fecha_inicial->month);
            $detalle_informe = [];


            $contenido = [];
            $index_contenido = 0;

            /**************************************** SALARIOS *****************************************/
            $egreso = DB::table('nota_egreso_sueldo')
                ->whereMonth('nota_egreso_sueldo.fecha_egreso', $fecha_inicial->month)
                ->whereYear('nota_egreso_sueldo.fecha_egreso', $fecha_inicial->year)
                ->get();
            $detalle = [];
            $detalle['titulo'] = 'SALARIOS';
            $detalle['total'] = array_sum(array_pluck($egreso, 'monto'));

            $contenido[$index_contenido] = $detalle;
            $index_contenido += 1;
            /**************************************** FIN SALARIOS *****************************************/



            /**************************************** OTROS EGRESOS *****************************************/
            $tipos = Tipo_Egreso::all();

            foreach ($tipos as $tipo)
            {
                $recibo = DB::table('nota_egreso')
                    ->whereMonth('nota_egreso.fecha_egreso', $fecha_inicial->month)
                    ->whereYear('nota_egreso.fecha_egreso', $fecha_inicial->year)
                    ->where('nota_egreso.id_tipo_egreso', '=', $tipo->id)
                    ->get();

                $detalle = [];
                $detalle['titulo'] = $tipo->nombre;
                $detalle['total'] = array_sum(array_pluck($recibo, 'monto'));

                $contenido[$index_contenido] = $detalle;
                $index_contenido += 1;
            }
            /**************************************** FIN OTROS INGRESOS *****************************************/


            $detalle_informe['mes'] = $mes;
            $detalle_informe['contenido'] = $contenido;

            $informes[$index] = $detalle_informe;
            $fecha_inicial->addMonth(1);
            $index += 1;
        }

        $totales = [];
        for($j = 0; $j < count($informes[0]['contenido']); $j++){
            $totales[$j] = 0;
        }

        foreach ($informes as $informe)
        {
            $i = 0;
            foreach ($informe['contenido'] as $cnd)
            {
                if($totales[$i] == null){
                    $totales[$i] = 0;
                }
                $totales[$i] += $cnd['total'];
                $i += 1;
            }
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('informes.egreso.pdf', ['egresos' => $informes, 'totales' => $totales, 'fecha_inicial'=>$request['fecha_inicial'], 'fecha_finalizacion'=>$request['fecha_finalizacion']]);
        $pdf->setPaper(\Dompdf\Adapter\CPDF::$PAPER_SIZES['legal'], 'landscape');
        $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        return $pdf->stream();
    }

    private function getMes($mes)
    {
        switch ($mes){
            case 1 : return 'ENERO';
            case 2 : return 'FEBRERO';
            case 3 : return 'MARZO';
            case 4 : return 'ABRIL';
            case 5 : return 'MAYO';
            case 6 : return 'JUNIO';
            case 7 : return 'JULIO';
            case 8 : return 'AGOSTO';
            case 9 : return 'SEPTIEMBRE';
            case 10 : return 'OCTUBRE';
            case 11 : return 'NOVIEMBRE';
            case 12 : return 'DICIEMBRE';
        }
    }
}
