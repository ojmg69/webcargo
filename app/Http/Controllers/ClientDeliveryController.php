<?php

namespace App\Http\Controllers;

use App\Delivery;
use App\Driver;
use App\Provider;
use App\ShoppingNote;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiClientDeliveryController extends Controller
{
    public function index(Request $request)
    {
        $client_id = $request['id'];

        $deliveries = DB::table('deliveries')
            ->join('shopping_notes', 'deliveries.shopping_note_id', '=', 'shopping_notes.id')
            ->where('shopping_notes.client_id', '=', $client_id)
            ->whereIn('deliveries.status', [100])
            ->select('deliveries.id', 'deliveries.shopping_note_id as shoppingNoteId', 'deliveries.car_id as carId')
            ->get();


        foreach ($deliveries as $delivery){

            $order = DB::table('deliveries')
                ->where('deliveries.id', '=', $delivery->id)
                ->join('cars', 'deliveries.car_id', '=', 'cars.id')
                ->join('shopping_notes', 'deliveries.shopping_note_id', '=', 'shopping_notes.id')
                ->select('shopping_notes.id', 'shopping_notes.business_name as businessName', 'shopping_notes.nit',
                    'shopping_notes.amount', 'shopping_notes.description', 'shopping_notes.sale_amount as saleAmount',
                    'shopping_notes.transportation_amount as transportationAmount', 'shopping_notes.total_amount as totalAmount', 'shopping_notes.status',
                    'shopping_notes.client_id as clientId',
                    'shopping_notes.provider_id as providerId', 'shopping_notes.created_at as createdAt', 'shopping_notes.updated_at as updatedAt',
                    'cars.id as carId')
                ->first();

            $client = DB::table('people')
                ->join('clients', 'people.id', '=', 'clients.people_id')
                ->where('clients.id', '=', $order->clientId)
                ->select('people.id', 'first_name as firstName', 'last_name as lastName', 'ci', 'gender', 'birthday',
                    'telephone', 'url_profile as urlProfile', 'status', 'clients.occupation')
                ->first();
            $order->client = $client;

            $provider = DB::table('providers')
                ->where('id', '=', $order->providerId)
                ->select('id', 'name', 'business_name as businessName', 'nit', 'url_image as urlImage', 'address', 'telephone',
                    'type', 'longitude', 'latitude')
                ->first();
            $order->provider = $provider;

            $delivery->order = $order;
        }
        return response()->json($deliveries);
    }

    public function pay(Request $request)
    {
        $client_id = $request['id'];

        $deliveries = DB::table('deliveries')
            ->join('shopping_notes', 'deliveries.shopping_note_id', '=', 'shopping_notes.id')
            ->where('shopping_notes.client_id', '=', $client_id)
            ->whereIn('deliveries.status', [101])
            ->select('deliveries.id', 'deliveries.shopping_note_id as shoppingNoteId', 'deliveries.car_id as carId')
            ->get();


        foreach ($deliveries as $delivery){

            $order = DB::table('deliveries')
                ->where('deliveries.id', '=', $delivery->id)
                ->join('cars', 'deliveries.car_id', '=', 'cars.id')
                ->join('shopping_notes', 'deliveries.shopping_note_id', '=', 'shopping_notes.id')
                ->select('shopping_notes.id', 'shopping_notes.business_name as businessName', 'shopping_notes.nit',
                    'shopping_notes.amount', 'shopping_notes.description', 'shopping_notes.sale_amount as saleAmount',
                    'shopping_notes.transportation_amount as transportationAmount', 'shopping_notes.total_amount as totalAmount', 'shopping_notes.status',
                    'shopping_notes.client_id as clientId',
                    'shopping_notes.provider_id as providerId', 'shopping_notes.created_at as createdAt', 'shopping_notes.updated_at as updatedAt',
                    'cars.id as carId')
                ->first();

            $client = DB::table('people')
                ->join('clients', 'people.id', '=', 'clients.people_id')
                ->where('clients.id', '=', $order->clientId)
                ->select('people.id', 'first_name as firstName', 'last_name as lastName', 'ci', 'gender', 'birthday',
                    'telephone', 'url_profile as urlProfile', 'status', 'clients.occupation')
                ->first();
            $order->client = $client;

            $provider = DB::table('providers')
                ->where('id', '=', $order->providerId)
                ->select('id', 'name', 'business_name as businessName', 'nit', 'url_image as urlImage', 'address', 'telephone',
                    'type', 'longitude', 'latitude')
                ->first();
            $order->provider = $provider;

            $delivery->order = $order;
        }
        return response()->json($deliveries);
    }

    public function paying(Request $request)
    {
        $delivery = Delivery::findOrFail($request['id']);
        $delivery->status = 102;
        $delivery->save();

        $order = ShoppingNote::findOrFail($request['shoppingNoteId']);
        $order->status = 103;
        $order->save();

        //mandar notificaciones al driver
        //registrar trigger que cambie el status del driver
        $this->notify($order, $delivery);

        $order;

        return response()->json(['data'=>$order]);
    }

    public function history(Request $request)
    {
        $client_id = $request['id'];

        $deliveries = DB::table('deliveries')
            ->join('shopping_notes', 'deliveries.shopping_note_id', '=', 'shopping_notes.id')
            ->where('shopping_notes.client_id', '=', $client_id)
            ->whereIn('deliveries.status', [112, 200, 0])
            ->select('deliveries.id', 'deliveries.shopping_note_id as shoppingNoteId', 'deliveries.car_id as carId')
            ->get();


        foreach ($deliveries as $delivery){

            $order = DB::table('deliveries')
                ->where('deliveries.id', '=', $delivery->id)
                ->join('cars', 'deliveries.car_id', '=', 'cars.id')
                ->join('shopping_notes', 'deliveries.shopping_note_id', '=', 'shopping_notes.id')
                ->select('shopping_notes.id', 'shopping_notes.business_name as businessName', 'shopping_notes.nit',
                    'shopping_notes.amount', 'shopping_notes.description', 'shopping_notes.sale_amount as saleAmount',
                    'shopping_notes.transportation_amount as transportationAmount', 'shopping_notes.total_amount as totalAmount', 'shopping_notes.status',
                    'shopping_notes.client_id as clientId',
                    'shopping_notes.provider_id as providerId', 'shopping_notes.created_at as createdAt', 'shopping_notes.updated_at as updatedAt',
                    'cars.id as carId')
                ->first();

            $client = DB::table('people')
                ->join('clients', 'people.id', '=', 'clients.people_id')
                ->where('clients.id', '=', $order->clientId)
                ->select('people.id', 'first_name as firstName', 'last_name as lastName', 'ci', 'gender', 'birthday',
                    'telephone', 'url_profile as urlProfile', 'status', 'clients.occupation')
                ->first();
            $order->client = $client;

            $provider = DB::table('providers')
                ->where('id', '=', $order->providerId)
                ->select('id', 'name', 'business_name as businessName', 'nit', 'url_image as urlImage', 'address', 'telephone',
                    'type', 'longitude', 'latitude')
                ->first();
            $order->provider = $provider;

            $delivery->order = $order;
        }
        return response()->json($deliveries);
    }

    private function notify(Model $note, Model $delivery)
    {
        $provider = Provider::findOrFail($note->provider_id);
        $driver = Driver::findOrFail($delivery->car->driver_id);

        $notification = array(
            "title" => "El pedido esta listo para su carga y envio al destinatario",
            "body" => $note->business_name
        );
        $message = array("message" => "Message from serve", "customKey" => "customValue");
        $url = "https://fcm.googleapis.com/fcm/send ";
        $fields = array(
            "registration_ids" => [$provider->uuid_notify, $driver->people->uuid_notify],
            "notification" => $notification,
            "data" => $message
        );
        $header = array(
            'Content-Type: application/json',
            'Authorization: key=AAAATyzl3yg:APA91bFGUDFy1VaVz38l-p8fjevTRnSQ8_ScQlzin46etSzkNk0i4FHzXzJD3D71n0FuD-AGiv3sy82Ev1niXUkplXX5n3jFNeZ_CbQkUdFRBAkX0E0pPh5rMS13GyrBg1_5ExiUJHLJ'
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_exec($ch);
        curl_close($ch);

    }
}
