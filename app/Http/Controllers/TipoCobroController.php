<?php

namespace App\Http\Controllers;

use App\Classes\MessageError;
use App\Http\Requests\DeleteRequest;
use App\Http\Requests\TipoCobroRequest;
use App\Tipo_Cobro;
use Illuminate\Http\Request;

class TipoCobroController extends Controller
{

    private $mensaje_error;

    public function __construct()
    {
        $this->mensaje_error = new MessageError();
    }

    public function index()
    {
        $tipos = Tipo_Cobro::all();
        return view('gestionar_tipo_cobro.index', ['tipos'=>$tipos]);
    }

    public function ver($id)
    {
        $tipo = Tipo_Cobro::findOrFail($id);
        return view('gestionar_tipo_cobro.detalle', ['tipo'=>$tipo]);
    }

    public function registrar()
    {
        return view('gestionar_tipo_cobro.registrar');
    }

    public function guardar(TipoCobroRequest $request)
    {
        $tipo = new Tipo_Cobro($request->all());
        $tipo->id_usuario = auth()->user()->persona()->id;
        $tipo->save();
        return redirect()->route('admin.tipocobro.ver', [$tipo->id]);
    }

    public function editar($id)
    {
        $tipo = Tipo_Cobro::findOrFail($id);
        if($tipo->present()->sePuedeModificar())
        {
            return view('gestionar_tipo_cobro.editar', ['tipo'=>$tipo]);
        }
        $error = $this->mensaje_error->getMessageError(MessageError::ERROR_MODIFICAR_TIPO_COBRO);
        return view('layout-error', ['error'=>$error]);
    }

    public function modificar(Request $request, $id)
    {
        $tipo = Tipo_Cobro::findOrFail($id);
        if($tipo->present()->sePuedeModificar())
        {
            $tipo->motivo = $request['motivo'];
            $tipo->descripcion = $request['descripcion'];
            $tipo->monto = $request['monto'];
            $tipo->id_usuario = auth()->user()->persona()->id;
            return redirect()->route('admin.tipocobro.index');
        }
        $error = $this->mensaje_error->getMessageError(MessageError::ERROR_MODIFICAR_TIPO_COBRO);
        return view('layout-error', ['error'=>$error]);
    }

    public function eliminar($id)
    {
        $cobro = Tipo_Cobro::findOrFail($id);
        if($cobro->present()->sePuedeEliminar())
        {
            return view('gestionar_tipo_cobro.eliminar', ['cobro'=>$cobro]);
        } else {
            $error = $this->mensaje_error->getDeleteError(MessageError::ERROR_ELIMINAR_TIPO_COBRO);
            return view('layout-error', ['error'=>$error]);
        }
    }

    public function destroy(DeleteRequest $request, $id)
    {
        if($request['key_eliminar'] == 'ELIMINAR')
        {
            $cobro = Tipo_Cobro::findOrFail($id);
            if($cobro->present()->sePuedeEliminar())
            {
                $cobro->delete();
                return redirect()->route('admin.tipocobro.index');
            } else {
                $error = $this->mensaje_error->getDeleteError(MessageError::ERROR_ELIMINAR_TIPO_COBRO);
                return view('layout-error', ['error'=>$error]);
            }
        } else {
            return redirect()->route('admin.tipocobro.eliminar', [$id]);
        }
    }
}
