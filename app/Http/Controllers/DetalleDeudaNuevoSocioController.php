<?php

namespace App\Http\Controllers;

use App\Classes\MessageError;
use App\Detalle_Deuda_Nuevo_Socio;
use App\Persona;
use Illuminate\Support\Facades\DB;

class DetalleDeudaNuevoSocioController extends Controller
{
    private $messageError;

    public function __construct()
    {
        $this->messageError = new MessageError();
    }

    public function index($id, $id_socio){
        $socio = Persona::findOrFail($id_socio);
        $cobros = Detalle_Deuda_Nuevo_Socio::where('detalle_deuda_nuevo_socio.id_deuda_nuevo_socio', '=', $id)->get();
        return view('socio.deuda.detalle_deuda_nuevo_socio', ['cobros'=>$cobros, 'socio' => $socio]);
    }
}

