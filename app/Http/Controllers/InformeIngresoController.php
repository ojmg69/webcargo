<?php

namespace App\Http\Controllers;

use App\Cobro_Adicional;
use App\Documento;
use App\Servicio_Basico;
use App\Tipo_Ingreso;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use SplFixedArray;

class InformeIngresoController extends Controller
{
    public function generar()
    {
        return view('informes.ingreso.generar');
    }

    public function imprimir(Request $request)
    {
        $fecha_inicial = Carbon::parse($request['fecha_inicial']);
        $fecha_finalizacion = Carbon::parse($request['fecha_finalizacion']);

        $informes = [];
        $index = 0;

        while(($fecha_inicial->month <= $fecha_finalizacion->month && $fecha_inicial->year <= $fecha_finalizacion->year))
        {
            $mes = $this->getMes($fecha_inicial->month);
            $detalle_informe = [];


            $contenido = [];
            $index_contenido = 0;

            /**************************************** SERVICIOS BASICOS *****************************************/
            $servicos = Servicio_Basico::all();
            foreach ($servicos as $servico)
            {
                $recibo = DB::table('recibo_servicio_basico')
                    ->whereMonth('recibo_servicio_basico.fecha_recibo', $fecha_inicial->month)
                    ->whereYear('recibo_servicio_basico.fecha_recibo', $fecha_inicial->year)
                    ->join('deuda_servicio_basico', 'recibo_servicio_basico.id_deuda_servicio_basico', '=', 'deuda_servicio_basico.id')
                    ->join('contrato_servicio_basico', 'deuda_servicio_basico.id_contrato_servicio_basico', '=', 'contrato_servicio_basico.id')
                    ->where('contrato_servicio_basico.id_servicio_basico', '=', $servico->id)
                    ->get();
                $detalle = [];
                $detalle['titulo'] = $servico->nombre;
                $detalle['total'] = array_sum(array_pluck($recibo, 'monto'));

                $contenido[$index_contenido] = $detalle;
                $index_contenido += 1;
            }
            /**************************************** FIN SERVICIOS BASICOS *****************************************/

            /**************************************** AFILIACION *****************************************/
            $recibo = DB::table('recibo_deuda_nuevo_socio')
                ->whereMonth('recibo_deuda_nuevo_socio.fecha_recibo', $fecha_inicial->month)
                ->whereYear('recibo_deuda_nuevo_socio.fecha_recibo', $fecha_inicial->year)
                ->get();

            $detalle = [];
            $detalle['titulo'] = 'AFILIACION';
            $detalle['total'] = array_sum(array_pluck($recibo, 'monto'));

            $contenido[$index_contenido] = $detalle;
            $index_contenido += 1;

            /**************************************** FIN AFILIACION *****************************************/

            /**************************************** EVENTO *****************************************/
            $recibo = DB::table('recibo_contrato_evento')
                ->whereMonth('recibo_contrato_evento.fecha_recibo', $fecha_inicial->month)
                ->whereYear('recibo_contrato_evento.fecha_recibo', $fecha_inicial->year)
                ->get();

            $detalle = [];
            $detalle['titulo'] = 'FERIAS';
            $detalle['total'] = array_sum(array_pluck($recibo, 'monto'));

            $contenido[$index_contenido] = $detalle;
            $index_contenido += 1;
            /**************************************** FIN EVENTO *****************************************/

            /**************************************** MULTAS *****************************************/
            $recibo = DB::table('recibo_asignacion_multa')
                ->whereMonth('recibo_asignacion_multa.fecha_recibo', $fecha_inicial->month)
                ->whereYear('recibo_asignacion_multa.fecha_recibo', $fecha_inicial->year)
                ->get();

            $detalle = [];
            $detalle['titulo'] = 'MULTAS';
            $detalle['total'] = array_sum(array_pluck($recibo, 'monto'));

            $contenido[$index_contenido] = $detalle;
            $index_contenido += 1;
            /**************************************** FIN MULTAS *****************************************/


            /**************************************** OTROS COBROS *****************************************/
            $cobros = Cobro_Adicional::all();

            foreach ($cobros as $cobro)
            {
                $recibo = DB::table('recibo_cobro_adicional')
                    ->whereMonth('recibo_cobro_adicional.fecha_recibo', $fecha_inicial->month)
                    ->whereYear('recibo_cobro_adicional.fecha_recibo', $fecha_inicial->year)
                    ->join('deuda_cobro_adicional', 'recibo_cobro_adicional.id_deuda_cobro_adicional', '=', 'deuda_cobro_adicional.id')
                    ->join('gestion_cobro_adicional', 'deuda_cobro_adicional.id_gestion_cobro_adicional', '=', 'gestion_cobro_adicional.id')
                    ->where('gestion_cobro_adicional.id_cobro_adicional', '=', $cobro->id)
                    ->get();

                $detalle = [];
                $detalle['titulo'] = $cobro->nombre;
                $detalle['total'] = array_sum(array_pluck($recibo, 'monto'));

                $contenido[$index_contenido] = $detalle;
                $index_contenido += 1;
            }

            /**************************************** FIN OTROS COBROS *****************************************/


            /**************************************** OTROS INGRESOS *****************************************/
            $tipos = Tipo_Ingreso::all();

            foreach ($tipos as $tipo)
            {
                $recibo = DB::table('recibo_ingreso')
                    ->whereMonth('recibo_ingreso.fecha_recibo', $fecha_inicial->month)
                    ->whereYear('recibo_ingreso.fecha_recibo', $fecha_inicial->year)
                    ->where('recibo_ingreso.id_tipo_ingreso', '=', $tipo->id)
                    ->get();

                $detalle = [];
                $detalle['titulo'] = $tipo->nombre;
                $detalle['total'] = array_sum(array_pluck($recibo, 'monto'));

                $contenido[$index_contenido] = $detalle;
                $index_contenido += 1;
            }
            /**************************************** FIN OTROS INGRESOS *****************************************/

            /**************************************** CERTIFICADOS *****************************************/
            $documentos = Documento::all();

            foreach ($documentos as $documento)
            {
                $recibo = DB::table('recibo_documento')
                    ->whereMonth('recibo_documento.fecha_recibo', $fecha_inicial->month)
                    ->whereYear('recibo_documento.fecha_recibo', $fecha_inicial->year)
                    ->where('recibo_documento.id_documento', '=', $documento->id)
                    ->get();

                $detalle = [];
                $detalle['titulo'] = $documento->nombre;
                $detalle['total'] = array_sum(array_pluck($recibo, 'monto'));

                $contenido[$index_contenido] = $detalle;
                $index_contenido += 1;
            }
            /**************************************** FIN CERTIFICADOS *****************************************/


            $detalle_informe['mes'] = $mes;
            $detalle_informe['contenido'] = $contenido;

            $informes[$index] = $detalle_informe;
            $fecha_inicial->addMonth(1);
            $index += 1;
        }

        $totales = [];
        for($j = 0; $j < count($informes[0]['contenido']); $j++){
            $totales[$j] = 0;
        }

        foreach ($informes as $informe)
        {
            $i = 0;
            foreach ($informe['contenido'] as $cnd)
            {
                if($totales[$i] == null){
                    $totales[$i] = 0;
                }
                $totales[$i] += $cnd['total'];
                $i += 1;
            }
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('informes.ingreso.pdf', ['ingresos' => $informes, 'totales' => $totales, 'fecha_inicial'=>$request['fecha_inicial'], 'fecha_finalizacion'=>$request['fecha_finalizacion']]);
        $pdf->setPaper(\Dompdf\Adapter\CPDF::$PAPER_SIZES['legal'], 'landscape');
        $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        return $pdf->stream();
    }

    private function getMes($mes)
    {
        switch ($mes){
            case 1 : return 'ENERO';
            case 2 : return 'FEBRERO';
            case 3 : return 'MARZO';
            case 4 : return 'ABRIL';
            case 5 : return 'MAYO';
            case 6 : return 'JUNIO';
            case 7 : return 'JULIO';
            case 8 : return 'AGOSTO';
            case 9 : return 'SEPTIEMBRE';
            case 10 : return 'OCTUBRE';
            case 11 : return 'NOVIEMBRE';
            case 12 : return 'DICIEMBRE';
        }
    }
}
