<?php

namespace App\Http\Controllers;

use App\Repositories\PermissionRepository;
use App\Repositories\UserPermissionRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserPermissionController extends Controller
{
    private $permission;
    private $user;
    private $user_permission;

    public function __construct(PermissionRepository $permission, UserRepository $user, UserPermissionRepository $user_permission)
    {
        $this->permission = $permission;
        $this->user = $user;
        $this->user_permission = $user_permission;
    }

    public function index()
    {
        $users = $this->user->index();
        return view('admin.permission_user.index', ['users'=>$users]);
    }

    public function show($administrator_id)
    {
        $administrator = $this->user->find($administrator_id);
        $permissions = $this->permission->index();

        $administrator_permissions = $this->user_permission->getPermissionId($administrator->people->user->id);
        foreach ($permissions as $permission)
        {
            $have = false;
            foreach($administrator_permissions as $administrator_permission)
            {
                if($permission->id == $administrator_permission->permission_id)
                {
                    $have = true;
                    break;
                }
            }
            $permission->administrator = $have;
        }

        return view('admin.permission_user.show', ['user' => $administrator, 'permissions' => $permissions]);
    }

    public function store(Request $request, $administrator_id)
    {
        $administrator = $this->user->find($administrator_id);
        $permission_ids = $request['permission_ids'];
        $user_id = $administrator->people->user->id;

        $this->user_permission->destroyAllPermission($user_id);

        foreach ($permission_ids as $permission_id)
        {
            $this->user_permission->store($user_id, $permission_id);
        }
        return redirect()->route('admin.permission_user.index');
    }

}
