<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiClientCommentController extends Controller
{
    public function index(Request $request)
    {
        $service_id = $request['service_id'];

        $comments = DB::table('comments')
            ->where('service_id', '=', $service_id)
            ->join('clients', 'comments.client_id', '=', 'clients.id')
            ->join('people', 'clients.people_id', '=', 'people.id')
            ->select('comments.id', 'comments.title', 'comments.description', 'comments.rate', 'people.first_name', 'people.last_name', 'people.url_profile',
                'comments.updated_at as date')
            ->get();

        $result['data'] = $comments;
        return response()->json($result);
    }

    public function storeOrUpdate(Request $request)
    {
        $comment = $this->findCommentId($request);
        if($comment != null)
        {
            $this->update($request, $comment);
        } else {
            $this->store($request);
        }
        $result['data'] = ['message'=>'success'];
        return response()->json($result);
    }

    private function store($request)
    {
        $comment = new Comment($request->all());
        $comment->save();
    }

    private function update($request, $comment)
    {
        $comment = Comment::findOrFail($comment->id);
        $comment->title = $request['title'];
        $comment->description = $request['description'];
        $comment->rate = $request['rate'];
        $comment->save();
    }

    private function findCommentId($request)
    {
        return DB::table('comments')
            ->where('client_id', '=', $request['client_id'])
            ->where('service_id', '=', $request['service_id'])
            ->select('id')
            ->first();
    }
}
