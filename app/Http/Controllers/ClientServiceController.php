<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiClientServiceController extends Controller
{

    public function index()
    {
        $services = DB::table('services')
            ->select('id', 'url_image', 'name as business_name')
            ->get();
        $result['data'] = $services;
        return response()->json($result);
    }

    public function show(Request $request)
    {
        $client_id = $request['client_id'];
        $service_id = $request['service_id'];
        $this->update($service_id);

        $service = DB::table('services')
            ->where('id', '=', $service_id)
            ->select('id', 'name', 'description', 'branch_office', 'type_business', 'contacts', 'whatsaap', 'count_visit',
                'facebook', 'instagram', 'url_image')
            ->first();

        $service->comment = DB::table('comments')
            ->where('service_id', '=', $service_id)
            ->where('client_id', '=', $client_id)
            ->first();
        $result['data'] = $service;
        return response()->json($result);
    }

    public function update($service_id)
    {
        $service = Service::findOrFail($service_id);
        $service->count_visit = $service->count_visit + 1;
        $service->save();
    }
}
