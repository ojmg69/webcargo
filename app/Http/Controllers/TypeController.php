<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
class TypeController extends Controller
{
    public function index()

    {
        $fecha=date("Y-m-d H:i:s");
        $types = DB::table('type_vehicles')->select('id','name','url_image','category_id')->orderBy('id')->get();
        return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
            "fecha_hora"=>$fecha,"data"=>$types],200);
    }

    public function show(Request $request)
    {
        $fecha=date("Y-m-d H:i:s");
        $types = DB::table('type_vehicles')
        ->where('category_id', '=', $request->categoria_id)
         ->select('id','name','description','url_image')
        ->get();
        return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
            "fecha_hora"=>$fecha,"data"=>$types],200);
    }
}
