<?php

namespace App\Http\Controllers;

use App\Cobro_Adicional;
use App\Servicio_Basico;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class InformeDeudaController extends Controller
{
    public function generar()
    {
        return view('informes.deuda.generar');
    }

    public function imprimir(Request $request)
    {
        $fecha_inicial = Carbon::parse($request['fecha_inicial']);
        $fecha_finalizacion = Carbon::parse($request['fecha_finalizacion']);

        $informes = [];
        $index = 0;


        while(($fecha_inicial->month <= $fecha_finalizacion->month && $fecha_inicial->year <= $fecha_finalizacion->year))
        {
            $mes = $this->getMes($fecha_inicial->month);
            $detalle_informe = [];


            $contenido = [];
            $index_contenido = 0;

            /**************************************** SERVICIOS BASICOS *****************************************/
            $servicos = Servicio_Basico::all();
            $ids_deudas = DB::table('recibo_servicio_basico')
                ->join('deuda_servicio_basico', 'recibo_servicio_basico.id_deuda_servicio_basico', '=', 'deuda_servicio_basico.id')
                ->whereMonth('deuda_servicio_basico.fecha_deuda', $fecha_inicial->month)
                ->whereYear('deuda_servicio_basico.fecha_deuda', $fecha_inicial->year)
                ->pluck('id_deuda_servicio_basico');
            foreach ($servicos as $servico)
            {
                $deudas = DB::table('deuda_servicio_basico')
                    ->whereNotIn('deuda_servicio_basico.id', $ids_deudas)
                    ->whereMonth('deuda_servicio_basico.fecha_deuda', $fecha_inicial->month)
                    ->whereYear('deuda_servicio_basico.fecha_deuda', $fecha_inicial->year)
                    ->join('contrato_servicio_basico', 'deuda_servicio_basico.id_contrato_servicio_basico', '=', 'contrato_servicio_basico.id')
                    ->where('contrato_servicio_basico.id_servicio_basico', '=', $servico->id)
                    ->get();
                $detalle = [];
                $detalle['titulo'] = $servico->nombre;
                $detalle['total'] = array_sum(array_pluck($deudas, 'monto'));

                $contenido[$index_contenido] = $detalle;
                $index_contenido += 1;
            }
            /**************************************** FIN SERVICIOS BASICOS *****************************************/

            /**************************************** AFILIACION *****************************************/
            $ids_deudas = DB::table('recibo_deuda_nuevo_socio')
                ->join('deuda_nuevo_socio', 'recibo_deuda_nuevo_socio.id_deuda_nuevo_socio', '=', 'deuda_nuevo_socio.id')
                ->select('deuda_nuevo_socio.id as id', DB::raw('SUM(recibo_deuda_nuevo_socio.monto) as total'))
                ->groupBy('deuda_nuevo_socio.id')
                ->havingRaw('SUM(recibo_deuda_nuevo_socio.monto) >= deuda_nuevo_socio.monto')
                ->get();

            $ids_recibos = DB::table('recibo_deuda_nuevo_socio')
                ->groupBy('recibo_deuda_nuevo_socio.id_deuda_nuevo_socio')
                ->pluck('recibo_deuda_nuevo_socio.id_deuda_nuevo_socio');


            $afiliacion_deuda = DB::table('deuda_nuevo_socio')
                ->whereNotIn('deuda_nuevo_socio.id', array_pluck($ids_deudas, 'id'))
                ->whereMonth('deuda_nuevo_socio.fecha_deuda', $fecha_inicial->month)
                ->whereYear('deuda_nuevo_socio.fecha_deuda', $fecha_inicial->year)
                ->join('persona', 'deuda_nuevo_socio.id_socio', '=', 'persona.id')
                ->join('recibo_deuda_nuevo_socio', 'deuda_nuevo_socio.id', '=', 'recibo_deuda_nuevo_socio.id_deuda_nuevo_socio')
                ->select('persona.apellidos', 'persona.nombre', DB::raw('deuda_nuevo_socio.monto - SUM(recibo_deuda_nuevo_socio.monto) as monto'), 'deuda_nuevo_socio.fecha_deuda as fecha')
                ->groupBy('deuda_nuevo_socio.id', 'persona.id')
                ->get();
            $afiliacion = DB::table('deuda_nuevo_socio')
                ->whereNotIn('deuda_nuevo_socio.id', $ids_recibos)
                ->whereMonth('deuda_nuevo_socio.fecha_deuda', $fecha_inicial->month)
                ->whereYear('deuda_nuevo_socio.fecha_deuda', $fecha_inicial->year)
                ->join('persona', 'deuda_nuevo_socio.id_socio', '=', 'persona.id')
                ->select('persona.apellidos', 'persona.nombre', 'deuda_nuevo_socio.monto', 'deuda_nuevo_socio.fecha_deuda as fecha')
                ->get();
            $afiliacion = $afiliacion->merge($afiliacion_deuda);

            $detalle = [];
            $detalle['titulo'] = 'AFILIACION';
            $detalle['total'] = array_sum(array_pluck($afiliacion, 'monto'));

            $contenido[$index_contenido] = $detalle;
            $index_contenido += 1;

            /**************************************** FIN AFILIACION *****************************************/

            /**************************************** EVENTO *****************************************/
            $ids_deudas = DB::table('recibo_contrato_evento')
                ->join('deuda_contrato_evento', 'recibo_contrato_evento.id_deuda_contrato_evento', '=', 'deuda_contrato_evento.id')
                ->whereMonth('deuda_contrato_evento.fecha_deuda', $fecha_inicial->month)
                ->whereYear('deuda_contrato_evento.fecha_deuda', $fecha_inicial->year)
                ->pluck('deuda_contrato_evento.id');

            $deudas = DB::table('deuda_contrato_evento')
                ->whereNotIn('id', $ids_deudas)
                ->whereMonth('deuda_contrato_evento.fecha_deuda', $fecha_inicial->month)
                ->whereYear('deuda_contrato_evento.fecha_deuda', $fecha_inicial->year)
                ->get();

            $detalle = [];
            $detalle['titulo'] = 'FERIAS';
            $detalle['total'] = array_sum(array_pluck($deudas, 'monto'));

            $contenido[$index_contenido] = $detalle;
            $index_contenido += 1;
            /**************************************** FIN EVENTO *****************************************/

            /**************************************** MULTAS *****************************************/
            $ids_recibos = DB::table('recibo_asignacion_multa')
                ->join('asignacion_multa', 'recibo_asignacion_multa.id_asignacion_multa', '=', 'asignacion_multa.id')
                ->whereMonth('asignacion_multa.fecha_deuda', $fecha_inicial->month)
                ->whereYear('asignacion_multa.fecha_deuda', $fecha_inicial->year)
                ->pluck('asignacion_multa.id');

            $deudas = DB::table('asignacion_multa')
                ->whereNotIn('id', $ids_recibos)
                ->whereMonth('asignacion_multa.fecha_deuda', $fecha_inicial->month)
                ->whereYear('asignacion_multa.fecha_deuda', $fecha_inicial->year)
                ->get();

            $detalle = [];
            $detalle['titulo'] = 'MULTAS';
            $detalle['total'] = array_sum(array_pluck($deudas, 'monto'));

            $contenido[$index_contenido] = $detalle;
            $index_contenido += 1;
            /**************************************** FIN MULTAS *****************************************/

            /**************************************** OTROS COBROS *****************************************/
            $cobros = Cobro_Adicional::all();

            foreach ($cobros as $cobro)
            {
                $ids_recibos = DB::table('recibo_cobro_adicional')
                    ->join('deuda_cobro_adicional', 'recibo_cobro_adicional.id_deuda_cobro_adicional', '=', 'deuda_cobro_adicional.id')
                    ->join('gestion_cobro_adicional', 'deuda_cobro_adicional.id_gestion_cobro_adicional', '=', 'gestion_cobro_adicional.id')
                    ->whereMonth('deuda_cobro_adicional.fecha_deuda', $fecha_inicial->month)
                    ->whereYear('deuda_cobro_adicional.fecha_deuda', $fecha_inicial->year)
                    ->where('gestion_cobro_adicional.id_cobro_adicional', '=', $cobro->id)
                    ->pluck('deuda_cobro_adicional.id');

                $deudas = DB::table('deuda_cobro_adicional')
                    ->whereNotIn('deuda_cobro_adicional.id', $ids_recibos)
                    ->join('gestion_cobro_adicional', 'deuda_cobro_adicional.id_gestion_cobro_adicional', '=', 'gestion_cobro_adicional.id')
                    ->whereMonth('deuda_cobro_adicional.fecha_deuda', $fecha_inicial->month)
                    ->whereYear('deuda_cobro_adicional.fecha_deuda', $fecha_inicial->year)
                    ->where('gestion_cobro_adicional.id_cobro_adicional', '=', $cobro->id)
                    ->get();

                $detalle = [];
                $detalle['titulo'] = $cobro->nombre;
                $detalle['total'] = array_sum(array_pluck($deudas, 'monto'));

                $contenido[$index_contenido] = $detalle;
                $index_contenido += 1;
            }

            /**************************************** FIN OTROS COBROS *****************************************/

            $detalle_informe['mes'] = $mes;
            $detalle_informe['contenido'] = $contenido;

            $informes[$index] = $detalle_informe;
            $fecha_inicial->addMonth(1);
            $index += 1;
        }

        $totales = [];
        for($j = 0; $j < count($informes[0]['contenido']); $j++){
            $totales[$j] = 0;
        }

        foreach ($informes as $informe)
        {
            $i = 0;
            foreach ($informe['contenido'] as $cnd)
            {
                if($totales[$i] == null){
                    $totales[$i] = 0;
                }
                $totales[$i] += $cnd['total'];
                $i += 1;
            }
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('informes.deuda.pdf', ['deudas' => $informes, 'totales' => $totales, 'fecha_inicial'=>$request['fecha_inicial'], 'fecha_finalizacion'=>$request['fecha_finalizacion']]);
        $pdf->setPaper(\Dompdf\Adapter\CPDF::$PAPER_SIZES['legal'], 'landscape');
        $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        return $pdf->stream();
    }

    private function getMes($mes)
    {
        switch ($mes){
            case 1 : return 'ENERO';
            case 2 : return 'FEBRERO';
            case 3 : return 'MARZO';
            case 4 : return 'ABRIL';
            case 5 : return 'MAYO';
            case 6 : return 'JUNIO';
            case 7 : return 'JULIO';
            case 8 : return 'AGOSTO';
            case 9 : return 'SEPTIEMBRE';
            case 10 : return 'OCTUBRE';
            case 11 : return 'NOVIEMBRE';
            case 12 : return 'DICIEMBRE';
        }
    }
}
