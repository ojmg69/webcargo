<?php

namespace App\Http\Controllers;
use App\Color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;


class ApiColorController extends Controller
{
    public function index()

    {
        $colors = DB::table('colors')->select('id','name')->orderBy('id')->get();
        return response()->json($colors,200);
    }
    public function store($request)
    {
        $color = new Color($request->all());
        $color->administrator_id = auth()->user()->people->administrator->id;
        $color->save();
        return $color;
    }
    public function find($id)
    {
        $color = Color::findOrFail($id);
        return $color;
    }

    public function update($request, $id)
    {
        $color = Color::findOrFail($id);
        $color->name = $request['name'];
        $color->administrator_id = auth()->user()->people->administrator->id;
        $color->save();
        return $color;
    }

    public function destroy($id)
    {
        $color = Color::findOrFail($id);
        return $color->delete();
    }
}
