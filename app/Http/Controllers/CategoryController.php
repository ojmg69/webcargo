<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class CategoryController extends Controller
{
    public function index()

    {
        $fecha=date("Y-m-d H:i:s");
        $types = DB::table('categories')->select('id','name','url_image')->orderBy('id')->get();
        return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
            "fecha_hora"=>$fecha,"data"=>$types],200);
    }
}
