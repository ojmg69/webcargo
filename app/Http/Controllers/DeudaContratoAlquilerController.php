<?php

namespace App\Http\Controllers;

use App\Classes\MessageError;
use App\Deuda_Contrato_Alquiler;
use Illuminate\Http\Request;

class DeudaContratoAlquilerController extends Controller
{

    private $mensaje_error;

    public function __construct()
    {
        $this->mensaje_error = new MessageError();
    }

    public function editar($id)
    {
        $deuda = Deuda_Contrato_Alquiler::findOrFail($id);
        if($deuda->present()->sePuedeModificar())
        {
            return view('gestionar_alquiler.gestionar_deuda.editar', ['deuda'=>$deuda]);
        } else {
            $error = $this->mensaje_error->getMessageError(MessageError::ERROR_MODIFICAR_DEUDA_ALQUILER);
            return view('layout-error', ['error'=>$error]);
        }
    }

    public function modificar(Request $request, $id)
    {
        $validado = $request->validate([
            'monto'=>'required'
        ]);

        $deuda = Deuda_Contrato_Alquiler::findOrFail($id);
        if($deuda->present()->sePuedeModificar())
        {
            $deuda->monto = $validado['monto'];
            $deuda->save();
            return redirect()->route('admin.alquiler.ver', [$deuda->contrato_alquiler->id]);
        } else {
            $error = $this->mensaje_error->getMessageError(MessageError::ERROR_MODIFICAR_DEUDA_ALQUILER);
            return view('layout-error', ['error'=>$error]);
        }
    }
}
