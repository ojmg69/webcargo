<?php

namespace App\Http\Controllers;

use App\OrderImage;
use Illuminate\Http\Request;

class OrderImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderImage  $orderImage
     * @return \Illuminate\Http\Response
     */
    public function show(OrderImage $orderImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderImage  $orderImage
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderImage $orderImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderImage  $orderImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderImage $orderImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderImage  $orderImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderImage $orderImage)
    {
        //
    }
}
