<?php

namespace App\Http\Controllers;

use App\Delivery;
use App\Driver;
use App\Provider;
use App\ShoppingNote;
use App\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PaymentLiveesController extends Controller
{

    public function payment($delivery_id)
    {

        $delivery = Delivery::findOrFail($delivery_id);
        $shopping_note = ShoppingNote::findOrfail($delivery->shopping_note_id);
        $client = Client::findOrFail($shopping_note->client_id);


        $amt2 = $shopping_note->total_amount;
        $name = $client->people->first_name;
        $lastname = $client->people->last_name;
        $phone = $client->people->phone;
        $invno = $delivery->id;

        return view('payment.form', ['amt2'=>1, 'name'=>$name, 'lastname'=>$lastname, 'phone'=>$phone, 'invno'=>$invno]);
    }

    public function confirmed(Request $request)
    {
        $result = $request['result'];
        $order_id = $request['order_id'];

        if($result != null & $order_id != null)
        {
            $delivery = Delivery::findOrFail($result);
            $delivery->status = 102;
            $delivery->save();
            $shopping_note = ShoppingNote::findOrfail($delivery->shopping_note_id);
            $shopping_note->status =  103;
            $shopping_note->save();

            $this->notify($shopping_note, $delivery);
            return view('payment.confirmed', ['result'=>$result, 'order_id'=>$order_id, 'success'=>true]);
        } else {
            return view('payment.deneged', ['result'=>$result, 'order_id'=>$order_id, 'success'=>false]);
        }
    }

    private function notify(Model $note, Model $delivery)
    {
        $provider = Provider::findOrFail($note->provider_id);
        $driver = Driver::findOrFail($delivery->car->driver_id);

        $notification = array(
            "title" => "El pedido esta listo para su carga y envio al destinatario",
            "body" => $note->business_name
        );
        $message = array("message" => "Message from serve", "customKey" => "customValue");
        $url = "https://fcm.googleapis.com/fcm/send ";
        $fields = array(
            "registration_ids" => [$provider->uuid_notify, $driver->people->uuid_notify],
            "notification" => $notification,
            "data" => $message
        );
        $header = array(
            'Content-Type: application/json',
            'Authorization: key=AAAATyzl3yg:APA91bFGUDFy1VaVz38l-p8fjevTRnSQ8_ScQlzin46etSzkNk0i4FHzXzJD3D71n0FuD-AGiv3sy82Ev1niXUkplXX5n3jFNeZ_CbQkUdFRBAkX0E0pPh5rMS13GyrBg1_5ExiUJHLJ'
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_exec($ch);
        curl_close($ch);

    }
}
