<?php

namespace App\Http\Controllers;


use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Order;
use App\OrderImage;

class ClientOrderController extends Controller
{
    public function index()
    {
        $fecha = date("Y-m-d H:i:s");
        return response()->json([
            "code" => 0, "mensaje" => "Ha ocurrido un error",
            "fecha_hora" => $fecha, "data" => 'true'
        ], 200);
    }


    public function store(Request $request)
    {
        $rules = [
            'description'           => 'nullable|string',
            'total_price'           => 'required|numeric',
            'locate_origin'         => 'required',
            'locate_destination'    => 'required',
            'origin'                => 'nullable|string',
            'destination'           => 'nullable|string',
            'description_origin'    => 'nullable|string',
            'description_destination' => 'nullable|string',
            'product_type'          => 'nullable|string',
            'helpers'               => 'nullable|numeric',
            'schedule_order'        => 'nullable|date',
            'measured_type'         => 'required|string',
            'weight'                => 'required|numeric',
            'width'                 => 'required|numeric',
            'high'                  => 'required|numeric',
            'client_id'             => 'required|integer',
            'type_vehicle_id'       => 'required|integer',
            'distance_order'        => 'required|numeric',

        ];

        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $fecha = date("Y-m-d H:i:s");
            return response()->json([
                "code" => 1, "mensaje" => "Ha ocurrido un error",
                "fecha_hora" => $fecha, "data" => ['errors'  => $validator->errors()->all()]
            ], 200);
        }
        DB::beginTransaction();
        try {
            //$format_birthday = date('Y/m/d', strtotime($request['birthday']));

            $order = new Order($request->all());
            $order->locate_origin = json_encode($request->locate_origin);
            $order->locate_destination = json_encode($request->locate_destination);
            //$order->description_origin = $request->description_origin;
            //$order->description_destination = $request->description_destination;
            $order->status = 101;
            //$order->distance_order = $request->distance_order;
            //$order->type_vehicle_id=$request->type_vehicle_id;
            $order->save();


            /*$respuesta= DB::table('people')
        ->where('people.id', '=', $people->id)
        ->join('users', 'users.people_id', '=', 'people.id')
        ->join('drivers', 'drivers.people_id', '=', 'people.id')
        ->join('cars', 'cars.driver_id', '=', 'drivers.id')
        ->select('people.id as id', 'people.first_name as firstName','people.last_name as lastName', 'people.url_profile as urlProfile','users.email','people.gender','people.birthday',
        'people.number_account as numberAccount', 'people.telephone','people.ci', 'people.address','cars.name_car as nameCar','cars.type_car as typeCar', 'cars.mark', 'cars.color',
        'cars.model', 'cars.license_plate as licensePlate','drivers.id as driverId','drivers.url_breve as urlBrevet','people.url_ci as urlCi', 'cars.url_ruat as urlRuat','cars.url_soat as urlSoat', 'cars.capacity',
        'cars.url_image as urlCar','people.uuid_notify as uuidNotify')
        ->first();*/
            foreach ($request['images'] as $product) {

                $image = new OrderImage();
                $image->url_image = $product['url'];
                $image->order_id = $order->id;
                $image->save();
            }

            DB::commit();
            $fecha = date("Y-m-d H:i:s");
            return response()->json([
                "code" => 0, "mensaje" => "Consulta realizada correctamente",
                "fecha_hora" => $fecha, "data" => "Pedido realizado"
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
            return response()->json($e);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
            return response()->json($e);
        }
    }

    //obtener order por id de pedido
    public function showOrdersById(Request $request)
    { }

    public function guardarImagen(string $nombre, string $url_rol, string $imagen_64)
    {
        $name = $nombre;
        $url_base = $url_rol . '/';
        $url_i = null;
        if ($imagen_64 == null || $imagen_64 == "") {
            return $url_i;
        } else {
            $image_avatar_b64 = $imagen_64;
            // Obtener los datos de la imagen
            $img = $this->getB64Image($image_avatar_b64);
            // Obtener la extensión de la Imagen
            $img_extension = $this->getB64Extension($image_avatar_b64);
            // Crear un nombre aleatorio para la imagen
            $img_name = $name . time() . '.' . $img_extension;
            // Usando el Storage guardar en el disco creado anteriormente y pasandole a
            // la función "put" el nombre de la imagen y los datos de la imagen como
            // segundo parametro
            Storage::disk('public')->put($url_base . $img_name, $img, 'public');
            $url_i = Storage::url($url_base . $img_name);
            return $url_i;
        }
    }

    function getB64Image($base64_image)
    {
        // Obtener el String base-64 de los datos
        $image_service_str = substr($base64_image, strpos($base64_image, ",") + 1);
        // Decodificar ese string y devolver los datos de la imagen
        $image = base64_decode($image_service_str);
        // Retornamos el string decodificado
        return $image;
    }

    function getB64Extension($base64_image, $full = null)
    {
        // Obtener mediante una expresión regular la extensión imagen y guardarla
        // en la variable "img_extension"

        preg_match("/^data:image\/(.*);base64/i", $base64_image, $img_extension);
        // Dependiendo si se pide la extensión completa o no retornar el arreglo con
        // los datos de la extensión en la posición 0 - 1
        return ($full) ?  $img_extension[0] : $img_extension[1];
    }

    private function notify(Model $note)
    {
        $provider = Provider::findOrFail($note->provider_id);

        $notification = array(
            "title" => "Tienes un nuevo pedido",
            "body" => $note->business_name
        );
        $message = array("message" => "Message from serve", "customKey" => "customValue");
        $url = "https://fcm.googleapis.com/fcm/send ";
        $fields = array(
            "registration_ids" => [$provider->uuid_notify],
            "notification" => $notification,
            "data" => $message
        );
        $header = array(
            'Content-Type: application/json',
            'Authorization: key=AAAATyzl3yg:APA91bFGUDFy1VaVz38l-p8fjevTRnSQ8_ScQlzin46etSzkNk0i4FHzXzJD3D71n0FuD-AGiv3sy82Ev1niXUkplXX5n3jFNeZ_CbQkUdFRBAkX0E0pPh5rMS13GyrBg1_5ExiUJHLJ'
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_exec($ch);
        curl_close($ch);
    }
}
