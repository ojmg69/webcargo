<?php

namespace App\Http\Controllers;

use App\Provider;
use Illuminate\Http\Request;

class ApiClientShoppingController extends Controller
{

    public function transportationAmount(Request $request)
    {
        $amount_mts = 0.027;
        $transportation_amount = $this->getDistance($request) * $amount_mts;
        $transportation_amount = round($transportation_amount, 0);
        return response()->json(['transportation_amount'=>$transportation_amount]);
    }

    private function getDistance($request)
    {
        $longitude = $request['longitude'];
        $latitude = $request['latitude'];

        $provider = Provider::findOrFail($request['provider_id']);


        $theta = $longitude - $provider->longitude;
        $dist = sin(deg2rad($latitude)) * sin(deg2rad($provider->latitude)) +  cos(deg2rad($latitude)) * cos(deg2rad($provider->latitude)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $km = ($miles * 1.609344);
        $mt = $km * 1000;

        return $mt;
    }
}
