<?php

namespace App\Http\Controllers;

use App\Asignacion_Multa;
use App\B_Recibo_Asignacion_Multa;
use App\B_Recibo_Cobro_Adicional;
use App\B_Recibo_Contrato_Evento;
use App\B_Recibo_Deuda_Nuevo_Socio;
use App\B_Recibo_Servicio_Basico;
use App\Bitacora;
use App\Classes\BitacoraKey;
use App\Deuda_Asignacion_Cobro;
use App\Deuda_Cobro_Adicional;
use App\Deuda_Contrato_Alquiler;
use App\Deuda_Contrato_Evento;
use App\Deuda_Nuevo_Socio;
use App\Deuda_Servicio_Basico;
use App\Http\Requests\CobroRequest;
use App\Http\Requests\ReciboRequest;
use App\Recibo_Asignacion_Multa;
use App\Recibo_Cobro_Adicional;
use App\Recibo_Contrato_Evento;
use App\Recibo_Deuda_Asignacion_Cobro;
use App\Recibo_Deuda_Contrato_Alquiler;
use App\Recibo_Deuda_Nuevo_Socio;
use App\Recibo_Servicio_Basico;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class GenerarReciboCobrosController extends Controller
{

    public function deudaAfiliacionIndex()
    {
        $deudas = Deuda_Nuevo_Socio::all();
        return view('realizar_cobros.nuevo_socio.index',['deudas' => $deudas]);
    }

    public function deudaAfiliacionCobrar($id)
    {
        $deuda = Deuda_Nuevo_Socio::findOrFail($id);
        return view('realizar_cobros.nuevo_socio.cobrar', ['deuda' => $deuda]);
    }

    public function deudaAfiliacionGuardar(CobroRequest $request, $id)
    {
        $this->validate(request(), [
            'monto'=>'required|integer',
            'id_caja'=>'required',
            'fecha_recibo'=>'required|date'
        ]);
        $recibo = new Recibo_Deuda_Nuevo_Socio($request->all());
        $recibo->id_deuda_nuevo_socio = $id;
        $recibo->id_caja = $request['id_caja'];
        $recibo->id_usuario = auth()->user()->persona()->id;
        $recibo->save();
        $this->bitacora('CREADO', null, auth()->user(), $recibo, BitacoraKey::DB_RECIBO_AFILIACION);

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('realizar_cobros.nuevo_socio.reporte.recibo', ['recibo' => $recibo]);
        $pdf->setOptions(['dpi' => 120]);
        $pdf->setPaper(array(0, 0, 342.00, 396.00), 'portrait');// 98 x 110 -- 360 x 530    120 x 140 --- 453 x 530
        return $pdf->stream();
    }

    /*************************************************************************************/
    public function deudaServicioBasicoIndex()
    {
        $ids_recibos = DB::table('recibo_servicio_basico')
            ->pluck('id_deuda_servicio_basico');
        $fecha = Carbon::now('America/La_Paz')->addMonth(1);
        $deudas = DB::table('deuda_servicio_basico')
            ->whereNotIn('deuda_servicio_basico.id', $ids_recibos)
            ->whereDate('deuda_servicio_basico.fecha_deuda', '<', $fecha)
            ->join('contrato_servicio_basico', 'deuda_servicio_basico.id_contrato_servicio_basico', '=', 'contrato_servicio_basico.id')
            ->join('contrato_socio_puesto', 'contrato_servicio_basico.id_contrato_socio_puesto', '=', 'contrato_socio_puesto.id')
            ->join('servicio_basico', 'contrato_servicio_basico.id_servicio_basico', '=', 'servicio_basico.id')
            ->join('sector', 'contrato_socio_puesto.id_sector', '=', 'sector.id')
            ->join('puesto', 'contrato_socio_puesto.id_puesto', '=', 'puesto.id')
            ->join('persona', 'contrato_socio_puesto.id_socio', '=', 'persona.id')
            ->select('deuda_servicio_basico.id as id', 'puesto.numero_puesto as puesto', 'sector.nombre as sector', 'persona.nombre as socio_nombre',
                'persona.apellidos as socio_apellidos', 'contrato_socio_puesto.inquilino as inquilino',
                'servicio_basico.nombre as servicio_basico', 'deuda_servicio_basico.mes as mes', 'deuda_servicio_basico.anio as anio',
                'deuda_servicio_basico.monto')
            ->get();
        return view('realizar_cobros.servicio_basico.index', ['deudas' => $deudas]);
    }

    public function deudaServicioBasicoCobrar(ReciboRequest $request)
    {
        $deudas = Deuda_Servicio_Basico::whereIn('id', $request['id'])
            ->get();
        $total = array_sum(array_pluck($deudas, 'monto'));
        return view('realizar_cobros.servicio_basico.cobrar', ['deudas' => $deudas, 'total' => $total]);
    }

    public function deudaServicioBasicoGuardar(ReciboRequest $request)
    {
        $this->validate(request(), [
            'id_caja'=>'required',
            'fecha_recibo'=>'required|date',
            'se_recibe_de' => 'required|string'
        ]);

        $recibos = [];
        $total = 0;
        foreach ($request['id'] as $id){
            $deuda = Deuda_Servicio_Basico::findOrFail($id);
            if(!$deuda->recibo_servicio_basico){
                $recibo = new Recibo_Servicio_Basico($request->all());
                $recibo->id_deuda_servicio_basico = $id;
                $recibo->id_caja = $request['id_caja'];
                $recibo->id_usuario = auth()->user()->persona()->id;
                $recibo->monto = $deuda->monto;
                $recibo->save();
                $total = $total + $recibo->monto;
                $this->bitacora('CREADO', null, auth()->user(), $recibo, BitacoraKey::DB_RECIBO_SERVICIO_BASICO);
                array_push($recibos, $recibo);
            }
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('realizar_cobros.servicio_basico.reporte.recibo', ['recibos' => $recibos, 'total' => $total]);
        $pdf->setOptions(['dpi' => 120]);
        $pdf->setPaper(array(0, 0, 342.00, 396.00), 'portrait');// 98 x 110 -- 360 x 530    120 x 140 --- 453 x 530
        return $pdf->stream();
    }

    /*************************************************************************************/
    public function deudaEventoIndex()
    {
        $ids_recibos = DB::table('recibo_contrato_evento')
            ->pluck('id_deuda_contrato_evento');

        $deudas = Deuda_Contrato_Evento::whereNotIn('id', $ids_recibos)
            ->get();
        return view('realizar_cobros.evento.index', ['deudas' => $deudas]);
    }

    public function deudaEventoCobrar(ReciboRequest $request)
    {
        $deudas = Deuda_Contrato_Evento::whereIn('id', $request['id'])
            ->get();
        $total = array_sum(array_pluck($deudas, 'monto'));
        return view('realizar_cobros.evento.cobrar', ['deudas' => $deudas, 'total' => $total]);
    }

    public function deudaEventoGuardar(ReciboRequest $request)
    {
        $this->validate(request(), [
            'id_caja'=>'required',
            'fecha_recibo'=>'required|date',
            'se_recibe_de' => 'required|string'
        ]);
        $recibos = [];
        foreach ($request['id'] as $id){
            $deuda = Deuda_Contrato_Evento::findOrFail($id);
            if($deuda->recibo_contrato_evento == null){
                $recibo = new Recibo_Contrato_Evento($request->all());
                $recibo->id_deuda_contrato_evento = $id;
                $recibo->id_caja = $request['id_caja'];
                $recibo->id_usuario = auth()->user()->persona()->id;
                $recibo->monto = $deuda->monto;
                $recibo->save();
                $this->bitacora('CREADO', null, auth()->user(), $recibo, BitacoraKey::DB_RECIBO_EVENTO);
                array_push($recibos, $recibo);
            }
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('realizar_cobros.evento.reporte.recibo', ['recibos' => $recibos]);
        $pdf->setOptions(['dpi' => 120]);
        $pdf->setPaper(array(0, 0, 342.00, 396.00), 'portrait');// 98 x 110 -- 360 x 530    120 x 140 --- 453 x 530
        return $pdf->stream();
    }

    /*************************************************************************************/
    public function deudaCobroAdicionalIndex()
    {
        $fecha = Carbon::now('America/La_Paz')->addMonth(1);
        $ids_recibos = DB::table('recibo_cobro_adicional')
            ->pluck('id_deuda_cobro_adicional');

        $deudas = Deuda_Cobro_Adicional::whereNotIn('id', $ids_recibos)
            ->whereDate('fecha_deuda', '<', $fecha)
            ->get();
        return view('realizar_cobros.cobro_adicional.index', ['deudas' => $deudas]);
    }

    public function deudaCobroAdicionalCobrar(ReciboRequest $request)
    {
        $deudas = Deuda_Cobro_Adicional::whereIn('id', $request['id'])
            ->get();
        $total = array_sum(array_pluck($deudas, 'monto'));
        return view('realizar_cobros.cobro_adicional.cobrar', ['deudas' => $deudas, 'total' => $total]);
    }

    public function deudaCobroAdicionalGuardar(ReciboRequest $request)
    {
        $this->validate(request(), [
            'id_caja'=>'required',
            'fecha_recibo'=>'required|date',
            'se_recibe_de' => 'required|string'
        ]);
        $recibos = [];
        foreach ($request['id'] as $id){
            $deuda = Deuda_Cobro_Adicional::findOrFail($id);
            if(!$deuda->recibo_cobro_adicional){
                $recibo = new Recibo_Cobro_Adicional($request->all());
                $recibo->id_deuda_cobro_adicional = $id;
                $recibo->id_caja = $request['id_caja'];
                $recibo->id_usuario = auth()->user()->persona()->id;
                $recibo->monto = $deuda->monto;
                $recibo->save();
                $this->bitacora('CREADO', null, auth()->user(), $recibo, BitacoraKey::DB_RECIBO_ADICIONAL);
                array_push($recibos, $recibo);
            }
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('realizar_cobros.cobro_adicional.reporte.recibo', ['recibos' => $recibos]);
        $pdf->setOptions(['dpi' => 120]);
        $pdf->setPaper(array(0, 0, 342.00, 396.00), 'portrait');// 98 x 110 -- 360 x 530    120 x 140 --- 453 x 530
        return $pdf->stream();
    }

    /*************************************************************************************/
    public function deudaMultaIndex()
    {
        $ids_recibos = DB::table('recibo_asignacion_multa')
            ->pluck('id_asignacion_multa');

        $deudas = Asignacion_Multa::whereNotIn('id', $ids_recibos)
            ->get();
        return view('realizar_cobros.multa.index', ['deudas' => $deudas]);
    }

    public function deudaMultaCobrar(ReciboRequest $request)
    {
        $deudas = Asignacion_Multa::whereIn('id', $request['id'])
            ->get();
        $total = array_sum(array_pluck($deudas, 'monto'));
        return view('realizar_cobros.multa.cobrar', ['deudas' => $deudas, 'total' => $total]);
    }

    public function deudaMultaGuardar(ReciboRequest $request)
    {
        $this->validate(request(), [
            'id_caja'=>'required',
            'fecha_recibo'=>'required|date',
            'se_recibe_de' => 'required|string'
        ]);
        $recibos = [];
        foreach ($request['id'] as $id){
            $deuda = Asignacion_Multa::findOrFail($id);
            if($deuda->recibo_asignacion_multa == null){
                $recibo = new Recibo_Asignacion_Multa($request->all());
                $recibo->id_asignacion_multa = $id;
                $recibo->id_caja = $request['id_caja'];
                $recibo->id_usuario = auth()->user()->persona()->id;
                $recibo->monto = $deuda->monto;
                $recibo->save();
                $this->bitacora('CREADO', null, auth()->user(), $recibo, BitacoraKey::DB_RECIBO_MULTA);
                array_push($recibos, $recibo);
            }
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('realizar_cobros.multa.reporte.recibo', ['recibos' => $recibos]);
        $pdf->setOptions(['dpi' => 120]);
        $pdf->setPaper(array(0, 0, 342.00, 396.00), 'portrait');// 98 x 110 -- 360 x 530    120 x 140 --- 453 x 530
        return $pdf->stream();
    }
    /*************************************************************************************/
    public function deudaAlquilerIndex()
    {
        $ids = DB::table('recibo_deuda_contrato_alquiler')
            ->pluck('id_deuda_contrato_alquiler');

        $deudas = DB::table('deuda_contrato_alquiler')
            ->whereNotIn('deuda_contrato_alquiler.id', $ids)
            ->join('contrato_alquiler', 'deuda_contrato_alquiler.id_contrato_alquiler', '=', 'contrato_alquiler.id')
            ->join('persona', 'contrato_alquiler.id_inquilino', '=', 'persona.id')
            ->select('deuda_contrato_alquiler.id as id', 'persona.nombre as nombre', 'persona.apellidos as apellidos',
                'deuda_contrato_alquiler.detalle as detalle', 'deuda_contrato_alquiler.monto as monto', 'deuda_contrato_alquiler.fecha_deuda as fecha')
            ->get();
        return view('realizar_cobros.alquiler.index', ['deudas'=>$deudas]);
    }

    public function deudaAlquilerCobrar(ReciboRequest $request)
    {
        $deudas = Deuda_Contrato_Alquiler::whereIn('id', $request['id'])
            ->get();
        $total = array_sum(array_pluck($deudas, 'monto'));
        return view('realizar_cobros.alquiler.cobrar', ['deudas' => $deudas, 'total' => $total]);
    }

    public function deudaAlquilerGuardar(ReciboRequest $request)
    {
        $this->validate(request(), [
            'id_caja_cuenta'=>'required',
            'fecha_recibo'=>'required|date',
            'se_recibe_de' => 'required|string'
        ]);
        $recibos = [];
        $total = 0;
        foreach ($request['id'] as $id){
            $deuda = Deuda_Contrato_Alquiler::findOrFail($id);
            if($deuda->recibo_deuda_contrato_alquiler == null){
                $recibo = new Recibo_Deuda_Contrato_Alquiler($request->all());
                $recibo->id_deuda_contrato_alquiler = $id;
                $recibo->id_caja_cuenta = $request['id_caja_cuenta'];
                $recibo->id_usuario = auth()->user()->persona()->id;
                $recibo->monto = $deuda->monto;
                $total = $total + $deuda->monto;
                $recibo->save();
                array_push($recibos, $recibo);
            }
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('realizar_cobros.alquiler.reporte.recibo', ['recibos' => $recibos, 'total' => $total]);
        $pdf->setOptions(['dpi' => 120]);
        $pdf->setPaper(array(0, 0, 342.00, 396.00), 'portrait');// 98 x 110 -- 360 x 530    120 x 140 --- 453 x 530
        return $pdf->stream();
    }
    /****************************OTRO COBRO************************************/
    public function deudaOtroIndex()
    {
        $ids_recibos = DB::table('recibo_deuda_asignacion_cobro')
            ->pluck('id_deuda_asignacion_cobro');

        $deudas = Deuda_Asignacion_Cobro::whereNotIn('id', $ids_recibos)
            ->get();
        return view('realizar_cobros.otro.index', ['deudas' => $deudas]);
    }

    public function deudaOtroCobrar(ReciboRequest $request)
    {
        $deudas = Deuda_Asignacion_Cobro::whereIn('id', $request['id'])
            ->get();
        $total = array_sum(array_pluck($deudas, 'monto'));
        return view('realizar_cobros.otro.cobrar', ['deudas' => $deudas, 'total' => $total]);
    }

    public function deudaOtroGuardar(ReciboRequest $request)
    {
        $this->validate(request(), [
            'id_caja'=>'required',
            'fecha_recibo'=>'required|date',
            'se_recibe_de' => 'required|string'
        ]);
        $recibos = [];
        foreach ($request['id'] as $id){
            $deuda = Deuda_Asignacion_Cobro::findOrFail($id);
            if(!$deuda->recibo_deuda_asignacion_cobro){
                $recibo = new Recibo_Deuda_Asignacion_Cobro($request->all());
                $recibo->id_deuda_asignacion_cobro = $id;
                $recibo->id_caja_cuenta = $request['id_caja'];
                $recibo->id_usuario = auth()->user()->persona()->id;
                $recibo->monto = $deuda->monto;
                $recibo->save();
                array_push($recibos, $recibo);
            }
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('realizar_cobros.otro.reporte.recibo', ['recibos' => $recibos]);
        $pdf->setOptions(['dpi' => 120]);
        $pdf->setPaper(array(0, 0, 342.00, 396.00), 'portrait');// 98 x 110 -- 360 x 530    120 x 140 --- 453 x 530
        return $pdf->stream();
    }


    /************************* FUNCIONES AUXILIARES *********************/


    /************************* BITACORA *********************/

    public function bitacora($accion, $detalle, Model $user, Model $recibo, $tipo)
    {
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = $tipo;
        $bitacora->id_usuario = $user->persona()->id;
        $bitacora->save();

        if($tipo == BitacoraKey::DB_RECIBO_AFILIACION){
            $b_recibo = new B_Recibo_Deuda_Nuevo_Socio();
            $b_recibo->id_recibo_deuda_nuevo_socio = $recibo->id;
            $b_recibo->monto = $recibo->monto;
            $b_recibo->observacion = $recibo->observacion;
            $b_recibo->fecha_recibo = $recibo->fecha_recibo;
            $b_recibo->se_recibe_de = $recibo->se_recibe_de;
            $b_recibo->id_deuda_nuevo_socio = $recibo->id_deuda_nuevo_socio;
            $b_recibo->id_caja = $recibo->id_caja;
            $b_recibo->id_usuario = $user->persona()->id;
            $b_recibo->id_bitacora = $bitacora->id;
            $b_recibo->save();

        } else if($tipo == BitacoraKey::DB_RECIBO_SERVICIO_BASICO){
            $b_recibo = new B_Recibo_Servicio_Basico();
            $b_recibo->id_recibo_servicio_basico = $recibo->id;
            $b_recibo->monto = $recibo->monto;
            $b_recibo->observacion = $recibo->observacion;
            $b_recibo->fecha_recibo = $recibo->fecha_recibo;
            $b_recibo->se_recibe_de = $recibo->se_recibe_de;
            $b_recibo->id_deuda_servicio_basico = $recibo->id_deuda_servicio_basico;
            $b_recibo->id_caja = $recibo->id_caja;
            $b_recibo->id_usuario = $user->persona()->id;
            $b_recibo->id_bitacora = $bitacora->id;
            $b_recibo->save();
        } else if($tipo == BitacoraKey::DB_RECIBO_EVENTO){
            $b_recibo = new B_Recibo_Contrato_Evento();
            $b_recibo->id_recibo_contrato_evento = $recibo->id;
            $b_recibo->monto = $recibo->monto;
            $b_recibo->observacion = $recibo->observacion;
            $b_recibo->fecha_recibo = $recibo->fecha_recibo;
            $b_recibo->se_recibe_de = $recibo->se_recibe_de;
            $b_recibo->id_deuda_contrato_evento = $recibo->id_deuda_contrato_evento;
            $b_recibo->id_caja = $recibo->id_caja;
            $b_recibo->id_usuario = $user->persona()->id;
            $b_recibo->id_bitacora = $bitacora->id;
            $b_recibo->save();
        } else if($tipo == BitacoraKey::DB_RECIBO_MULTA){
            $b_recibo = new B_Recibo_Asignacion_Multa();
            $b_recibo->id_recibo_asignacion_multa = $recibo->id;
            $b_recibo->monto = $recibo->monto;
            $b_recibo->observacion = $recibo->observacion;
            $b_recibo->fecha_recibo = $recibo->fecha_recibo;
            $b_recibo->se_recibe_de = $recibo->se_recibe_de;
            $b_recibo->id_asignacion_multa = $recibo->id_asignacion_multa;
            $b_recibo->id_caja = $recibo->id_caja;
            $b_recibo->id_usuario = $user->persona()->id;
            $b_recibo->id_bitacora = $bitacora->id;
            $b_recibo->save();
        } else if($tipo == BitacoraKey::DB_RECIBO_ADICIONAL){
            $b_recibo = new B_Recibo_Cobro_Adicional();
            $b_recibo->id_recibo_cobro_adicional = $recibo->id;
            $b_recibo->monto = $recibo->monto;
            $b_recibo->observacion = $recibo->observacion;
            $b_recibo->fecha_recibo = $recibo->fecha_recibo;
            $b_recibo->se_recibe_de = $recibo->se_recibe_de;
            $b_recibo->id_deuda_cobro_adicional = $recibo->id_asignacion_multa;
            $b_recibo->id_caja = $recibo->id_caja;
            $b_recibo->id_usuario = $user->persona()->id;
            $b_recibo->id_bitacora = $bitacora->id;
            $b_recibo->save();
        }
    }
}


