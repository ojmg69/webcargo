<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiClientProviderController extends Controller
{

    public function all()
    {
        $providers = DB::table('providers')
            ->select('providers.id', 'providers.name', 'providers.business_name', 'providers.nit',
                'providers.url_image', 'providers.address', 'providers.telephone', 'providers.longitude',
                'providers.latitude')
            ->get();
        return response()->json($providers);
    }

    public function indexManufacturer()
    {
        $distributors = DB::table('providers')
            ->where('type', '=', 'Fabricante')
            ->select('providers.id', 'providers.name', 'providers.business_name', 'providers.nit',
                'providers.url_image', 'providers.address', 'providers.telephone', 'providers.longitude',
                'providers.latitude')
            ->get();

        return response()->json($distributors);
    }

    public function indexDistributor()
    {
        $distributors = DB::table('providers')
            ->where('type', '=', 'Distribuidor')
            ->select('providers.id', 'providers.name', 'providers.business_name', 'providers.nit',
                'providers.url_image', 'providers.address', 'providers.telephone', 'providers.longitude',
                'providers.latitude')
            ->get();
        return response()->json($distributors);
    }

    public function indexCategories()
    {
        $categories = DB::table('categories')
            ->select('categories.id', 'categories.name')
            ->get();

        foreach ($categories as $category)
        {
            $providers = DB::table('providers')
                ->join('products', 'providers.id', '=', 'products.provider_id')
                ->where('products.category_id', '=', $category->id)
                ->select('providers.id', 'providers.name', 'providers.business_name', 'providers.url_image', 'providers.address',
                    'providers.telephone', 'providers.type', 'providers.longitude', 'providers.latitude')
                ->groupBy('providers.id')
                ->get();
            $category->providers = $providers;
        }
        return response()->json($categories);
    }

    public function products($id_provider)
    {
        $categories = DB::table('categories')
            ->join('products', 'categories.id', '=', 'products.category_id')
            ->where('products.provider_id', '=', $id_provider)
            ->select('categories.id', 'categories.name')
            ->groupBy('categories.id', 'categories.name')
            ->get();

        foreach ($categories as $category)
        {
            $products = DB::table('products')
                ->join('stocks', 'products.id', '=', 'stocks.product_id')
                ->where('products.provider_id', '=', $id_provider)
                ->where('products.category_id', '=', $category->id)
                ->where('status', '=', '200')
                ->select('products.id', 'products.code', 'products.mark', 'products.name', 'products.title',
                    'products.description', 'products.url_image', 'stocks.count', 'stocks.sale_amount')
                ->get();
            $category->products = $products;
        }

        return response()->json($categories);
    }
}
