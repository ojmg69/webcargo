<?php

namespace App\Http\Controllers;

use App\B_Caja_Cuenta;
use App\B_Cobro_Adicional;
use App\B_Cobro_Nuevo_Socio;
use App\B_Evento;
use App\B_Inquilino;
use App\B_Multa;
use App\B_Puesto;
use App\B_Sector;
use App\B_Servicio_Basico;
use App\B_Socio;
use App\Bitacora;
use App\Caja_Cuenta;
use App\Cobro_Adicional;
use App\Cobro_Nuevo_Socio;
use App\Evento;
use App\Inquilino;
use App\Multa;
use App\Puesto;
use App\Sector;
use App\Servicio_Basico;
use App\Socio;
use Illuminate\Support\Facades\Auth;

class BitacoraController extends Controller
{

    const DB_SECTOR = 0;
    const DB_PUESTO = 1;
    const DB_SERVICIO_BASICO = 2;
    const DB_COBRO_ADICIONAL = 3;
    const DB_COBRO_NUEVO_SOCIO = 4;
    const DB_SOCIO = 5;
    const DB_EVENTO = 6;
    const DB_MULTA = 7;
    const DB_CAJA = 8;

    public function __construct()
    {
    }

    public function guardarSector($accion, $detalle, $id_publicacion, Sector $sector){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_SECTOR;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;

        if($bitacora->save()){
            $b_sector = new B_Sector();
            $b_sector->id_sector = $sector->id;
            $b_sector->nombre = $sector->nombre;
            $b_sector->descripcion = $sector->descripcion;
            $b_sector->id_usuario = $sector->id_usuario;
            $b_sector->id_bitacora = $bitacora->id;

            if($b_sector->save()){
                return true;
            } else {
                return false;
                //error al guardar
            }

        } else {
            return false;
            // error de guardar bitacora
        }
    }

    public function guardarPuesto($accion, $detalle, $id_publicacion, Puesto $puesto){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_PUESTO;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;

        if($bitacora->save()){
            $b_puesto = new B_Puesto();
            $b_puesto->id_puesto = $puesto->id;
            $b_puesto->numero_puesto = $puesto->numero_puesto;
            $b_puesto->descripcion = $puesto->descripcion;
            $b_puesto->pasillo = $puesto->pasillo;
            $b_puesto->id_usuario = $puesto->id_usuario;
            $b_puesto->id_bitacora = $bitacora->id;

            if($b_puesto->save()){
                return true;
            } else {
                return false;
                //error al guardar
            }

        } else {
            return false;
            // error de guardar bitacora
        }
    }

    public function guardarServicioBasico($accion, $detalle, $id_publicacion, Servicio_Basico $servicio_basico){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_SERVICIO_BASICO;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;

        if($bitacora->save()){
            $b_servicio_basico = new B_Servicio_Basico();
            $b_servicio_basico->id_servicio_basico = $servicio_basico->id;
            $b_servicio_basico->nombre = $servicio_basico->nombre;
            $b_servicio_basico->descripcion = $servicio_basico->descripcion;
            $b_servicio_basico->id_usuario = $servicio_basico->id_usuario;
            $b_servicio_basico->id_bitacora = $bitacora->id;

            if($b_servicio_basico->save()){
                return true;
            } else {
                return false;
                //error al guardar
            }

        } else {
            return false;
            // error de guardar bitacora
        }
    }
    public function guardarCobroAdicional($accion, $detalle, $id_publicacion, Cobro_Adicional $cobro_adicional){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_COBRO_ADICIONAL;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;

        if($bitacora->save()){
            $b_cobro_adicional = new B_Cobro_Adicional();
            $b_cobro_adicional->id_cobro_adicional = $cobro_adicional->id;
            $b_cobro_adicional->nombre = $cobro_adicional->nombre;
            $b_cobro_adicional->monto = $cobro_adicional->monto;
            $b_cobro_adicional->descripcion = $cobro_adicional->descripcion;
            $b_cobro_adicional->id_publicacion = $cobro_adicional->id_publicacion;
            $b_cobro_adicional->id_usuario = $cobro_adicional->id_usuario;
            $b_cobro_adicional->id_bitacora = $bitacora->id;

            if($b_cobro_adicional->save()){
                return true;
            } else {
                return false;
                //error al guardar
            }

        } else {
            return false;
            // error de guardar bitacora
        }
    }
    public function guardarCobroNuevoSocio($accion, $detalle, $id_publicacion, Cobro_Nuevo_Socio $cobro_nuevo_socio){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_COBRO_NUEVO_SOCIO;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;

        if($bitacora->save()){
            $b_cobro_nuevo_socio = new B_Cobro_Nuevo_Socio();
            $b_cobro_nuevo_socio->id_cobro_nuevo_socio = $cobro_nuevo_socio->id;
            $b_cobro_nuevo_socio->nombre = $cobro_nuevo_socio->nombre;
            $b_cobro_nuevo_socio->monto = $cobro_nuevo_socio->monto;
            $b_cobro_nuevo_socio->descripcion = $cobro_nuevo_socio->descripcion;
            $b_cobro_nuevo_socio->estado = $cobro_nuevo_socio->estado;
            $b_cobro_nuevo_socio->id_publicacion = $cobro_nuevo_socio->id_publicacion;
            $b_cobro_nuevo_socio->id_usuario = $cobro_nuevo_socio->id_usuario;
            $b_cobro_nuevo_socio->id_bitacora = $bitacora->id;

            if($b_cobro_nuevo_socio->save()){
                return true;
            } else {
                return false;
                //error al guardar
            }

        } else {
            return false;
            // error de guardar bitacora
        }
    }
    public function guardarSocio($accion, $detalle, $id_publicacion, Socio $socio){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_SOCIO;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;

        if($bitacora->save()){
            $b_socio = new B_Socio();
            $b_socio->id_socio = $socio->id;

            if($b_socio->save()){
                return true;
            } else {
                return false;
            }
        } else{
            return false;
        }
    }
    public function guardarInquilino($accion, $detalle, $id_publicacion, Inquilino $inquilino){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_SOCIO;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;

        if($bitacora->save()){
            $b_inquilino = new B_Inquilino();
            $b_inquilino->id_inquilino = $inquilino->id;

            if($b_inquilino->save()){
                return true;
            } else {
                return false;
            }
        } else{
            return false;
        }
    }

    public function guardarEvento($accion, $detalle, $id_publicacion, Evento $evento){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_EVENTO;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;
        if($bitacora->save()){
            $b_evento = new B_Evento();
            $b_evento->id_evento = $evento->id;
            $b_evento->nombre = $evento->nombre;
            $b_evento->descripcion = $evento->descripcion;
            $b_evento->fecha = $evento->fecha;
            $b_evento->fecha_inicio = $evento->fecha_inicio;
            $b_evento->fecha_finalizacion = $evento->fecha_finalizacion;
            $b_evento->cantidad_puesto = $evento->cantidad_puesto;
            $b_evento->costo_puesto = $evento->costo_puesto;
            $b_evento->id_usuario = $evento->id_usuario;

            $b_evento->id_bitacora = $bitacora->id;
            if($b_evento->save()){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function guardarMulta($accion, $detalle, $id_publicacion, Multa $multa){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_MULTA;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;
        if($bitacora->save()){
            $b_multa = new B_Multa();
            $b_multa->id_multa = $multa->id;
            $b_multa->motivo = $multa->motivo;
            $b_multa->descripcion = $multa->descripcion;
            $b_multa->monto = $multa->monto;
            $b_multa->fecha = $multa->fecha;
            $b_multa->id_publicacion = $multa->id_publicacion;

            $b_multa->id_usuario = $multa->id_usuario;
            $b_multa->id_bitacora = $bitacora->id;
            if($b_multa->save()){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function guardarCaja($accion, $detalle, $id_publicacion, Caja_Cuenta $caja){
        $bitacora = new Bitacora();
        $bitacora->accion = $accion;
        $bitacora->detalle = $detalle;
        $bitacora->tipo = BitacoraController::DB_CAJA;
        $bitacora->id_publicacion = $id_publicacion;
        $bitacora->id_usuario = Auth::user()->persona()->id;
        if($bitacora->save()){
            $b_caja = new B_Caja_Cuenta();
            $b_caja->id_caja_cuenta = $caja->id;
            $b_caja->codigo_caja = $caja->codigo_caja;
            $b_caja->nombre = $caja->nombre;
            $b_caja->descripcion = $caja->descripcion;
            $b_caja->fecha = $caja->fecha;
            $b_caja->encargado = $caja->encargado;

            $b_caja->id_usuario = $caja->id_usuario;
            $b_caja->id_bitacora = $bitacora->id;
            if($b_caja->save()){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
