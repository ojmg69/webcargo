<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Auth;

class LoginController extends Controller
{

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {

        if(Auth::attempt($this->credenciales($request)))
        {
            return redirect()->route('admin.home');
        }
        return back()->withErrors(['email' => trans('auth.failed')])->withInput(request(['email']));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }


    private function credenciales($request)
    {
        $credencial = [];
        $credencial['email'] = $request['email'];
        $credencial['password'] = $request['password'];
        return $credencial;
    }
}
