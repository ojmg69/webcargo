<?php
/**
 * Created by PhpStorm.
 * User: Ronaldo Rivero
 * Date: 11/02/2019
 * Time: 7:16
 */
namespace App\Classes;

class BitacoraKey {

    const DB_SECTOR = 0;
    const DB_PUESTO = 1;
    const DB_SERVICIO_BASICO = 2;
    const DB_COBRO_ADICIONAL = 3;
    const DB_COBRO_NUEVO_SOCIO = 4;
    const DB_SOCIO = 5;
    const DB_EVENTO = 6;
    const DB_MULTA = 7;
    const DB_CAJA = 8;
    const DB_CAJA_ASIGNACION = 9;
    const DB_CAJA_TRANSFERENCIA = 10;
    const DB_USUARIO = 11;
    const DB_INQUILINO = 12;
    const DB_CONTRATO_SOCIO = 13;
    const DB_CONTRATO_SERVICIO = 14;
    const DB_GESTION_COBRO_ADICIONAL = 15;
    const DB_CONTRATO_EVENTO = 16;
    const DB_TIPO_INGRESO = 17;
    const DB_TIPO_EGRESO = 18;
    const DB_TIPO_ACTIVO = 19;
    const DB_ACTIVO = 20;
    const DB_ASIGNACION_ACTIVO = 21;
    const DB_ESTADO_ACTIVO = 22;
    const DB_CARGO = 23;
    const DB_CONTRATO = 24;
    const DB_PLANILLA = 25;
    const DB_EXTRA = 26;


    const DB_DEUDA_AFILIACION = 150;
    const DB_DEUDA_SERVICIO_BASICO = 151;
    const DB_DEUDA_EVENTO = 152;
    const DB_DEUDA_MULTA = 153;
    const DB_DEUDA_OTRO = 154;
    const DB_DEUDA_CONTRATO_EVENTO = 155;
    const DB_ASIGNACION_MULTA = 156;


    const DB_RECIBO_TRANSFERENCIA = 200;
    const DB_RECIBO_AFILIACION = 201;
    const DB_RECIBO_SERVICIO_BASICO = 202;
    const DB_RECIBO_EVENTO = 203;
    const DB_RECIBO_ADICIONAL = 204;
    const DB_RECIBO_MULTA = 205;
    const DB_RECIBO_INGRESO = 206;
    const DB_RECIBO_CERTIFICADO = 206;

    const DB_NOTA_TRANSFERENCIA = 300;
    const DB_NOTA_EGRESO = 301;
    const DB_NOTA_EGRESO_SUELDO = 302;

    const DB_DEUDA_NUEVO_SOCIO = 400;
}

?>
