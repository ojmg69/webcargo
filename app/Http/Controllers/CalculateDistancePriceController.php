<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TypeVehicle;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
class CalculateDistancePriceController extends Controller
{
    public function  calculatePrice(Request $request)
    {
        $rules = [
            'vehicle_id'            =>'required|integer',
            'distance_order'        =>'required|numeric',
            ];


        $fecha=date("Y-m-d H:i:s");
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            return response()->json(['code'=>'1','mensaje'=>$rules], 500);
        }

        $vehicle = TypeVehicle::findOrFail($request->vehicle_id);

        if($vehicle!=""){
            $distance_km=round($request->distance_order/1000,1);
            if ($distance_km <= $vehicle->min_distance || $distance_km <= $vehicle->max_distance ){
               $total= $vehicle->base_price +($vehicle->min_cost_km * $distance_km);
               return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
               "fecha_hora"=>$fecha,"data"=>["total_price"=>$total]],200);
            }else{
                $total= $vehicle->base_price+($vehicle->max_cost_km*$distance_km);
                return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
                "fecha_hora"=>$fecha,"data"=>["total_price"=>$total]],200);
            }
             }else{
                return response()->json(["code"=>1, "mensaje"=>"Consulta realizada correctamente",
                "fecha_hora"=>$fecha,"data"=>'Registro no encontrado'],400);
        }
    }
}
