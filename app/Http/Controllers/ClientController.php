<?php

namespace App\Http\Controllers;
use App\Client;
use App\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;



class ClientController extends Controller
{

    public function login(Request $request)
    {
        $fecha=date("Y-m-d H:i:s");

        $client = $this->getClientUuid($request);
        if($client == null)
        {
            $client="usuario no registrado";
            return response()->json(["code"=>1, "mensaje"=>"Consulta realizada con errores",
        "fecha_hora"=>$fecha,"data"=>$client],200);
        } else {
            $client=$this->getClientUuid($request);
            //$client = $this->updateClient($request, $client);
            return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
            "fecha_hora"=>$fecha,"data"=>$client],200);
        }

    }

    public function register(Request $request)
    {
        $fecha=date("Y-m-d H:i:s");
        $validator = Validator::make($request->all(), $this->loginRules());
        if($validator->fails())
        {

            return response()->json(["code"=>1, "mensaje"=>"Consulta realizada con errores",
            "fecha_hora"=>$fecha,"data"=>['errors'=>$validator->messages()]],500);
        }

        DB::beginTransaction();
        try{
        $format_birthday = date('Y/m/d', strtotime($request['birthday']));

        $pe = new People($request->all());
        $pe->people_type = 2;
        $pe->birthday = $format_birthday;
        $pe->save();


        $cl = new Client();
        $cl->occupation_id= $request['occupation_id'];
        $cl->email = strtolower($request->email);
        $cl->people_id = $pe->id;
        if($request['type_login'] == 'facebook')
        {
            $cl->uuid_facebook = $request['uuid_facebook'];
        } else {
            $cl->uuid_gmail = $request['uuid_gmail'];
        }
        $cl->save();
        $respuesta= $this->getClientUuid($request);
        DB::commit();
        //return response()->json($respuesta,200);
        return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
            "fecha_hora"=>$fecha,"data"=>$respuesta],200);

    } catch (\Exception $e) {
        DB::rollback();

        throw $e;
        return response()->json($e);
    } catch (\Throwable $e) {
        DB::rollback();
        throw $e;
        return response()->json($e);
    }
}

public function update_occupation(Request $request){
    $fecha=date("Y-m-d H:i:s");
    $client = Client::find($request->client_id);
    if ($client !== null) {
        $client->occupation_id=$request->occupation_id;
        $client->update();
        return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
        "fecha_hora"=>$fecha,"data"=>'Ocupación actualizado correctamente'],200);
    } else {
        return response()->json(["code"=>1, "mensaje"=>"Consulta realizada correctamente",
        "fecha_hora"=>$fecha,"data"=>'Usuario no encontrado '],200);
    }

    $client->occupation_id=$request->occupation_id;
    $client->update();
    return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
    "fecha_hora"=>$fecha,"data"=>'Ocupción actaulizado correctamente'],200);
}



    public function login2(Request $request)
    {
        $validator = Validator::make($request->all(), $this->loginRules());
        if($validator->fails())
        {
            return response()->json(['code'=>'1','mensaje'=>$validator->messages()], 500);
        }

        $client = $this->getClientUuid($request);
        if($client == null)
        {
            $client = $this->saveClient($request);
        } else {
            $client = $this->updateClient($request, $client);
        }
        return response()->json($client);
    }

    public function updateNotify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required|integer',
            'uuid_notify'   => 'required|max:1000'
        ]);
        if($validator->fails())
        {
            return response()->json([], 500, ['code'=>'1000', 'message'=>$validator->messages()]);
        }
        $client = Client::findOrFail($request['id']);
        $people = People::findOrFail($client->people_id);
        $people->uuid_notify = $request['uuid_notify'];
        $people->save();
        return response()->json([], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required|integer'
        ]);
        if($validator->fails())
        {
            return response()->json([], 500, ['code'=>'1000', 'message'=>$validator->messages()]);
        }

        $client = Client::findOrFail($request['id']);
        if($request['occupation_id'] != null)
        {
            $client->occupation_id = $request['occupation_id'];
            $client->save();
        }

        $people = People::findOrFail($client->people_id);

        if($request['full_name']!=null)
        {
            $people->full_name = $request['full_name'];
        }

        if($request['ci']!=null)
        {
            $people->ci = $request['ci'];
        }
        if($request['gender']!=null)
        {
            $people->gender = $request['gender'];
        }
        if($request['birthday']!=null)
        {
            $people->birthday = $request['birthday'];
        }
        if($request['telephone']!=null)
        {
            $people->telephone = $request['telephone'];
        }
        $people->save();
        $client = $this->getClientId($client->id);
        return response()->json($client);
    }

    private function saveClient($request)
    {
        $pe = new People($request->all());
        $pe->type = 2;
        $pe->status = 200;
        $pe->save();

        $login = new User();
        $login->email = strtolower($request->email);
        $login->people_id = $pe->id;
        $login->save();

        $cl = new Client();
        $cl->occupation_id = $request['occupation_id'];
        $cl->people_id = $pe->id;
        if($request['type_login'] == 'facebook')
        {
            $cl->uuid_facebook = $request['uuid_facebook'];
        } else {
            $cl->uuid_gmail = $request['uuid_gmail'];
        }
        $cl->save();
        return $this->getClientUuid($request);
    }



    private function updateClient($request, $client)
    {
        $cl = Client::findOrFail($client->id);
        $pe = People::findOrFail($cl->people_id);
        $pe->full_name = $request['full_name'];
        $pe->url_profile = $request['url_profile'];
        $pe->uuid_notify = $request['uuid_notify'];
        $pe->save();
        return $this->getClientUuid($request);
    }

    private function loginRules()
    {
        $rules = [
            'type_login'    => 'required|max:8',
            'uuid_facebook' => 'nullable|max:255',
            'email'             =>'required|string|max:255|email|unique:clients',
            'ci'             =>'required|numeric|unique:peoples',
            'uuid_gmail'    => 'nullable|max:255',
            'occupation_id'    => 'required|max:255',
            'full_name'    => 'required|max:255',
            'telephone'     => 'nullable|max:15',
            'url_profile'   => 'nullable|max:1000',
            'uuid_notify'   => 'required|max:255',
        ];
        return $rules;
    }

    private function getClientUuid($request)
    {
        $client = null;
        if($request['type_login'] == 'facebook')
        {
            $client = DB::table('clients')
                ->where('uuid_facebook', '=', $request['uuid_facebook'])
                ->join('peoples', 'clients.people_id', '=', 'peoples.id')
                ->join('occupations', 'clients.occupation_id', '=', 'occupations.id')
                ->select('clients.id', 'occupations.name as occupation', 'peoples.full_name', 'clients.email', 'peoples.ci',
                    'peoples.birthday', 'peoples.telephone', 'peoples.url_profile')
                ->first();
        } else {
            $client = DB::table('clients')
                ->where('uuid_gmail', '=', $request['uuid_gmail'])
                ->join('peoples', 'clients.people_id', '=', 'peoples.id')
                ->join('occupations', 'clients.occupation_id', '=', 'occupations.id')
                ->select('clients.id', 'occupations.name as occupation', 'peoples.full_name', 'clients.email', 'peoples.ci',
                    'peoples.birthday', 'peoples.telephone', 'peoples.url_profile')
                ->first();
        }
        return $client;
    }

    private function getClientId($id)
    {
        $client = DB::table('clients')
            ->where('clients.id', '=', $id)
            ->join('peoples', 'clients.people_id', '=', 'peoples.id')
            ->join('occupations', 'clients.occupation_id', '=', 'occupations.id')
            ->select('clients.id', 'occupations.name as occupation', 'peoples.full_name', 'clients.email', 'peoples.ci',
                'peoples.birthday', 'peoples.telephone', 'peoples.url_profile')
            ->first();
        return $client;
    }
}
