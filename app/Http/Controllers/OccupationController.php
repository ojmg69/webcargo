<?php

namespace App\Http\Controllers;




use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class OccupationController extends Controller
{
    public function index()
    {
        $occupation = DB::table('occupations')->select('id','name')->orderBy('id')->get();
        $fecha=date("Y-m-d H:i:s");
        return response()->json(["code"=>0, "mensaje"=>"Consulta realizada correctamente",
        "fecha_hora"=>$fecha,"data"=>$occupation],200);
    }

    public function store($request)
    {
        $occupation = new Occupation($request->all());
        $occupation->administrator_id = auth()->user()->people->administrator->id;
        $occupation->save();
        return $occupation;
    }

    public function find($id)
    {
        $occupation = Occupation::findOrFail($id);
        return $occupation;
    }

    public function update($request, $id)
    {
        $occupation = Occupation::findOrFail($id);
        $occupation->name = $request['name'];
        $occupation->administrator_id = auth()->user()->people->administrator->id;
        $occupation->save();
        return $occupation;
    }

    public function destroy($id)
    {
        $occupation = Occupation::findOrFail($id);
        return $occupation->delete();
    }
}
