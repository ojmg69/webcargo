<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeVehicle extends Model
{
    protected $table = 'type_vehicles';
    protected $fillable = ['name', 'description', 'url_image', 'category_id', 'base_price',
    'min_distance', 'max_distance','min_cost_km', 'max_cost_km', 'commission_percentage'];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    public function drivers()
    {
        return $this->hasMany('App\Driver', 'type_vehicle_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Order', 'type_vehicle_id');
    }
}
