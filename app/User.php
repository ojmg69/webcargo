<?php

namespace App;

use App\Terms\UserTerm;
use App\Presenters\UsuarioPresenter;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;

class User extends Authenticatable
{
    use Notifiable, HasRolesAndPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'email', 'password','people_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function people()
    {
        return $this->belongsTo('App\People', 'people_id');
    }
    public function occupations(){
        return $this->hasMany('App\Occupation', 'user_id');
    }
    public function marks(){
        return $this->hasMany('App\Mark', 'user_id');
    }

    public function colors(){
        return $this->hasMany('App\Color', 'user_id');
    }

    public function categories(){
        return $this->hasMany('App\Category', 'user_id');
    }

    public function term()
    {
        return new UserTerm();
    }
    public function present(){
        return new UsuarioPresenter($this);
    }
}
