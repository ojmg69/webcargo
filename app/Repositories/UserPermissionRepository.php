<?php


namespace App\Repositories;


use Illuminate\Support\Facades\DB;

class UserPermissionRepository
{

    public function store($user_id, $permission_id)
    {
        return DB::table('permission_user')->insert(
            ['permission_id' => $permission_id, 'user_id' => $user_id]
        );
    }

    public function destroy($user_id, $permission_id)
    {
        return DB::table('permission_user')
            ->where('permission_id', '=', $permission_id)
            ->where('user_id', '=', $user_id)
            ->delete();
    }

    public function destroyAllPermission($user_id)
    {
        return DB::table('permission_user')
            ->where('user_id', '=', $user_id)
            ->delete();
    }

    public function getPermissionId($user_id)
    {
        return DB::table('permission_user')
            ->where('user_id', '=', $user_id)
            ->select('permission_id')
            ->get();
    }
}
