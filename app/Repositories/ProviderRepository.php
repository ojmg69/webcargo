<?php
/**
 * Created by PhpStorm.
 * User: Ronaldo Rivero
 * Date: 08/06/2019
 * Time: 16:22
 */

namespace App\Repositories;


use App\Provider;


class ProviderRepository implements ResourcesInterface
{

    public function index()
    {
        $providers = Provider::all();
        return $providers;
    }

    public function store($request)
    {
        $provider = new Provider($request->all());
        $provider->username = strtolower($request['username']);

        $provider->url_image = $this->saveImage($request);
        $provider->password = bcrypt($request['password']);
        $provider->status = 200;
        $provider->administrator_id = auth()->user()->people->administrator->id;
        $provider->save();
        return $provider;
    }

    public function find($id)
    {
        $provider = Provider::findOrFail($id);
        return $provider;
    }

    public function update($request, $id)
    {
        $provider = Provider::findOrFail($id);
        $provider->name = $request['name'];
        $provider->business_name = $request['business_name'];
        $provider->nit = $request['nit'];
        $provider->address = $request['address'];
        $provider->telephone = $request['telephone'];
        $provider->type = $request['type'];
        $provider->longitude = $request['longitude'];
        $provider->latitude = $request['latitude'];

        $provider->username = strtolower($request['username']);

        if($request->hasFile('image'))
        {
            $provider->url_image = $this->saveImage($request);
        }
        $provider->password = bcrypt($request['password']);
        $provider->status = 200;
        $provider->administrator_id = auth()->user()->people->administrator->id;
        $provider->save();
        return $provider;
    }

    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }

    private function saveImage($request)
    {
        $public = 'public/';
        $url_base = 'providers/' . $request['name'];
        $url_image = null;
        if($request->hasFile('image'))
        {
            $name_image = $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs($public . $url_base, $name_image);
            $url_image = $url_base . '/' . $name_image;
        }
        return $url_image;
    }
}
