<?php
/**
 * Created by PhpStorm.
 * User: Ronaldo Rivero
 * Date: 27/04/2019
 * Time: 17:40
 */

namespace App\Repositories;


interface ResourcesInterface
{

    public function index();

    public function store($request);

    public function find($id);

    public function update($request, $id);

    public function destroy($id);
}
