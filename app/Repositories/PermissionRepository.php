<?php


namespace App\Repositories;


use Illuminate\Support\Facades\DB;

class PermissionRepository
{

    public function index()
    {
        $permissions = DB::table('permissions')
            ->select('permissions.id', 'permissions.name', 'permissions.slug')
            ->orderBy('id', 'asc')
            ->get();
        return $permissions;
    }


}
