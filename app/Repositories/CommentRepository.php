<?php


namespace App\Repositories;


use App\Comment;
use Illuminate\Support\Facades\DB;

class CommentRepository
{

    public function index($service_id)
    {
        $comments = DB::table('comments')
            ->where('comments.service_id', '=', $service_id)
            ->join('clients', 'comments.client_id', '=', 'clients.id')
            ->join('people', 'clients.people_id', '=', 'people.id')
            ->select('comments.id', 'comments.title', 'comments.description', 'comments.rate', 'people.first_name', 'people.last_name')
            ->get();

        return $comments;
    }

    public function store($request)
    {
        $comment = new Comment($request->all());
        $comment->save();
        return $comment;
    }

    public function find($id)
    {
        $comment = Comment::findOrFail($id);
        return $comment;
    }

    public function update($request, $id)
    {
        $comment = Comment::findOrFail($id);
        $comment->title = $request['title'];
        $comment->description = $request['description'];
        $comment->rate = $request['rate'];
        $comment->save();
        return $comment;
    }

    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        return $comment->delete();
    }

}
