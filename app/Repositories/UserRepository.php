<?php
/**
 * Created by PhpStorm.
 * User: Ronaldo Rivero
 * Date: 29/05/2019
 * Time: 20:13
 */

namespace App\Repositories;


use App\Administrator;
use Illuminate\Support\Facades\DB;

class UserRepository implements ResourcesInterface
{

    protected $people;

    public function __construct()
    {
        $this->people = new PeopleRepository();
    }

    public function index()
    {
        $users = DB::table('people')
            ->join('administrators', 'people.id', '=', 'administrators.people_id')
            ->where('people.status', '=', 200)
            ->select('people.id', 'people.first_name', 'people.last_name', 'people.ci', 'people.birthday', 'people.telephone',
                'administrators.id as administrator_id')
            ->orderBy('first_name', 'asc')
            ->get();
        return $users;
    }

    public function store($request)
    {
        $people = $this->people->store($request);

        $administrator = new Administrator();
        $administrator->people_id = $people->id;
        $administrator->save();

        return $administrator;
    }

    public function find($id)
    {
        $administrator = Administrator::findOrFail($id);
        return $administrator;
    }

    public function update($request, $id)
    {
        $administrator = Administrator::findOrFail($id);
        $this->people->update($request, $administrator->people_id);
        return $administrator->save();
    }

    public function destroy($id)
    {
        $administrator = Administrator::findOrFail($id);
        $people = $administrator->people;
        $people->status = 0;
        $login = $people->user;
        $login->status = 0;
        return $people->save() && $login->save();
    }
}
