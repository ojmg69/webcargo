<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallets';
    protected $fillable = ['gain','commision', 'balance', 'driver_id'];

    public function driver()
    {
        return $this->belongsTo('App\Driver', 'driver_id');
    }
}
