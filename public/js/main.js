"use strict";

var map = L.map('leaflet-map').setView([0, 0], 16);
var tileURL = 'ttps://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var tileURL2 = 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png';
var tile = L.tileLayer(tileURL2); // Socket Io
//"API REST corriendo en http://localhost:".concat(_config["default"].port)

var socket = io.connect("https://servicio-demo.herokuapp.com/", {
  //const socket = io.connect("http://localhost:".concat(_config["default"].port), {
  transports: ["websocket"]
}); // Marker

/*const marker = L.marker([50.5, 30.5]); // kiev, ukraine
marker.bindPopup('Hello There!');
map.addLayer(marker);*/
// Geolocation

map.locate({
  enableHighAccuracy: true
});
map.on('locationfound', function (e) {
  var coords = [e.latlng.lat, e.latlng.lng];
  var newMarker = L.marker(coords);
  newMarker.bindPopup('Usted Esta Aquí');
  map.addLayer(newMarker);
  socket.emit('userCoordinates', e.latlng);
  map.panTo(new L.LatLng(coords[0], coords[1]), 6);
});
var myIcon = L.icon({
  iconUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRo3M0pulX5ijeWLagtgozCdxFpvGUnWBJnUXmiJSqw9t1I5IxU&s',
  iconSize: [38, 42],
  iconAnchor: [22, 94],
  popupAnchor: [-3, -76],
  // shadowUrl: 'my-icon-shadow.png',
  shadowSize: [68, 95],
  shadowAnchor: [22, 94]
});
var markers = [];
socket.on("vehicles_update", function (carros) {
  carros.map(function (vehicle) {
    var contenido = "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Fuso_Canter_3C13%2C_8th_Generation.jpg/220px-Fuso_Canter_3C13%2C_8th_Generation.jpg' alt='logo' width='150px'/><br/>" + "<b>Nombre: </b>" + vehicle.first_name + ' ' + vehicle.last_name + "<br/>" + "<b>Placa:: </b>" + vehicle.license_plate + "<br/>" + "<b>Tipo:: </b>" + vehicle.mark + "<br/>" + "<b>Estado: </b>Disponible<br/>" + "<b>Deuda: </b>0<br/>"; // specify popup options 
    if (!markers[vehicle.first_name]) {
      var customOptions = {
        'maxWidth': '350',
        'className': 'custom'
      };
      markers[vehicle.first_name] = L.marker([vehicle.latitud, vehicle.longitud], {
        icon: myIcon
      }).bindPopup(contenido).addTo(map);
    } else {
      markers[vehicle.first_name].setLatLng([vehicle.latitud, vehicle.longitud]).setPopupContent(contenido).update();
    }
  });
}); // socket new User connected

socket.on('newUserCoordinates', function (coords) {
  console.log(coords);
  var userIcon = L.icon({
    iconUrl: '/img/icon2.png',
    iconSize: [38, 42]
  });
  var newUserMarker = L.marker([coords.lat, coords.lng], {
    icon: userIcon
  });
  newUserMarker.bindPopup('New User!');
  map.addLayer(newUserMarker);
});
map.addLayer(tile);