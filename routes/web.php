<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('welcome')->middleware('guest');
Route::get('/test', function () {
  return view('welcome');
});
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('payment/{id}', 'PaymentLiveesController@payment');
Route::get('confirmed', 'PaymentLiveesController@confirmed');


Route::middleware(['auth'])->group(function() {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('home', 'HomeController@inicio')->name('admin.home');







    });
});



