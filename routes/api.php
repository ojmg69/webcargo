<?php

use Illuminate\Http\Request;
use App\Http\Controllers;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//rutas user admin
Route::group(['prefix'=>'admin'], function (){
    Route::group(['prefix'=>'usuario'], function (){
        Route::get('mostrarTodo', 'UserController@index');
        Route::get('mostrar/{id}', 'UserController@show');
        Route::get('registrar', 'UserController@create');
        Route::post('guardar', 'UserController@store');
        Route::get('editar/{id}', 'UserController@edit');
        Route::post('actualizar/{id}', 'UserController@update');
        Route::get('eliminar/{id}', 'UserController@delete');
    });
 });
//Rutas clientes
Route::group(['prefix'=>'client'], function (){
    Route::post('login', 'ClientController@login');
    Route::post('register', 'ClientController@register');
    Route::post('update_notify', 'ClientController@updateNotify');
    Route::post('update', 'ClientController@update');
    Route::post('update_occupation', 'ClientController@update_occupation');
    Route::post('calculate_price', 'CalculateDistancePriceController@calculatePrice');

    //orden de pedido cliente
    Route::group(['prefix'=>'order_client'], function (){
        Route::post('order', 'ClientOrderController@store');
        Route::get('orders', 'ClientOrderController@index');
    });

    Route::group(['prefix'=>'delivery'], function (){
        Route::post('index', 'ApiClientDeliveryController@index');
        Route::post('pay', 'ApiClientDeliveryController@pay');
        Route::post('paying', 'ApiClientDeliveryController@paying');
        Route::post('history', 'ApiClientDeliveryController@history');
    });

});

//rutas para el transportista
Route::group(['prefix'=>'driver'], function (){
    Route::post('register', 'ApiDriverController@register');
    Route::post('login', 'ApiDriverController@login');


});

//Api compartida para cliente-administrador-transportista agregado por Jimmy
//Obtener Color
Route::get('get-colors', 'ColorController@index');
Route::get('get-categories', 'CategoryController@index');
Route::get('get-types', 'TypeController@index');
Route::post('get-type', 'TypeController@show');
Route::get('get-marks', 'MarkController@index');
//api usuario
Route::get('get-occupations', 'OccupationController@index');




