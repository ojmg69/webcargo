<?php

use App\Provider;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name'              => 'Navegar usuarios.',
            'slug'              => 'user.index',
            'description'       => 'Lista y navega todos los usuarios del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación de un nuevo usuario.',
            'slug'              => 'user.create',
            'description'       => 'Crear un usuario al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle de usuarios.',
            'slug'              => 'user.show',
            'description'       => 'Ver en detalle cada usuario del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion de usuarios.',
            'slug'              => 'user.edit',
            'description'       => 'Editar cualquier dato de un usuario del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar usuario.',
            'slug'              => 'user.destroy',
            'description'       => 'Eliminar cualquier usuario del sistema.',
        ]);

        /************************ CLIENTES **********************************/

        Permission::create([
            'name'              => 'Lista Cliente.',
            'slug'              => 'client.index',
            'description'       => 'Lista y navega todos los clientes del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación de un nuevo cliente.',
            'slug'              => 'client.create',
            'description'       => 'Crear un usuario al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle de un cliente.',
            'slug'              => 'client.show',
            'description'       => 'Ver en detalle cada cliente del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion de cliente.',
            'slug'              => 'client.edit',
            'description'       => 'Editar cualquier dato de un cliente del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar cliente.',
            'slug'              => 'client.destroy',
            'description'       => 'Eliminar cualquier cliente del sistema.',
        ]);

        /************************ ROLES **********************************/

        Permission::create([
            'name'              => 'Lista Roles.',
            'slug'              => 'role.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación de un nuevo rol.',
            'slug'              => 'role.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle de un rol.',
            'slug'              => 'role.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion de role.',
            'slug'              => 'role.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar rol.',
            'slug'              => 'role.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);

        /************************ DRIVER **********************************/

        Permission::create([
            'name'              => 'Lista Transportistas.',
            'slug'              => 'driver.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación de un nuevo Transportista.',
            'slug'              => 'driver.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle de un Transportista.',
            'slug'              => 'driver.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion de Transportista.',
            'slug'              => 'driver.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar Transportista.',
            'slug'              => 'driver.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);

        /************************ PROVIDER **********************************/

        Permission::create([
            'name'              => 'Listar.',
            'slug'              => 'provider.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación',
            'slug'              => 'provider.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle',
            'slug'              => 'provider.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion',
            'slug'              => 'provider.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar',
            'slug'              => 'provider.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);

        /************************ FAMILY **********************************/

        Permission::create([
            'name'              => 'Listar.',
            'slug'              => 'family.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación',
            'slug'              => 'family.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle',
            'slug'              => 'family.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion',
            'slug'              => 'family.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar',
            'slug'              => 'family.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);

        /************************ CATEGORY **********************************/

        Permission::create([
            'name'              => 'Listar.',
            'slug'              => 'category.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación',
            'slug'              => 'category.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle',
            'slug'              => 'category.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion',
            'slug'              => 'category.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar',
            'slug'              => 'category.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);

        /************************ PRODUCT **********************************/

        Permission::create([
            'name'              => 'Listar.',
            'slug'              => 'product.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación',
            'slug'              => 'product.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle',
            'slug'              => 'product.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion',
            'slug'              => 'product.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar',
            'slug'              => 'product.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);

        /************************ CAR **********************************/

        Permission::create([
            'name'              => 'Listar.',
            'slug'              => 'car.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación',
            'slug'              => 'car.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle',
            'slug'              => 'car.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion',
            'slug'              => 'car.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar',
            'slug'              => 'car.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);

        /************************ SERVICE **********************************/

        Permission::create([
            'name'              => 'Listar.',
            'slug'              => 'service.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación',
            'slug'              => 'service.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle',
            'slug'              => 'service.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion',
            'slug'              => 'service.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar',
            'slug'              => 'service.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);


        /************************ PERMISION USER **********************************/

        Permission::create([
            'name'              => 'Listar.',
            'slug'              => 'permission_user.index',
            'description'       => 'Lista y navega todos los roles del sistema.',
        ]);
        Permission::create([
            'name'              => 'Creación',
            'slug'              => 'permission_user.create',
            'description'       => 'Registrar un rol al sistema.',
        ]);
        Permission::create([
            'name'              => 'Ver detalle',
            'slug'              => 'permission_user.show',
            'description'       => 'Ver en detalle cada rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Edicion',
            'slug'              => 'permission_user.edit',
            'description'       => 'Editar cualquier dato de un rol del sistema.',
        ]);
        Permission::create([
            'name'              => 'Eliminar',
            'slug'              => 'permission_user.destroy',
            'description'       => 'Eliminar cualquier rol del sistema.',
        ]);


    }
}
