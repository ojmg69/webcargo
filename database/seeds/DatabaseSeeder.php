<?php

use App\Administrator;
use App\People;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        People::create([
            'first_name'              => 'Ronaldo',
            'last_name'              => 'Rivero Gonzales',
            'ci'       => '12788605',
            'gender'              => 'M',
            'birthday'              => '1995-06-23',
            'telephone'              => '76042142',
            'type'              => '1',
            'status'              => '200',
        ]);
        Administrator::create([
            'people_id' => 1
        ]);

        User::create([
            'email' => 'ronaldorivero3@gmail.com',
            'password'  => bcrypt('73642804rivero'),
            'people_id' => 1
        ]);
    }
}
