<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('description')->nullable();
            $table->float('total_price', 8, 2);
            $table->jsonb('locate_origin');
            $table->jsonb('locate_destination');
            $table->string('origin')->nullable();
            $table->string('destination')->nullable();
            $table->string('description_origin')->nullable();
            $table->string('description_destination')->nullable();
            $table->string('product_type')->nullable();
            $table->integer('helpers')->nullable();
            $table->timestamp('schedule_order')->nullable();
            $table->string('measured_type');
            $table->float('weight', 8, 2);
            $table->float('width', 8, 2);
            $table->float('high', 8, 2);
            $table->smallInteger('status');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('type_vehicle_id');
            $table->float('distance_order', 8, 2);
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('type_vehicle_id')->references('id')->on('type_vehicles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
