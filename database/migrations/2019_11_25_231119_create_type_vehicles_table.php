<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('description');
            $table->string('url_image');
            $table->unsignedBigInteger('category_id');
            $table->float('base_price', 8, 2);
            $table->float('min_distance', 8, 2);
            $table->float('max_distance', 8, 2);
            $table->float('min_cost_km', 8, 2);
            $table->float('max_cost_km', 8, 2);
            $table->float('commission_percentage', 8, 2);
            $table->float('tn_cost', 8, 2);
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_vehicles');
    }
}
