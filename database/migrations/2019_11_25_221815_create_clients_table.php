<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid_facebook')->nullable();
            $table->string('uuid_gmail')->nullable();
            $table->string('email')->unique();
            $table->unsignedBigInteger('occupation_id');
            $table->unsignedBigInteger('people_id');
            $table->timestamps();

            $table->foreign('occupation_id')->references('id')->on('occupations')->onDelete('cascade');
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
