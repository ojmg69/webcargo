<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeoplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peoples', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('full_name');
            $table->string('ci')->nullable();
            $table->char('gender', 1);
            $table->date('birthday')->nullable();
            $table->string('country_code')->nullable();
            $table->string('mobile')->nullable();
            $table->string('telephone')->nullable();
            $table->string('url_profile')->nullable();
            $table->smallInteger('people_type'); //1 users 2 client //3 transportista
            $table->string('uuid_notify')->nullable();
            $table->string('number_account')->nullable();
            $table->string('address')->nullable();
            $table->string('url_ci')->nullable();
            $table->string('department')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peoples');
    }
}
