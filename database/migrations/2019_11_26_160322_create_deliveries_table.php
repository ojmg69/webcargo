<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('observation')->nullable();
            $table->smallInteger('score')->nullable();
            $table->smallInteger('status');
            $table->date('start_delivery')->nullable();
            $table->date('end_delivery')->nullable();
            $table->unsignedBigInteger('offert_id');
            $table->timestamps();

            $table->foreign('offert_id')->references('id')->on('offerts')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('deliveries');
        Schema::drop('deliveries');
    }
}
