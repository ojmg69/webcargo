<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('url_brevet')->nullable();
            $table->string('url_ruat')->nullable();
            $table->string('url_soat')->nullable();
            $table->string('url_car')->nullable();
            $table->string('car_owner')->nullable();
            $table->string('capacity');
            $table->string('license_plate');
            $table->smallInteger('status_offert');
            $table->smallInteger('status_notify');
            $table->unsignedBigInteger('mark_id');
            $table->string('model');
            $table->unsignedBigInteger('color_id');
            $table->unsignedBigInteger('type_vehicle_id');
            $table->unsignedBigInteger('people_id');
            $table->timestamps();

            $table->foreign('mark_id')->references('id')->on('marks')->onDelete('cascade');
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');
            $table->foreign('type_vehicle_id')->references('id')->on('type_vehicles')->onDelete('cascade');
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
